!----------------------------------------------------------------!
!  This is universal module with basic constants and functions   !
!----------------------------------------------------------------!
!  Last revision of constants: 06/2004                           !
!  Last changes: 08/2004                                         !
!                                                                !
!  Karel Houfek (karel.houfek@gmail.com)                         !
!----------------------------------------------------------------!
!
!  Description:
!
!  * mathematical and physical constants
!
!  * miscellaenous but useful functions
!
!    - Round(x, n)                  - rounding of a real number to a given number of digits
!    - Int2Str(i, nchars, zch)      - converting an integer to a string 
!    - Check_Error(error, str)      - unified handling of errors, termination
!    - Check_Size(str, n1, ..., n5) - checking if n1 = n2 = ... = n5
!    - Swap(a, b)                   - universal function for swapping two values or arrays
!    - IndexA(n, arr, indx)         - quicksort indexing of an array
!    - Root(funcd, x1, x2, xacc)    - finding root of 1D function
!
!  * polynomials
!
!    - Poly(x, coef)       - evaluation of polynomial for x and coef real or complex, x scalar or array
!    - DDPoly(x, coef, pd) - derivatives of polynomial for x and coef real or complex, x scalar or array
!    - Poly_Roots(a, m, roots, polish) - all roots of polynomial, a and roots complex
!
!  * special functions
!
!    - Gamma(x)            - Euler's function Gamma of real x
!    - Erf(z)              - error function erf(z) of real or complex z
!    - Erfc(z)             - complementary error function erfc(x) of real or complex z
!    - SphericalBessel(ch, n, x [, df])  - spherical Bessel functions j_n(x), y_n(x), h^+_n(x) or h^-_n(x) (ch = 'j','y','h+' or 'h-')
!                            and optionally its derivative for integer or complex n and real or complex x (see INTERFACE)
!                            Note: for x = 0 returns incorrect result
!    - CylidricalBessel(ch, n, x [, df]) - cylindrical Bessel functions j_n(x), y_n(x), h^+_n(x) or h^-_n(x) (ch = 'j','y','h+' or 'h-')
!                            and optionally its derivative for integer, real or complex n and real or complex x
!    - HermitePol(n, x)    - Hermite polynomial H_n(x) for scalar or array x
!
!  * functions and subroutines for grids
!
!    - Build_Equidistant_Grid(p, n, [x_min, x_max, q])  - equidistant grids
!    - Build_Gauss_Grid(grid, n, [x_min, x_max, which]) - grids Gauss-Legendre, Gauss-Lobatto, ...
!    - Grid_Clean(grid)                                 - deallocation of memory
!
!  Sources:
!
!    http://physics.nist.gov/PhysRefData
!    Numerical Recipes in Fortran
!    Abramovitz, Stegun
!    http://mathworld.wolfram.com
!
!  Note:
!
!    This module is dependent on:
!    'wofc.f' by G.P.M. Poppe and C.M.J. Wijers (ACM TOMS 16 (1990) 48)
!    'coulcc.f' by I.J.Thompson and A.R.Barnett (CPC 36 (1985) 363)
!

MODULE general
  USE iso_c_binding

  IMPLICIT NONE

! precision of calculation
!-------------------------
  INTEGER, PARAMETER :: prec = C_DOUBLE ! SELECTED_REAL_KIND(P=8)

! error status for general use
!-----------------------------
  INTEGER :: error

! mathematical constants
!-----------------------
  REAL(KIND=prec), PARAMETER ::       &
    pi     = 3.1415926535897932_prec, &  ! Pi
    pitwo  = 6.2831853071795865_prec, &  ! 2 * Pi
    twoopi = 0.63661977236758134_prec,&  ! 2 / Pi
    pio4   = 0.78539816339744831_prec,&  ! Pi / 4
    sqrtpi = 1.7724538509055160_prec, &  ! SQRT(Pi)
    eu     = 2.7182818284590452_prec, &  ! Euler number
    sqrt2  = 1.4142135623730950_prec     ! SQRT(2)
  COMPLEX(KIND=prec), PARAMETER ::    &
    imu    = (0.0_prec,1.0_prec),     &  ! imaginary unit
    zero   = (0.0_prec,0.0_prec),     &  ! complex zero
    zone   = (1.0_prec,0.0_prec)         ! complex one
  REAL(KIND=prec), PARAMETER ::       &
    tiny   = 1.0e-16_prec                ! tiny number used instead of zero
                                         !   if 1/0 error could occur 
! physical constants
!-------------------
  REAL(KIND=prec), PARAMETER ::     &
    phys_a0 = 0.5291772108_prec,    & ! Bohr radius [angstroms]
    phys_h0 = 27.2113845_prec,      & ! 1 Hartree in [eV]
    phys_t0 = 0.024188843265_prec,  & ! 1 time a.u. in [fs]
    phys_k  = 8.617343e-5_prec,     & ! Boltzman konstant in [eV/K]
    phys_e  = 1.60217653e-19_prec,  & ! 1 eV [J] = electron charge
    phys_me = 9.1093826e-31_prec,   & ! electron mass [kg]
    phys_u  = 1.66053886e-27_prec     ! (unified) atomic mass unit [kg]

! general grid with weights
!--------------------------
  TYPE grid_type
    LOGICAL :: init
    INTEGER :: n                ! number of grid points
    REAL(KIND=prec) :: &
      x_min, &                  ! endpoints of
      x_max                     !   the interval
    REAL(KIND=prec), DIMENSION(:), POINTER :: &
      x, &                      ! arrays (1:n) for grid points and quadrature weights
      w                         !   in the interval
  END TYPE grid_type

! auxiliary subroutine for swaping two values or two arrays
!----------------------------------------------------------
  INTERFACE Swap
    MODULE PROCEDURE iSwap, dSwap, zSwap
    MODULE PROCEDURE dvSwap, zvSwap
  END INTERFACE

! polynomials
!------------

! evaluation of polynomials
  INTERFACE Poly
    MODULE PROCEDURE Poly_DD1
    MODULE PROCEDURE Poly_DDN
    MODULE PROCEDURE Poly_ZD1
    MODULE PROCEDURE Poly_ZDN
    MODULE PROCEDURE Poly_DZ1
    MODULE PROCEDURE Poly_DZN
    MODULE PROCEDURE Poly_ZZ1
    MODULE PROCEDURE Poly_ZZN
  END INTERFACE

! derivatives of polynomials
  INTERFACE DDPoly
    MODULE PROCEDURE DDPoly_DD1
    MODULE PROCEDURE DDPoly_DDN
    MODULE PROCEDURE DDPoly_ZD1
    MODULE PROCEDURE DDPoly_ZDN
    MODULE PROCEDURE DDPoly_DZ1
    MODULE PROCEDURE DDPoly_DZN
    MODULE PROCEDURE DDPoly_ZZ1
    MODULE PROCEDURE DDPoly_ZZN
  END INTERFACE

! roots of polynomials
  INTERFACE Poly_Roots
    MODULE PROCEDURE zroots
  END INTERFACE

! special functions
!------------------
  INTERFACE Erf
    
    MODULE PROCEDURE DErf
    MODULE PROCEDURE ZErf

  END INTERFACE

  INTERFACE ErfC
    
    MODULE PROCEDURE DErfC
    MODULE PROCEDURE ZErfC

  END INTERFACE

  INTERFACE SphericalBessel
    
    MODULE PROCEDURE SphericalBessel_ID ! integer n, real x
    MODULE PROCEDURE SphericalBessel_IZ ! integer n, complex x
    MODULE PROCEDURE SphericalBessel_ZZ ! complex n, complex x
    
  END INTERFACE

  INTERFACE CylindricalBessel
    
    MODULE PROCEDURE CylindricalBessel_ID ! integer n, real x
    MODULE PROCEDURE CylindricalBessel_IZ ! integer n, complex x
    MODULE PROCEDURE CylindricalBessel_ZZ ! complex n, complex x
    
  END INTERFACE

  INTERFACE HermitePol
    
    MODULE PROCEDURE HermitePol1
    MODULE PROCEDURE HermitePolN

  END INTERFACE

CONTAINS

! General subroutine to handle an error
! (espacially during allocation and I/O)
!---------------------------------------
  SUBROUTINE Check_Error(error, str)
    INTEGER, INTENT(IN) :: error
    CHARACTER(LEN=*), INTENT(IN) :: str
  !---
    IF (error /= 0) THEN
      WRITE(*,'(A)') ''
      WRITE(*,'(A)') 'Error during '//str
      STOP 'Program terminated by Check_Error()'
    END IF
    RETURN
  END SUBROUTINE Check_Error

! General subroutine to check sizes of arrays
!   up to 5 arrays at once
!--------------------------------------------
  SUBROUTINE Check_Size(str, n1, n2, n3, n4, n5)
    CHARACTER(LEN=*), INTENT(IN) :: str
    INTEGER, INTENT(IN) :: n1, n2
    INTEGER, OPTIONAL :: n3, n4, n5
  !---
    INTEGER :: na(3:5)
  !---
    na = n1
    IF (PRESENT(n3)) na(3) = n3
    IF (PRESENT(n4)) na(4) = n4
    IF (PRESENT(n5)) na(5) = n5
    IF (.NOT. (n1 == n2 .AND. ALL(n2 == na(:)))) THEN
      WRITE(*,'(A)') ''
      WRITE(*,'(A)') 'Different size of at least two arrays in '//str//'. Compared sizes: '
      WRITE(*,*) n1, n2, na
      STOP 'Program terminated by Check_Size()'
    END IF
    RETURN
  END SUBROUTINE Check_Size

! Rounding of a real number to a given number of digits
!   (if anybody knows a more elegant solution, please let me know)
!-----------------------------------------------------------------
  FUNCTION Round(x, n)
    REAL(KIND=prec), INTENT(IN) :: x
    INTEGER, INTENT(IN) :: n 
    REAL(KIND=prec) :: Round
  !---
    CHARACTER(LEN=1) :: dec1
    CHARACTER(LEN=2) :: all, dec2
    CHARACTER(LEN=24) :: number
  !---
    SELECT CASE (n)
    CASE (1)
      WRITE(number,'(1E9.1E3)') x
    CASE (2:15)
      WRITE(all,'(I2)') 8 + n 
      IF (n < 10) THEN
        WRITE(dec1,'(I1)') n
        WRITE(number,'(1E'//all//'.'//dec1//'E3)') x
      ELSE
        WRITE(dec2,'(I2)') n
        WRITE(number,'(1E'//all//'.'//dec2//'E3)') x
      END IF
    CASE DEFAULT
      WRITE(number,'(1E24.16E3)') x
    END SELECT
    READ(number,*) Round
    RETURN
  END FUNCTION Round

! Function which converts an integer to a string
!   if 'nchars' is not specified the integer is printed without any leading characters
!   if zch = 'y' then leading zeroes will be used (default is a space)
!   if 'nchars' < number of digits of i, returned string will be as if 'nchars' has not been used 
!------------------------------------------------------------------------------------------------
  FUNCTION Int2Str(i, nchars, zch)
    INTEGER, INTENT(IN) :: i                      ! a number to convert to string
    INTEGER, INTENT(IN), OPTIONAL :: nchars       ! # characters to print (max. is 99)
    CHARACTER(LEN=1), INTENT(IN), OPTIONAL :: zch ! 'y'/'n' - which leading characters to use
    CHARACTER(LEN=99) :: Int2Str
  !---
    INTEGER :: act_nchars
    CHARACTER(LEN=1) :: act_zch
    CHARACTER(LEN=2) :: ch2
    CHARACTER(LEN=20) :: str
  !---
    act_nchars = 0
    IF (PRESENT(nchars)) THEN
      act_nchars = ABS(nchars)
      IF (act_nchars >= 100) act_nchars = 99
    END IF
    act_zch = 'n'
    IF (PRESENT(zch)) act_zch = zch

    WRITE(str,'(I0)') i
  ! if number of digits < nchars ...
    IF (LEN(TRIM(ADJUSTL(str))) < act_nchars) THEN
      WRITE(ch2,'(I2)') act_nchars
      IF (act_zch == 'y') THEN
        WRITE(Int2Str, '(I'//TRIM(ch2)//'.'//TRIM(ch2)//')') i
      ELSE
        WRITE(Int2Str, '(I'//TRIM(ch2)//')') i
      END IF
  ! otherwise print a bare number
    ELSE
      WRITE(Int2Str, '(A)') TRIM(ADJUSTL(str))
    END IF
    RETURN
  END FUNCTION

! Euler's function Gamma - Abramovitz, Stegun S.257 (Bernoulli-Reihe)
!   accurate up to 14 significant digits in the interval (-5,160)
!--------------------------------------------------------------------
  FUNCTION Gamma(x)
    REAL(KIND=prec) :: Gamma
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec) :: rr, sum, pp
  !---
    rr = x + 10.0_prec
    pp = 1 / rr / rr
    sum = (pp * 43867.0_prec / 244188.0_prec - 3617.0_prec / 122400.0_prec) * pp + 1.0_prec / 156.0_prec
    sum = ((sum * pp - 691.0_prec / 360260.0_prec) * pp + 1.0_prec / 1188.0_prec) * pp - 1.0_prec / 1680.0_prec
    sum = (sum * pp + 1.0_prec / 1260.0_prec) * pp - 1.0_prec / 360.0_prec
    sum = (sum * pp + 1.0_prec / 12.0_prec) / rr
    sum = sum + (rr - 0.5_prec) * LOG(rr) - rr + 0.5_prec * LOG(2.0_prec * pi)
    pp = x * (x + 1.0_prec) * (x + 2.0_prec) * (x + 3.0_prec) * (x + 4.0_prec) * (x + 5.0_prec) * (x + 6.0_prec)
    pp = pp * (x + 7.0_prec) * (x + 8.0_prec) * (x + 9.0_prec)
    Gamma = EXP(sum) / pp
    RETURN
  END FUNCTION Gamma

! Auxiliary function to find root of 1-dim function
! based on combination of bisection and Newton-Raphson's method
! (from Numerical Recipes, Chapter 9.4)
! Input: funcd - function returning functional value and its derivative
!        x1,x2 - find the root between these points
!        xacc  - accuracy of the root
! Output: Root of the function
!----------------------------------------------------------------------
  FUNCTION Root(funcd, x1, x2, xacc)
    REAL(KIND=prec), INTENT(IN) :: x1, x2, xacc
    REAL(KIND=prec) :: Root
    INTERFACE
      SUBROUTINE funcd(x, fval, fderiv)
        IMPLICIT NONE
        REAL(KIND=SELECTED_REAL_KIND(P=8)), INTENT(IN) :: x
        REAL(KIND=SELECTED_REAL_KIND(P=8)), INTENT(OUT) :: fval, fderiv
      END SUBROUTINE funcd
    END INTERFACE
  !---
    INTEGER, PARAMETER :: MAXIT = 100
    INTEGER :: j
    REAL(KIND=prec) :: df, dx, dxold, f, fh, fl, temp, xh, xl
  !---
    CALL funcd(x1, fl, df)
    CALL funcd(x2, fh, df)
    IF ((fl > 0.0_prec .AND. fh > 0.0_prec) .OR. (fl < 0.0_prec .AND. fh < 0.0_prec)) THEN
      WRITE(*,*) 'Root must be bracketed'
      STOP 'Program stop by function Root()'
    END IF
    IF (fl == 0.0_prec) THEN
      Root = x1
      RETURN
    ELSE IF (fh == 0.0_prec) THEN
      Root = x2
      RETURN
    ELSE IF (fl < 0.0_prec) THEN
      xl = x1
      xh = x2
    ELSE
      xh = x1
      xl = x2
    END IF
    Root = 0.5_prec * (x1 + x2)
    dxold = ABS(x2 - x1)
    dx = dxold
    CALL funcd(Root, f, df)
    DO j = 1, MAXIT
      IF (((Root - xh) * df - f) * ((Root - xl) * df - f) > 0.0_prec .OR. &
          ABS(2.0_prec * f) > ABS(dxold * df)) THEN
        dxold = dx
        dx = 0.5_prec * (xh - xl)
        Root = xl + dx
        IF (xl == Root) RETURN
      ELSE
        dxold = dx
        dx = f / df
        temp = Root
        Root = Root - dx
        IF (temp == Root) RETURN
      END IF
      IF (ABS(dx) < xacc) RETURN
      CALL funcd(Root, f, df)
      IF (f < 0.0_prec) THEN
        xl = Root
      ELSE
        xh = Root
      END IF
    END DO
    WRITE(*,*) 'Root: exceeded maximum iterations'
    STOP 'Program stop by function Root()'
  END FUNCTION Root

! Quicksort indexing of array 'arr' (based on Numerical Recipes)
! Index is stored into array 'indx' thus arr(indx(i)) are in ascending order
!---------------------------------------------------------------------------
  SUBROUTINE IndexA(n, arr, indx)
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec) :: arr(n)
    INTEGER, INTENT(OUT) :: indx(n)
    INTEGER, PARAMETER :: M = 7, NSTACK = 50
    INTEGER :: i, indxt, ir, j, jstack, k, l, istack(NSTACK)
    REAL(KIND=prec) :: a
  !---
    DO j = 1, n
      indx(j) = j
    END DO
    jstack = 0
    l = 1
    ir = n
    DO
      IF (ir - l < M) THEN
        DO j = l+1, ir
          indxt = indx(j)
          a = arr(indxt)
          DO i = j-1, l, -1
            IF (arr(indx(i)) <= a) EXIT
            indx(i+1) = indx(i)
          END DO
          indx(i+1)=indxt
        END DO
        IF (jstack == 0) RETURN
        ir = istack(jstack)
        l = istack(jstack-1)
        jstack = jstack - 2
      ELSE
        k = (l + ir) / 2
        CALL Swap(indx(k), indx(l+1))
        IF (arr(indx(l)) > arr(indx(ir)))   CALL Swap(indx(l), indx(ir))
        IF (arr(indx(l+1)) > arr(indx(ir))) CALL Swap(indx(l+1), indx(ir))
        IF (arr(indx(l)) > arr(indx(l+1)))  CALL Swap(indx(l), indx(l+1))
        i = l + 1
        j = ir
        indxt = indx(l+1)
        a = arr(indxt)
        DO
          DO
            i = i + 1
            IF (arr(indx(i)) >= a) EXIT
          END DO
          DO
            j = j - 1
            IF (arr(indx(j)) <= a) EXIT
          END DO
          IF (j < i) EXIT
          CALL Swap(indx(i), indx(j))
        END DO
        indx(l+1) = indx(j)
        indx(j) = indxt
        jstack = jstack + 2
        IF (jstack > NSTACK) THEN
          WRITE(*,*) 'NSTACK too small in IndexA'
          STOP 'Program stop by subroutine IndexA'
        END IF
        IF(ir - i + 1 >= j - l) THEN
          istack(jstack) = ir
          istack(jstack-1) = i
          ir = j - 1
        ELSE
          istack(jstack) = j - 1
          istack(jstack-1) = l
          l = i
        END IF
      END IF
    END DO
  END SUBROUTINE IndexA

! Swaping auxiliary subroutines (like in Numerical Recipes)
!----------------------------------------------------------
  SUBROUTINE iSwap(a, b)
    INTEGER, INTENT(INOUT) :: a, b
    INTEGER :: temp
  !---
    temp = a
    a = b
    b = temp

  END SUBROUTINE iSwap

  SUBROUTINE dSwap(a, b)
    REAL(KIND=prec), INTENT(INOUT) :: a, b
    REAL(KIND=prec) :: temp
  !---
    temp = a
    a = b 
    b = temp
    RETURN
  END SUBROUTINE dSwap

  SUBROUTINE zSwap(a, b)
    COMPLEX(KIND=prec), INTENT(INOUT) :: a, b
    COMPLEX(KIND=prec) :: temp
  !---
    temp = a
    a = b
    b = temp
    RETURN
  END SUBROUTINE zSwap

  SUBROUTINE dvSwap(a, b)
    REAL(KIND=prec), DIMENSION(:), INTENT(INOUT) :: a, b
    REAL(KIND=prec), DIMENSION(SIZE(a)) :: temp
  !---
    temp = a
    a = b
    b = temp
    RETURN
  END SUBROUTINE dvSwap

  SUBROUTINE zvSwap(a,b)
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(INOUT) :: a,b
    COMPLEX(KIND=prec), DIMENSION(SIZE(a)) :: temp
  !---
    temp = a
    a = b
    b = temp
    RETURN
  END SUBROUTINE zvSwap


!==========================================================!

!-----------------------------!
!  Evaluation of polynomials  !
!-----------------------------!

! REAL x and REAL coefficients
  FUNCTION Poly_DD1(x, coef)
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    REAL(KIND=prec) :: Poly_DD1
  !---
    INTEGER :: n, i
  !---
    n = SIZE(coef)
    Poly_DD1 = coef(n)
    DO i = n - 1, 1, -1
      Poly_DD1 = x * Poly_DD1 + coef(i)
    END DO
    RETURN
  END FUNCTION Poly_DD1

! array of REAL x(:) and REAL coefficients
  FUNCTION Poly_DDN(x, coef)
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: x
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    REAL(KIND=prec), DIMENSION(SIZE(x)) :: Poly_DDN
  !---
    INTEGER :: n, i
  !---
    n = SIZE(coef)
    Poly_DDN = coef(n)
    DO i = n - 1, 1, -1
      Poly_DDN = x * Poly_DDN + coef(i)
    END DO
    RETURN
  END FUNCTION Poly_DDN

! COMPLEX x and REAL coefficients
  FUNCTION Poly_ZD1(x, coef)
    COMPLEX(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec) :: Poly_ZD1
  !---
    INTEGER :: n, i
  !---
    n = SIZE(coef)
    Poly_ZD1 = coef(n)
    DO i = n - 1, 1, -1
      Poly_ZD1 = x * Poly_ZD1 + coef(i)
    END DO
    RETURN
  END FUNCTION Poly_ZD1

! array of COMPLEX x(:) and REAL coefficients
  FUNCTION Poly_ZDN(x, coef)
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: x
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec), DIMENSION(SIZE(x)) :: Poly_ZDN
  !---
    INTEGER :: n, i
  !---
    n = SIZE(coef)
    Poly_ZDN = coef(n)
    DO i = n - 1, 1, -1
      Poly_ZDN = x * Poly_ZDN + coef(i)
    END DO
    RETURN
  END FUNCTION Poly_ZDN

! REAL x and COMPLEX coefficients
  FUNCTION Poly_DZ1(x, coef)
    REAL(KIND=prec), INTENT(IN) :: x
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec) :: Poly_DZ1
  !---
    INTEGER :: n, i
  !---
    n = SIZE(coef)
    Poly_DZ1 = coef(n)
    DO i = n - 1, 1, -1
      Poly_DZ1 = x * Poly_DZ1 + coef(i)
    END DO
    RETURN
  END FUNCTION Poly_DZ1

! array of REAL x(:) and COMPLEX coefficients
  FUNCTION Poly_DZN(x, coef)
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: x
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec), DIMENSION(SIZE(x)) :: Poly_DZN
  !---
    INTEGER :: n, i
  !---
    n = SIZE(coef)
    Poly_DZN = coef(n)
    DO i = n - 1, 1, -1
      Poly_DZN = x * Poly_DZN + coef(i)
    END DO
    RETURN
  END FUNCTION Poly_DZN

! COMPLEX x and COMPLEX coefficients
  FUNCTION Poly_ZZ1(x, coef)
    COMPLEX(KIND=prec), INTENT(IN) :: x
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec) :: Poly_ZZ1
  !---
    INTEGER :: n, i
  !---
    n = SIZE(coef)
    Poly_ZZ1 = coef(n)
    DO i = n - 1, 1, -1
      Poly_ZZ1 = x * Poly_ZZ1 + coef(i)
    END DO
    RETURN
  END FUNCTION Poly_ZZ1

! array of COMPLEX x(:) and COMPLEX coefficients
  FUNCTION Poly_ZZN(x, coef)
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: x
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec), DIMENSION(SIZE(x)) :: Poly_ZZN
  !---
    INTEGER :: n, i
  !---
    n = SIZE(coef)
    Poly_ZZN = coef(n)
    DO i = n - 1, 1, -1
      Poly_ZZN = x * Poly_ZZN + coef(i)
    END DO
    RETURN
  END FUNCTION Poly_ZZN

!==========================================================!

!------------------------------!
!  Derivatives of polynomials  !
!------------------------------!

! From Numerical Recipes:
!  "Given the coefficients of a polynomial of degree nc-1 as an array coef(1:nc) with c(1) being
!   the constant term, and given a value x, and given a value nd > 1, this routine returns the
!   polynomial evaluated at x as pd(1) and nd-1 derivatives as pd(2:nd)."

! REAL x and REAL coefficients
!-----------------------------
  SUBROUTINE DDPoly_DD1(x, coef, pd)
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    REAL(KIND=prec), DIMENSION(:), INTENT(OUT) :: pd
  !---
    INTEGER :: i, j, nc, nd, nnd
    REAL(KIND=prec) :: const
  !---
    nc = SIZE(coef)
    nd = SIZE(pd)
    pd = 0.0_prec
    pd(1) = coef(nc)
    DO i = nc - 1, 1, -1
      nnd = MIN(nd, nc + 1 - i)
      DO j = nnd, 2, -1
        pd(j) = pd(j) * x + pd(j - 1)
      END DO
      pd(1) = pd(1) * x + coef(i)
    END DO
  ! After the first derivative, factorial constants come in
    const = 2.0_prec
    DO i = 3, nd
      pd(i) = const * pd(i)
      const = const * i
    END DO
    RETURN
  END SUBROUTINE DDPoly_DD1

! array of REAL x(:) and REAL coefficients
!-----------------------------------------
  SUBROUTINE DDPoly_DDN(x, coef, pd)
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: x
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    REAL(KIND=prec), DIMENSION(:,:), INTENT(OUT) :: pd
  !---
    INTEGER :: n, i, j, nc, nd, nnd
    REAL(KIND=prec) :: const
  !---
    nc = SIZE(coef)
    nd = SIZE(pd, DIM=2)
    pd = 0.0_prec
    pd(:,1) = coef(nc)
    DO i = nc - 1, 1, -1
      nnd = MIN(nd, nc + 1 - i)
      DO j = nnd, 2, -1
        pd(:,j) = pd(:,j) * x + pd(:,j - 1)
      END DO
      pd(:,1) = pd(:,1) * x + coef(i)
    END DO
  ! After the first derivative, factorial constants come in
    const = 2.0_prec
    DO i = 3, nd
      pd(:,i) = const * pd(:,i)
      const = const * i
    END DO
    RETURN
  END SUBROUTINE DDPoly_DDN

! COMPLEX x and REAL coefficients
!--------------------------------
  SUBROUTINE DDPoly_ZD1(x, coef, pd)
    COMPLEX(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(OUT) :: pd
  !---
    INTEGER :: i, j, nc, nd, nnd
    REAL(KIND=prec) :: const
  !---
    nc = SIZE(coef)
    nd = SIZE(pd)
    pd = zero
    pd(1) = coef(nc)
    DO i = nc - 1, 1, -1
      nnd = MIN(nd, nc + 1 - i)
      DO j = nnd, 2, -1
        pd(j) = pd(j) * x + pd(j - 1)
      END DO
      pd(1) = pd(1) * x + coef(i)
    END DO
  ! After the first derivative, factorial constants come in
    const = 2.0_prec
    DO i = 3, nd
      pd(i) = const * pd(i)
      const = const * i
    END DO
    RETURN
  END SUBROUTINE DDPoly_ZD1

! array of COMPLEX x(:) and REAL coefficients
!--------------------------------------------
  SUBROUTINE DDPoly_ZDN(x, coef, pd)
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: x
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec), DIMENSION(:,:), INTENT(OUT) :: pd
  !---
    INTEGER :: n, i, j, nc, nd, nnd
    REAL(KIND=prec) :: const
  !---
    nc = SIZE(coef)
    nd = SIZE(pd, DIM=2)
    pd = zero
    pd(:,1) = coef(nc)
    DO i = nc - 1, 1, -1
      nnd = MIN(nd, nc + 1 - i)
      DO j = nnd, 2, -1
        pd(:,j) = pd(:,j) * x + pd(:,j - 1)
      END DO
      pd(:,1) = pd(:,1) * x + coef(i)
    END DO
  ! After the first derivative, factorial constants come in
    const = 2.0_prec
    DO i = 3, nd
      pd(:,i) = const * pd(:,i)
      const = const * i
    END DO
    RETURN
  END SUBROUTINE DDPoly_ZDN

! REAL x and COMPLEX coefficients
!--------------------------------
  SUBROUTINE DDPoly_DZ1(x, coef, pd)
    REAL(KIND=prec), INTENT(IN) :: x
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(OUT) :: pd
  !---
    INTEGER :: i, j, nc, nd, nnd
    REAL(KIND=prec) :: const
  !---
    nc = SIZE(coef)
    nd = SIZE(pd)
    pd = zero
    pd(1) = coef(nc)
    DO i = nc - 1, 1, -1
      nnd = MIN(nd, nc + 1 - i)
      DO j = nnd, 2, -1
        pd(j) = pd(j) * x + pd(j - 1)
      END DO
      pd(1) = pd(1) * x + coef(i)
    END DO
  ! After the first derivative, factorial constants come in
    const = 2.0_prec
    DO i = 3, nd
      pd(i) = const * pd(i)
      const = const * i
    END DO
    RETURN
  END SUBROUTINE DDPoly_DZ1

! array of REAL x(:) and COMPLEX coefficients
!--------------------------------------------
  SUBROUTINE DDPoly_DZN(x, coef, pd)
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: x
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec), DIMENSION(:,:), INTENT(OUT) :: pd
  !---
    INTEGER :: n, i, j, nc, nd, nnd
    REAL(KIND=prec) :: const
  !---
    nc = SIZE(coef)
    nd = SIZE(pd, DIM=2)
    pd = zero
    pd(:,1) = coef(nc)
    DO i = nc - 1, 1, -1
      nnd = MIN(nd, nc + 1 - i)
      DO j = nnd, 2, -1
        pd(:,j) = pd(:,j) * x + pd(:,j - 1)
      END DO
      pd(:,1) = pd(:,1) * x + coef(i)
    END DO
  ! After the first derivative, factorial constants come in
    const = 2.0_prec
    DO i = 3, nd
      pd(:,i) = const * pd(:,i)
      const = const * i
    END DO
    RETURN
  END SUBROUTINE DDPoly_DZN

! COMPLEX x and COMPLEX coefficients
!-----------------------------------
  SUBROUTINE DDPoly_ZZ1(x, coef, pd)
    COMPLEX(KIND=prec), INTENT(IN) :: x
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(OUT) :: pd
  !---
    INTEGER :: i, j, nc, nd, nnd
    REAL(KIND=prec) :: const
  !---
    nc = SIZE(coef)
    nd = SIZE(pd)
    pd = zero
    pd(1) = coef(nc)
    DO i = nc - 1, 1, -1
      nnd = MIN(nd, nc + 1 - i)
      DO j = nnd, 2, -1
        pd(j) = pd(j) * x + pd(j - 1)
      END DO
      pd(1) = pd(1) * x + coef(i)
    END DO
  ! After the first derivative, factorial constants come in
    const = 2.0_prec
    DO i = 3, nd
      pd(i) = const * pd(i)
      const = const * i
    END DO
    RETURN
  END SUBROUTINE DDPoly_ZZ1

! array of COMPLEX x(:) and COMPLEX coefficients
!-----------------------------------------------
  SUBROUTINE DDPoly_ZZN(x, coef, pd)
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: x
    COMPLEX(KIND=prec), DIMENSION(:), INTENT(IN) :: coef
    COMPLEX(KIND=prec), DIMENSION(:,:), INTENT(OUT) :: pd
  !---
    INTEGER :: n, i, j, nc, nd, nnd
    REAL(KIND=prec) :: const
  !---
    nc = SIZE(coef)
    nd = SIZE(pd, DIM=2)
    pd = zero
    pd(:,1) = coef(nc)
    DO i = nc - 1, 1, -1
      nnd = MIN(nd, nc + 1 - i)
      DO j = nnd, 2, -1
        pd(:,j) = pd(:,j) * x + pd(:,j - 1)
      END DO
      pd(:,1) = pd(:,1) * x + coef(i)
    END DO
  ! After the first derivative, factorial constants come in
    const = 2.0_prec
    DO i = 3, nd
      pd(:,i) = const * pd(:,i)
      const = const * i
    END DO
    RETURN
  END SUBROUTINE DDPoly_ZZN

!==========================================================!

!------------------------!
!  Roots of polynomials  !
!------------------------!

! All roots of a polynomial with complex coefficients
!----------------------------------------------------
  SUBROUTINE zroots(a, m, roots, polish)
    INTEGER, INTENT(IN) :: m
    COMPLEX(KIND=prec), INTENT(IN) :: a(m+1)
    COMPLEX(KIND=prec), INTENT(OUT) :: roots(m)
    LOGICAL, INTENT(IN) :: polish
  !---
    REAL(KIND=prec), PARAMETER :: EPS = 1.0e-12_prec
    INTEGER, PARAMETER :: MAXM = 101
    INTEGER :: i, ii, j, jj, its
    COMPLEX(KIND=prec) :: ad(MAXM), x, b, c
  !---
    DO j = 1, m + 1
      ad(j) = a(j)
    END DO
    DO j = m, 1, -1
      x = zero
      CALL laguer(ad, j, x, its)
      IF (ABS(AIMAG(x)) <= 2.0_prec * EPS**2 * ABS(REAL(x))) THEN
        x = CMPLX(REAL(x, KIND=prec), 0.0_prec, KIND=prec)
      END IF
      roots(j) = x
      b = ad(j + 1)
      DO jj = j, 1, -1
        c = ad(jj)
        ad(jj) = b
        b = x * b + c
      END DO
    END DO
    IF (polish) THEN
      DO j = 1, m
        CALL laguer(a, m, roots(j), its)
      END DO
    END IF
    DO j = 2, m
      x = roots(j)
      ii = 0
      DO i = j - 1, 1, -1
        IF (REAL(roots(i)) <= REAL(x)) THEN
          ii = i
          EXIT
        END IF
        roots(i + 1) = roots(i)
      END DO
      roots(ii + 1) = x
    END DO
    RETURN
  END SUBROUTINE zroots

! Auxiliary subroutine for zroots()
!----------------------------------
  SUBROUTINE laguer(a, m, x, its)
    INTEGER, INTENT(IN) :: m
    COMPLEX(KIND=prec), INTENT(IN) :: a(m+1)
    COMPLEX(KIND=prec), INTENT(OUT) :: x 
    INTEGER, INTENT(OUT) :: its
  !---
    INTEGER, PARAMETER :: MR = 8, MT = 10, MAXIT = MT * MR
    REAL(KIND=prec), PARAMETER :: EPSS = 2.0e-16_prec
    INTEGER :: iter,j
    REAL(KIND=prec) :: abx, abp, abm, err
    COMPLEX(KIND=prec) :: dx, x1, b, d, f, g, h, sq, gp, gm, g2
    REAL(KIND=prec), PARAMETER :: &
      frac(MR) = (/0.50_prec, 0.25_prec, 0.75_prec, 0.13_prec, &
                   0.38_prec, 0.62_prec, 0.88_prec, 1.00_prec/)
  !---
    DO iter = 1, MAXIT
      its = iter
      b = a(m + 1)
      err = ABS(b)
      d = zero
      f = zero
      abx = ABS(x)
      DO j = m, 1, -1
        f = x * f + d
        d = x * d + b
        b = x * b + a(j)
        err = ABS(b) + abx * err
      END DO
      err = EPSS * err
      IF(ABS(b) <= err) THEN
        RETURN
      ELSE
        g = d / b
        g2 = g * g
        h = g2 - 2.0_prec * f / b
        sq = SQRT((m - 1) * (m * h - g2))
        gp = g + sq
        gm = g - sq
        abp = ABS(gp)
        abm = ABS(gm)
        IF (abp < abm) THEN
          gp = gm
        END IF
        IF (MAX(abp, abm) > 0.0_prec) THEN
          dx = m / gp
        ELSE
          dx = EXP(CMPLX(LOG(1.0_prec + abx), REAL(iter, KIND=prec), KIND=prec))
        ENDIF
      ENDIF
      x1 = x - dx
      IF (x == x1) THEN
        RETURN
      END IF
      IF (MOD(iter, MT) /= 0) THEN
        x = x1
      ELSE
        x = x - dx * frac(iter / MT)
      END IF
    END DO
    WRITE(*,'(A)') 'Too many iterations in laguer'
    RETURN
  END SUBROUTINE laguer


!==========================================================!

!-------------------!
! Special functions !
!-------------------!

! Calculation of error function erf(x)
!   for real 'x'
!-------------------------------------
  FUNCTION DErf(x)
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec) :: DErf
  !---
    COMPLEX(KIND=prec) :: z
  !---
    z = CMPLX(x, 0.0_prec, KIND=prec)
    DErf = 1.0_prec - REAL(ZErfC(z))
    RETURN
  END FUNCTION DErf

! Calculation of error function erf(z)
!   for complex 'z'
!-------------------------------------
  FUNCTION ZErf(z)
    COMPLEX(KIND=prec), INTENT(IN) :: z
    COMPLEX(KIND=prec) :: ZErf
  !---
    ZErf = zone - ZErfC(z)
    RETURN
  END FUNCTION ZErf

! Calculation of complementary error function erfc(x)
!   for real 'x'
!----------------------------------------------------
  FUNCTION DErfC(x)
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec) :: DErfC
  !---
    COMPLEX(KIND=prec) :: z
  !---
    z = CMPLX(x, 0.0_prec, KIND=prec)
    DErfC = REAL(ZErfC(z))
    RETURN
  END FUNCTION DErfC

! Calculation of complementary error function erfc(z)
!   for complex 'z'
!----------------------------------------------------
  FUNCTION ZErfC(z)
    COMPLEX(KIND=prec), INTENT(IN) :: z
    COMPLEX(KIND=prec) :: ZErfC
  !---
    REAL(KIND=prec) :: x, y, u, v
    COMPLEX(KIND=prec) :: zz
    LOGICAL :: FLAG
  !---
    x = -AIMAG(z)
    y = REAL(z)
    IF (y < -26.0_prec .AND. ABS(x) < -y * 0.99_prec) THEN
      ZErfC = CMPLX(2.0_prec, 0.0_prec, KIND=prec)
    ELSE
      CALL WOFZ(x, y, u ,v, FLAG)
      IF (FLAG) THEN
        WRITE(*,'(A)') 'Error: WOFZ failed to calculate the Faddeeva-function W(Z) = EXP(-Z**2)*ERFC(-I*Z),'
        WRITE(*,'(A,1P 1E13.5E2,A,1P 1E13.5E2,A)') 'for z = (', x, ',', y, ')'
        STOP
      END IF
      zz = CMPLX(x, y, KIND=prec)
      ZErfC = CMPLX(u, v, KIND=prec) * EXP(zz * zz)
    END IF
    RETURN
  END FUNCTION ZErfC

! Energy-normalized spherical Bessel function multiplied by r
! (standard intial state for quantum scattering calculations)
! E.g. for J = 0 (energy-normalized sine)
!   SQRT(2 * m / (k * pi)) * SIN(k * r)
! Note: J can be any real number
! Note: for z = 0 returns incorrect result
!------------------------------------------------------------
  FUNCTION SphericalBesselJ_EN(J, k, m, z)
    REAL(KIND=prec), INTENT(IN) :: J, k, m
    COMPLEX(KIND=prec), INTENT(IN) :: z
    COMPLEX(KIND=prec) :: SphericalBesselJ_EN
  !---
    SphericalBesselJ_EN = SQRT(2.0_prec * m * k / pi) * SphericalBessel('J', J * zone, k * z) * z
    RETURN
  END FUNCTION SphericalBesselJ_EN

! Calculation of spherical Bessel function j_n(x)
!   for integer 'n' and real 'x'
! Note: for dx = 0 returns incorrect result
!------------------------------------------------
  FUNCTION SphericalBessel_ID(ch, in, dx, df)
    CHARACTER(LEN=*), INTENT(IN) :: ch
    INTEGER, INTENT(IN) :: in
    REAL(KIND=prec), INTENT(IN) :: dx
    COMPLEX(KIND=prec) :: SphericalBessel_ID
    COMPLEX(KIND=prec), OPTIONAL :: df
  !---
    IF (PRESENT(df)) THEN
      SphericalBessel_ID = SphericalBessel_ZZ(ch, in * zone, dx * zone, df)
    ELSE
      SphericalBessel_ID = SphericalBessel_ZZ(ch, in * zone, dx * zone)
    END IF
    RETURN
  END FUNCTION SphericalBessel_ID

! Calculation of spherical Bessel function j_n(x)
!   for integer 'n' and complex 'x'
! Note: for zx = 0 returns incorrect result
!------------------------------------------------
  FUNCTION SphericalBessel_IZ(ch, in, zx, df)
    CHARACTER(LEN=*), INTENT(IN) :: ch
    INTEGER, INTENT(IN) :: in
    COMPLEX(KIND=prec), INTENT(IN) :: zx
    COMPLEX(KIND=prec) :: SphericalBessel_IZ
    COMPLEX(KIND=prec), OPTIONAL :: df
  !---
    IF (PRESENT(df)) THEN
      SphericalBessel_IZ = SphericalBessel_ZZ(ch, in * zone, zx, df)
    ELSE
      SphericalBessel_IZ = SphericalBessel_ZZ(ch, in * zone, zx)
    END IF
    RETURN
  END FUNCTION SphericalBessel_IZ

! Calculation of spherical Bessel function j_n(x)
!   for complex 'n' and complex 'x'
! Note: for zx = 0 returns incorrect result
!------------------------------------------------
  FUNCTION SphericalBessel_ZZ(ch, zn, zx, df)
    CHARACTER(LEN=*), INTENT(IN) :: ch
    COMPLEX(KIND=prec), INTENT(IN) :: zn, zx
    COMPLEX(KIND=prec) :: SphericalBessel_ZZ
    COMPLEX(KIND=prec), OPTIONAL :: df
  !---
    COMPLEX(KIND=prec) :: ZFC(1), ZGC(1), ZFCP(1), ZGCP(1), ZSIG(1)
    INTEGER :: MODE
    INTEGER :: IFAIL = 0
  !---
  ! see coulcc.f for explanation
    SELECT CASE (ch)
    CASE ('y', 'Y')
      MODE = 2
    CASE ('h+', 'H+')
      MODE = 12
    CASE ('h-', 'H-')
      MODE = 22
    CASE DEFAULT ! spherical Bessel function j_n(x) returned as defualt
      MODE = 4
    END SELECT
    IF (PRESENT(df)) MODE = MODE - 1
    CALL COULCC(zx, zero, zn, 1, ZFC, ZGC, ZFCP, ZGCP, ZSIG, MODE, 1, IFAIL)
    IF (IFAIL > 0) THEN
      WRITE(*,'(A)') 'Error: COULCC failed to calculate spherical Bessel function '//ch
      WRITE(*,'(A,1P 1E13.5E2,A,1P 1E13.5E2,A,1P 1E13.5E2,A,1P 1E13.5E2,A)') 'for n = (', REAL(zn), ',', AIMAG(zn), ') and z = (', REAL(zx), ',', AIMAG(zx), ')'
      STOP
    END IF
    SELECT CASE (ch)
    CASE ('y', 'Y', 'h+', 'H+', 'h-', 'H-')
      SphericalBessel_ZZ = ZGC(1)
      IF (PRESENT(df)) df = ZGCP(1)
    CASE DEFAULT
      SphericalBessel_ZZ = ZFC(1)
      IF (PRESENT(df)) df = ZFCP(1)
    END SELECT
    RETURN
  END FUNCTION SphericalBessel_ZZ

!-------------------------------
! Cylindrical Bessel functions !
!------------------------------!

! Calculation of cylindrical Bessel function J_0(x) for real(8) 'x'
!   see Numerical Recipes for axplanation
!------------------------------------------------------------------
  FUNCTION CylindricalBesselJ0_D(x)
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec) :: CylindricalBesselJ0_D
  !---
    REAL(KIND=prec) :: ax, xx, z, zz, numer, denom, numer2, denom2
    ax = ABS(x)
    IF (ax < 8.0_prec) THEN
      xx = x * x
      zz = 64.0_prec - xx
      numer = Poly_DD1(zz, (/ 1.682397144220462e-4_prec, 2.058861258868952e-5_prec, 5.288947320067750e-7_prec, 5.557173907680151e-9_prec, 2.865540042042604e-11_prec, 7.398972674152181e-14_prec, 7.925088479679688e-17_prec /) )
      denom = Poly_DD1(xx, (/ 1.0_prec, 1.019685405805929e-2_prec, 5.130296867064666e-5_prec, 1.659702063950243e-7_prec, 3.728997574317067e-10_prec, 5.709292619977798e-13_prec, 4.932979170744996e-16_prec /) )
      CylindricalBesselJ0_D = (xx - 5.783185962946785_prec) * (xx - 3.047126234366209e1_prec) * numer / denom
    ELSE
      z  = 8.0_prec / ax;
      zz = z * z;
      xx = ax - pio4;
      numer  = Poly_DD1(zz, (/ 1.0_prec, 1.039698629715637_prec, 2.576910172633398e-1_prec, 1.504152485749669e-2_prec, 1.052598413585270e-4_prec /) )
      denom  = Poly_DD1(zz, (/ 1.0_prec, 1.040797262528109_prec, 2.588070904043728e-1_prec, 1.529954477721284e-2_prec, 1.168931211650012e-4_prec /) )
      numer2 = Poly_DD1(zz, (/ -1.562499999999992e-2_prec, -1.920039317065641e-2_prec,	-5.827951791963418e-3_prec, -4.372674978482726e-4_prec, -3.895839560412374e-6_prec /) )
      denom2 = Poly_DD1(zz, (/ 1.0_prec, 1.237980436358390_prec, 3.838793938147116e-1_prec, 3.100323481550864e-2_prec, 4.165515825072393e-4_prec /) )
      CylindricalBesselJ0_D = SQRT(twoopi / ax) * (COS(xx)* numer / denom - z * SIN(xx) * numer2 / denom2)
    END IF
    RETURN
  END FUNCTION CylindricalBesselJ0_D

! Calculation of cylindrical Bessel function J_1(x) for real(8) 'x'
!   see Numerical Recipes for axplanation
!------------------------------------------------------------------
  FUNCTION CylindricalBesselJ1_D(x)
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec) :: CylindricalBesselJ1_D
  !---
    REAL(KIND=prec) :: ax, xx, z, zz, numer, denom, numer2, denom2
    ax = ABS(x)
    IF (ax < 8.0_prec) THEN
      xx = x * x
      zz = 64.0_prec - xx
      numer = Poly_DD1(zz, (/ 7.309637831891357e-5_prec, 3.551248884746503e-6_prec, 5.820673901730427e-8_prec, 4.500650342170622e-10_prec, 1.831596352149641e-12_prec, 3.891583573305035e-15_prec, 3.524978592527982e-18_prec /) )
      denom = Poly_DD1(xx, (/ 1.0_prec, 9.398354768446072e-3_prec, 4.328946737100230e-5_prec, 1.271526296341915e-7_prec, 2.566305357932989e-10_prec, 3.477378203574266e-13_prec, 2.593535427519985e-16_prec /) )
      CylindricalBesselJ1_D = x * (xx - 1.468197064212389e1_prec) * (xx - 4.921845632169460e1_prec) * numer / denom
    ELSE
      z  = 8.0_prec / ax;
      zz = z * z;
      xx = ax - 3.0_prec * pio4;
      numer  = Poly_DD1(zz, (/ 1.0_prec, 1.014039111045313_prec, 2.426762348629863e-1_prec, 1.350308200342000e-2_prec, 9.516522033988099e-5_prec /) )
      denom  = Poly_DD1(zz, (/ 1.0_prec, 1.012208056357845_prec, 2.408580305488938e-1_prec, 1.309511056184273e-2_prec, 7.746422941504713e-5_prec /) )
      numer2 = Poly_DD1(zz, (/ 4.687499999999991e-2_prec, 5.652407388406023e-2_prec, 1.676531273460512e-2_prec, 1.231216817715814e-3_prec, 1.178364381441801e-5_prec /) )
      denom2 = Poly_DD1(zz, (/ 1.0_prec, 1.210119370463693_prec, 3.626494789275638e-1_prec, 2.761695824829316e-2_prec, 3.240517192670181e-4_prec /) )
      CylindricalBesselJ1_D = SQRT(twoopi / ax) * (COS(xx)* numer / denom - z * SIN(xx) * numer2 / denom2)
      IF (x < 0.0_prec) CylindricalBesselJ1_D = -CylindricalBesselJ1_D
    END IF
    RETURN
  END FUNCTION CylindricalBesselJ1_D

  FUNCTION CylindricalBesselJN_D(n, x)
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec) :: CylindricalBesselJN_D
  !---
    INTEGER, PARAMETER :: IACC = 160
    REAL(KIND=prec), PARAMETER :: BIGNO = 1.0e10_prec
    REAL(KIND=prec), PARAMETER :: BIGNI = 1.0e-10_prec
    INTEGER :: j, jsum, m
    REAL(KIND=prec) :: ax, bj, bjm, bjp, sum, tox
  !---
    ! IF (n < 2) PAUSE 'Warning: bad argument n in CylindricalBesselJN_D'
    ax = ABS(x)
    IF (ax == 0.0_prec) THEN
      CylindricalBesselJN_D = 0.0_prec
    ELSE IF (ax > REAL(n, KIND=prec)) THEN
      tox = 2.0_prec / ax
      bjm = CylindricalBesselJ0_D(ax)
      bj  = CylindricalBesselJ1_D(ax)
      DO j = 1, n - 1
        bjp = j * tox * bj - bjm
        bjm = bj
        bj  = bjp
      END DO
      CylindricalBesselJN_D = bj
    ELSE
      tox  = 2.0_prec / ax
      m    = 2 * ((n + INT(SQRT(REAL(IACC * n, KIND=prec)))) / 2)
      jsum = 0
      sum  = 0.0_prec
      bjp  = 0.0_prec
      bj   = 1.0_prec
      CylindricalBesselJN_D = 0.0_prec
      DO j = m, 1, -1
        bjm = j * tox * bj - bjp
        bjp = bj
        bj  = bjm
        IF (ABS(bj) > BIGNO) THEN
          bj    = bj * BIGNI
          bjp   = bjp * BIGNI
          CylindricalBesselJN_D = CylindricalBesselJN_D * BIGNI
          sum   = sum * BIGNI
        END IF
        IF (jsum /= 0) sum = sum + bj
        jsum = 1 - jsum
        IF (j == n) CylindricalBesselJN_D = bjp
      END DO
      sum = 2.0_prec * sum - bj
      CylindricalBesselJN_D = CylindricalBesselJN_D / sum
    endif
    IF (x < 0.0_prec .AND. MOD(n, 2) == 1) CylindricalBesselJN_D = -CylindricalBesselJN_D
    RETURN
  END FUNCTION CylindricalBesselJN_D

  FUNCTION BesselJ(n, x)
    INTEGER, INTENT(IN) :: n 
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec) :: BesselJ
  !---
    SELECT CASE (n)
    CASE (0)
      BesselJ = CylindricalBesselJ0_D(x)
    CASE (1)
      BesselJ = CylindricalBesselJ1_D(x)
    CASE DEFAULT ! (n > 1)
      BesselJ = CylindricalBesselJN_D(n, x)
    END SELECT
    RETURN
  END FUNCTION BesselJ

! Calculation of cylindrical Bessel function J_n(x)
!   for integer 'n' and real 'x'
! Note: for dx = 0 returns incorrect result
!--------------------------------------------------
  FUNCTION CylindricalBessel_ID(ch, in, dx, df)
    CHARACTER(LEN=*), INTENT(IN) :: ch
    INTEGER, INTENT(IN) :: in
    REAL(KIND=prec), INTENT(IN) :: dx
    COMPLEX(KIND=prec) :: CylindricalBessel_ID
    COMPLEX(KIND=prec), OPTIONAL :: df
  !---
    IF (PRESENT(df)) THEN
      CylindricalBessel_ID = CylindricalBessel_ZZ(ch, in * zone, dx * zone, df)
    ELSE
      CylindricalBessel_ID = CylindricalBessel_ZZ(ch, in * zone, dx * zone)
    END IF
    RETURN
  END FUNCTION CylindricalBessel_ID

! Calculation of cylindrical Bessel function j_n(x)
!   for integer 'n' and complex 'x'
! Note: for zx = 0 returns incorrect result
!------------------------------------------------
  FUNCTION CylindricalBessel_IZ(ch, in, zx, df)
    CHARACTER(LEN=*), INTENT(IN) :: ch
    INTEGER, INTENT(IN) :: in
    COMPLEX(KIND=prec), INTENT(IN) :: zx
    COMPLEX(KIND=prec) :: CylindricalBessel_IZ
    COMPLEX(KIND=prec), OPTIONAL :: df
  !---
    IF (PRESENT(df)) THEN
      CylindricalBessel_IZ = CylindricalBessel_ZZ(ch, in * zone, zx, df)
    ELSE
      CylindricalBessel_IZ = CylindricalBessel_ZZ(ch, in * zone, zx)
    END IF
    RETURN
  END FUNCTION CylindricalBessel_IZ

! Calculation of cylindrical Bessel function j_n(x)
!   for complex 'n' and complex 'x'
! Note: for zx = 0 returns incorrect result
!------------------------------------------------
  FUNCTION CylindricalBessel_ZZ(ch, zn, zx, df)
    CHARACTER(LEN=*), INTENT(IN) :: ch
    COMPLEX(KIND=prec), INTENT(IN) :: zn, zx
    COMPLEX(KIND=prec) :: CylindricalBessel_ZZ
    COMPLEX(KIND=prec), OPTIONAL :: df
  !---
    COMPLEX(KIND=prec) :: ZFC(1), ZGC(1), ZFCP(1), ZGCP(1), ZSIG(1)
    INTEGER :: MODE
    INTEGER :: IFAIL = 0
  !---
  ! see coulcc.f for explanation
    SELECT CASE (ch)
    CASE ('y', 'Y')
      MODE = 2
    CASE ('h+', 'H+')
      MODE = 12
    CASE ('h-', 'H-')
      MODE = 22
    CASE DEFAULT ! Cylindrical Bessel function j_n(x) returned as defualt
      MODE = 4
    END SELECT
    IF (PRESENT(df)) MODE = MODE - 1
    CALL COULCC(zx, zero, zn, 1, ZFC, ZGC, ZFCP, ZGCP, ZSIG, MODE, 2, IFAIL)
    IF (IFAIL > 0) THEN
      WRITE(*,'(A)') 'Error: COULCC failed to calculate Cylindrical Bessel function '//ch
      WRITE(*,'(A,1P 1E13.5E2,A,1P 1E13.5E2,A,1P 1E13.5E2,A,1P 1E13.5E2,A)') 'for n = (', REAL(zn), ',', AIMAG(zn), ') and z = (', REAL(zx), ',', AIMAG(zx), ')'
      STOP
    END IF
    SELECT CASE (ch)
    CASE ('y', 'Y', 'h+', 'H+', 'h-', 'H-')
      CylindricalBessel_ZZ = ZGC(1)
      IF (PRESENT(df)) df = ZGCP(1)
    CASE DEFAULT
      CylindricalBessel_ZZ = ZFC(1)
      IF (PRESENT(df)) df = ZFCP(1)
    END SELECT
    RETURN
  END FUNCTION CylindricalBessel_ZZ

! Calculation of Hermite polynomial H_n(x)
!   using recurrence relation
!   for single value x
!-----------------------------------------
  FUNCTION HermitePol1(n, x)
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec) :: HermitePol1
  !---
    REAL(KIND=prec) :: xa(1), Fa(1)
  !---
    xa(1) = x
    Fa = HermitePolN(n, xa)
    HermitePol1 = Fa(1)
    RETURN
  END FUNCTION HermitePol1

! Calculation of Hermite polynomial H_n(x)
!   using recurrence relation
!   for array of values x(:)
!-----------------------------------------
  FUNCTION HermitePolN(n, x)
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec), DIMENSION(:), INTENT(IN) :: x
    REAL(KIND=prec), DIMENSION(SIZE(x)) :: HermitePolN
  !---------------------------------------------
    REAL(KIND=prec), DIMENSION(SIZE(x)) :: ha, hb
    INTEGER :: i
  !---
    IF (n == 0) THEN
      HermitePolN = 1.0_prec
      RETURN
    END IF
    ha = 1.0_prec
    hb = 2.0_prec * x
    DO i = 2, n - 1, 2
      ha = 2.0_prec * (x * hb - (i - 1) * ha)
      hb = 2.0_prec * (x * ha - i * hb)
    END DO      
    IF (MOD(n, 2) == 1) THEN
      HermitePolN = hb
    ELSE
      HermitePolN = 2.0_prec * (x * hb - (n - 1) * ha)
    END IF
    RETURN
  END FUNCTION HermitePolN

!==========================================================!

!---------------------!
! Functions for grids !
!---------------------!

! grid initialization
  SUBROUTINE Grid_Ini(p, n, x_min, x_max)
    TYPE(grid_type), POINTER :: p
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec), INTENT(IN) :: x_min, x_max
  !---
    IF (p%init .eqv. .TRUE.) THEN
      CALL Grid_Clean(p)
    END IF
    p%n = n
    p%x_min = x_min
    p%x_max = x_max
    ALLOCATE(p%x(p%n), p%w(p%n), STAT=error)
    CALL Check_Error(error, 'allocation of the grid')
    p%init = .TRUE.
    RETURN
  END SUBROUTINE Grid_Ini

! grid deallocation
  SUBROUTINE Grid_Clean(p)
    TYPE(grid_type), POINTER :: p
  !---
    IF (p%init .eqv. .TRUE.) THEN
      p%n = 0
      DEALLOCATE(p%x, p%w, STAT=error)
      CALL Check_Error(error, 'deallocation of the grid')
      p%init = .FALSE.
    ELSE
      WRITE(*,'(A)') 'Warning: attempt to deallocate the grid which was not allocated !'
    END IF
    RETURN
  END SUBROUTINE Grid_Clean

! Auxiliary subroutine to build equidistant grid
!   and optionally weights of quadrature based on p-points Newton-Cotes formulas
!   closed Newton-Cotes extended formulas:
!     q = 2 - Trapezoid rule     - O(h^3) (default, n arbitrary)
!     q = 3 - Simpson's rule     - O(h^5) (n = 2k+1, k > 0)
!     q = 4 - Simpson's 3/8 rule - O(h^5) (n = 3k+1, k > 0)
!     q = 5 - Boole's rule       - O(h^7) (n = 4k+1, k > 0)
!     q = 6 - 6-point rule       - O(h^7) (n = 5k+1, k > 0)
!     q = 7 - 7-point rule       - O(h^9) (n = 6k+1, k > 0)
! Integral of f(p%x(:)) in [x_min,x_max] is SUM(p%w * f(p%x)) 
!------------------------------------------------------
  SUBROUTINE Build_Equidistant_Grid(p, n, x_min, x_max, q)
    TYPE(grid_type), POINTER :: p
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec), INTENT(IN) :: x_min, x_max
    INTEGER, INTENT(IN), OPTIONAL :: q
  !---
    INTEGER :: i, j = 2
    REAL(KIND=prec) :: h
  !---
    IF (PRESENT(q)) j = q
    CALL Grid_Ini(p, n, x_min, x_max)

    IF (n == 1) THEN
      h = 0.0_prec
    ELSE
      h = (x_max - x_min) / (n - 1)
    END IF

    DO i = 1, n
      p%x(i) = x_min + h * (i - 1)
    END DO

  ! weights depend on the quadrature chosen
    IF (j < 2) j = 2
    IF (j > 7) j = 7
    IF (j > 2 .AND. MOD(n, j - 1) /= 1) THEN
      WRITE(*,'(A)') 'Warning: inconsistent number of gridpoints and p-point quadrature'
      WRITE(*,'(A,I1,A,I0)') '         it should be n = ', j - 1, 'k + 1, k > 0, but n = ', n
      WRITE(*,'(A)') '         integration will be inaccurate'
    END IF
    SELECT CASE(j)
    CASE(:2)
      p%w(1) = 0.5_prec * h
      p%w(2:n-1) = h
    CASE(3)
      h = h / 3.0_prec
      p%w(1) = h
      p%w(2:n-1:2) = 4.0_prec * h 
      p%w(3:n-2:2) = 2.0_prec * h 
    CASE(4)
      h = h / 8.0_prec
      p%w(1) = 3.0_prec * h
      p%w(2:n:3) = 9.0_prec * h 
      p%w(3:n:3) = 9.0_prec * h 
      p%w(4:n:3) = 6.0_prec * h 
    CASE(5)
      h = h / 45.0_prec
      p%w(1) = 14.0_prec * h
      p%w(2:n:4) = 64.0_prec * h 
      p%w(3:n:4) = 24.0_prec * h 
      p%w(4:n:4) = 64.0_prec * h 
      p%w(5:n:4) = 28.0_prec * h 
    CASE(6)
      h = h / 288.0_prec
      p%w(1) = 95.0_prec * h
      p%w(2:n:5) = 375.0_prec * h 
      p%w(3:n:5) = 250.0_prec * h 
      p%w(4:n:5) = 250.0_prec * h 
      p%w(5:n:5) = 375.0_prec * h 
      p%w(6:n:5) = 190.0_prec * h 
    CASE(7:)
      h = h / 140.0_prec
      p%w(1) = 41.0_prec * h
      p%w(2:n:6) = 216.0_prec * h 
      p%w(3:n:6) = 27.0_prec * h 
      p%w(4:n:6) = 272.0_prec * h 
      p%w(5:n:6) = 27.0_prec * h 
      p%w(6:n:6) = 216.0_prec * h 
      p%w(7:n:6) = 82.0_prec * h 
    END SELECT
    p%w(n) = p%w(1)
    
    RETURN
  END SUBROUTINE Build_Equidistant_Grid

! Basic 1D grid
!   which - 1 - Gauss-Legendre (default)
!           2 - Gauss-Lobatto
!   n - number of points
!   x_min, x_max - endpoints of the grid - optional
!                - if not specified then defualt is
!                  which = 1,2 => [-1,1]
!---------------------------------------------------------------
  SUBROUTINE Build_Gauss_Grid(p, n, x_min, x_max, which)
    TYPE(grid_type), POINTER :: p
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec), INTENT(IN), OPTIONAL :: x_min, x_max
    INTEGER, INTENT(IN), OPTIONAL :: which
  !---
    REAL(KIND=prec) :: h
    INTEGER :: wh = 1
  !---
    IF (p%init .eqv. .TRUE.) CALL Grid_Clean(p)

  ! allocation of arrays
    p%n = n
    ALLOCATE(p%x(n), p%w(n), STAT=error)
    CALL Check_Error(error, 'allocation of a basic grid')

    IF (PRESENT(which)) wh = which

  ! determination of limits
    IF (PRESENT(x_min)) THEN
      p%x_min = x_min
    ELSE
      p%x_min = -1.0_prec
    END IF
    IF (PRESENT(x_max)) THEN
      p%x_max = x_max
    ELSE
      p%x_max = 1.0_prec
    END IF

  ! calculation of grid points and weights
    SELECT CASE(wh)
    CASE(1)
      CALL GLe_Quad(n, p%x, p%w)
    CASE(2)
      CALL GLo_Quad(n, p%x, p%w)
    END SELECT

  ! scaling to the interval
    IF ((PRESENT(x_min) .OR. PRESENT(x_max))) THEN
      p%x = 0.5_prec * (p%x_min + p%x_max + (p%x_max - p%x_min) * p%x)
      p%w = 0.5_prec * (p%x_max - p%x_min) * p%w
    END IF
    p%init = .TRUE.
    RETURN
  END SUBROUTINE Build_Gauss_Grid

! Returns abscissas and weights of the Gauss-Lobatto
! N-point quadrature in the interval (-1,1)
!-----------------------------------------------------
  SUBROUTINE GLo_Quad(n, x, w)
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec), INTENT(OUT), DIMENSION(n) :: x, w
  !---
    INTEGER :: kind, kpts
    REAL(KIND=prec) :: alpha, beta, endpts(2)
    REAL(KIND=prec), DIMENSION(n) :: scr ! used only in GaussQuad
  !---
    alpha = 0.0_prec 
    beta  = 0.0_prec
    endpts(1) = -1.0_prec
    endpts(2) =  1.0_prec
    kind = 1  ! Legendre polynomials
    kpts = 2  ! Lobatto - endpts included in grid
    call GaussQuad(kind, n, alpha, beta, kpts, endpts, scr, x, w)
    x(1) = -1.0_prec
    IF (MOD(n, 2) == 1) x((n+1) / 2) = 0.0_prec
    x(n) =  1.0_prec

    RETURN
  END SUBROUTINE GLo_Quad

! Returns abscissas and weights of the Gauss-Legendre
! N-point quadrature in the interval (-1,1)
!-----------------------------------------------------
  SUBROUTINE GLe_Quad(n, x, w)
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec), INTENT(OUT), DIMENSION(n) :: x, w
  !---
    INTEGER :: kind, kpts
    REAL(KIND=prec) :: alpha, beta, endpts(2)
    REAL(KIND=prec), DIMENSION(n) :: scr ! used only in GaussQuad
  !---
    alpha = 0.0_prec 
    beta  = 0.0_prec
    endpts(1) = -1.0_prec
    endpts(2) =  1.0_prec
    kind = 1  ! Legendre polynomials
    kpts = 0  ! endpts not included in grid
    call GaussQuad(kind, n, alpha, beta, kpts, endpts, scr, x, w)

    RETURN
  END SUBROUTINE GLe_Quad

  SUBROUTINE GaussQuadBind(kind, n, alpha, beta, kpts, endpts, b, t, w) BIND(C,NAME='GaussQuadBind')
    
    INTEGER(KIND=C_INT), INTENT(IN) :: kind, n, kpts
    REAL(KIND=C_DOUBLE), INTENT(IN) :: alpha, beta, endpts(2)
    REAL(KIND=C_DOUBLE), INTENT(INOUT) :: b(n), t(n)
    REAL(KIND=C_DOUBLE), INTENT(OUT) :: w(n)
    call GaussQuad(kind, n, alpha, beta, kpts, endpts, b, t, w)

  END SUBROUTINE GaussQuadBind

! Returns abscissas and weights of the n-point 
! quadrature in the interval (endpts(1),endpts(2))
! Variables 'kind' and 'kpts' determine the type of the quadrature
! kind = 0: equidistant grid and Simpson's rule (n must be odd)
!        1: Gauss_Legendre (kpts = 2: Gauss-Lobatto)
!        2: Gauss_Chebyshev of the first kind
!        3: Gauss_Chebyshev of the second kind
!        4: Gauss_Hermite
!        5: Gauss_Jacobi
!        6: Gauss_Laguerre
! alpha is used only for Jacobi and Laguerre
! beta  is used only for Jacobi
!-----------------------------------------------------
  SUBROUTINE GaussQuad(kind, n, alpha, beta, kpts, endpts, b, t, w)
    INTEGER, INTENT(IN) :: kind, n, kpts
    REAL(KIND=prec), INTENT(IN) :: alpha, beta, endpts(2)
    REAL(KIND=prec), INTENT(INOUT), DIMENSION(n) :: b, t
    REAL(KIND=prec), INTENT(OUT), DIMENSION(n) :: w
  !---
    INTEGER :: i, ierr, nm1
    REAL(KIND=prec) :: muzero
    REAL(KIND=prec) :: gam, t1, h, h3
  !---
    nm1 = n - 1
  ! Simpson's rule  
    IF (kind == 0) THEN
      IF (MOD(n, 2) == 0) then
        WRITE(*,*) " n must be odd for Simpson's rule ", n
        STOP
      END IF
      IF (n == 1) THEN
        t(1) = 0.0_prec
        w(1) = 2.0_prec
      ELSE
        h = 2.0_prec / (n - 1)
        h3 = h / 3.0_prec
        t(1) = -1.0_prec
        t(n) =  1.0_prec
        w(1) = h3
        w(n) = h3
        DO i = 2, nm1
          t(i) = t(i - 1) + h
          w(i) = 4.0_prec - 2.0_prec * MOD(i, 2)
          w(i) = w(i) * h3
        END DO
      END IF

  ! Other quadratures
    ELSE
      CALL RecCoef(kind, n, alpha, beta, b, t, muzero)

    ! The matrix of coefficients is assumed to be symmetric.
    ! The array t contains the diagonal elements,
    ! the array b the off-diagonal elements.
    ! Make appropriate changes in the lower right 2 by 2
    ! submatrix if endpoints are to be part of the grid

      SELECT CASE (kpts)

      CASE(1) ! if kpts = 1, only t(n) must be changed

        t(n)   = gbshift(endpts(1)) * b(nm1)**2 + endpts(1)

      CASE(2) ! if kpts = 2, t(n) and b(n-1) must be recomputed

        gam    = gbshift(endpts(1))
        t1     = ((endpts(1) - endpts(2)) / (gbshift(endpts(2)) - gam))
        b(nm1) = SQRT(t1)
        t(n)   = endpts(1) + gam * t1

      END SELECT

    ! Note that the indices of the elements of b run from 1 to n-1
    ! and thus the value of b(n) is arbitrary.
    ! Now compute the eigenvalues of the symmetric tridiagonal
    ! matrix, which has been modified as necessary.
    ! The method used is a QL-type method with origin shifting

      w    = 0.0_prec
      w(1) = 1.0_prec
     
      CALL gbtql2(n, t, b, w, ierr)

      w = muzero * w * w

    END IF
    RETURN

  CONTAINS

  ! Auxiliary function
  !-------------------
    FUNCTION gbshift(shift)
      REAL(KIND=prec) :: gbshift
      REAL(KIND=prec), INTENT(IN) :: shift
      REAL(KIND=prec) :: al
    !---
      al = t(1) - shift
      DO i = 2, nm1
        al = t(i) - shift - b(i-1)**2 / al
      END DO
      gbshift = 1.0_prec / al
      RETURN
    END FUNCTION gbshift

  END SUBROUTINE GaussQuad


! Subroutine RecCoef supplies the coefficients a(j), b(j) of the
! recurrence relation
!
!   b p (x) = (x - a ) p   (x) - b   p   (x)
!    j j            j   j-1       j-1 j-2
!
! for the various classical (normalized) orthogonal polynomials,
! and the zero-th moment
!
!   muzero = integral w(x) dx
!
! of the given polynomial weight function w(x). Since the
! polynomials are orthonormalized, the tridiagonal matrix is
! guaranteed to be symmetric.
!
! The input parameter alpha is used only for Laguerre and
! Jacobi polynomials, and the parameter beta is used only for
! Jacobi polynomials. The Laguerre and Jacobi polynomials
! require the Gamma0to3 function.
!---------------------------------------------------------------
  SUBROUTINE RecCoef(kind, n, alpha, beta, b, a, muzero)
    INTEGER, INTENT(IN) :: kind, n
    REAL(KIND=prec), INTENT(IN) :: alpha, beta
    REAL(KIND=prec), INTENT(OUT), DIMENSION(n) :: b, a
    REAL(KIND=prec), INTENT(OUT) :: muzero
    INTEGER :: i, nm1
    REAL(KIND=prec) :: ab, abi, a2b2
  !---
    nm1 = n - 1

    SELECT CASE (kind)

    CASE(1)  ! Legendre polynomials p(x)
             ! on (-1, +1), w(x) = 1
      muzero = 2.0_prec
      a = 0.0_prec
      DO i = 1, nm1
        b(i) = i / SQRT(4.0_prec * i * i - 1.0_prec)
      END DO

    CASE(2)  ! Chebyshev polynomials of the first kind t(x)
             ! on (-1, +1), w(x) = 1 / sqrt(1 - x*x)
      muzero = pi
      a = 0.0_prec
      b(1) = SQRT(0.5_prec)
      DO i = 2, nm1
        b(i) = 0.5_prec
      END DO
 
    CASE(3)  ! Chebyshev polynomials of the second kind u(x)
             ! on (-1, +1), w(x) =  sqrt(1 - x*x)
      muzero = pi / 2.0_prec
      a = 0.0_prec
      DO i = 1, nm1
        b(i) = 0.5_prec
      END DO
  
    CASE(4)  ! Hermite polynomials
             ! on (-infinity, +infinity), w(x) =  exp(-x**2)
      muzero = SQRT(pi)
      a = 0.0_prec
      DO i = 1, nm1
        b(i) = SQRT(i / 2.0_prec)
      END DO

    CASE(5)  ! Jacobi polynomials p(alpha, beta)(x), alpha and beta greater than -1
             ! on (-1, +1), w(x) = (1-x)**alpha + (1+x)**beta

      ab  = alpha + beta
      abi = 2.0_prec + ab
      muzero = 2.0_prec**(ab + 1.0_prec) * Gamma0to3(alpha + 1.0_prec) * &
             & Gamma0to3(beta + 1.0_prec) / Gamma0to3(abi)
      a(1) = (beta - alpha) / abi
      b(1) = SQRT(4.0_prec * (1.0_prec + alpha) * (1.0_prec + beta) / &
           & ((abi + 1.0_prec) * abi * abi))
      a2b2 = beta * beta - alpha * alpha
      DO i = 2, nm1
        abi  = 2.0_prec * i + ab
        a(i) = a2b2 / ((abi - 2.0_prec) * abi)
        b(i) = SQRT(4.0_prec * i * (i + alpha) * (i + beta) * (i + ab) / &
             & ((abi * abi - 1) * abi * abi))
      END DO
      abi  = 2.0_prec * n + ab
      a(n) = a2b2 / ((abi - 2.0_prec) * abi)

    CASE(6)  ! Laguerre polynomials l(alpha)(x), alpha greater than -1
             ! on (0, +infinity), w(x) = exp(-x) * x**alpha

      muzero = Gamma0to3(alpha + 1.0_prec)
      DO i = 1, nm1
        a(i) = 2.0_prec * i - 1.0_prec + alpha
        b(i) = SQRT(i * (i + alpha))
      END DO
      a(n) = 2.0_prec * n - 1 + alpha

    END SELECT

    RETURN
  END SUBROUTINE RecCoef


! Subroutine gbtql2 is a translation of the algol procedure imtql2,
! num. math. 12, 377-383(1968) by martin and wilkinson,
! as modified in num. math. 15, 450(1970) by dubrulle.
! handbook for auto. comp., vol.ii-linear algebra, 241-248(1971).
!
! This subroutine finds the eigenvalues and first components of the
! eigenvectors of a symmetric tridiagonal matrix by the implicit QL
! method, and is adapted from the eispak routine imtql2
!
! on input=
!
!    n is the order of the matrix;
!
!    d contains the diagonal elements of the input matrix;
!
!    e contains the subdiagonal elements of the input matrix
!      in its first n-1 positions.  e(n) is arbitrary;
!
!    z contains the first row of the identity matrix.
!
! on output=
!
!    d contains the eigenvalues in ascending order; if an
!      error exit is made, the eigenvalues are correct but
!      unordered for indices 1, 2, ..., ierr-1;
!
!    e has been destroyed;
!
!    z contains the first components of the orthonormal eigenvectors
!      of the symmetric tridiagonal matrix; if an error exit is
!      made, z contains the eigenvectors associated with the stored
!      eigenvalues;
!
!    ierr is set to
!      zero       for normal return,
!      j          if the j-th eigenvalue has not been
!                 determined after 30 iterations.
!-------------------------------------------------------------------
  SUBROUTINE gbtql2(n, d, e, z, ierr)
    INTEGER, INTENT(IN) :: n
    REAL(KIND=prec), INTENT(INOUT), DIMENSION(n) :: d, e, z
    INTEGER, INTENT(OUT) :: ierr
  !---
    INTEGER :: i, j, k, l, m, ii, mml
    REAL(KIND=prec) :: machep, c, f, b, s, p, g, r
  !---

    !     ========== machep is a machine dependent parameter specifying
    !                the relative precision of floating point arithmetic.
    !                machep = 16.0d0**(-13) for long form arithmetic
    !                on s360 ==========
    machep=1.0e-14_prec

    ierr = 0
    IF (n == 1) RETURN

    e(n) = 0.0_prec
    DO l = 1, n
      j = 0

    ! ========== Look for small sub-diagonal element ==========
  105 DO m = l, n - 1
        IF (ABS(e(m)) <= machep * (ABS(d(m)) + ABS(d(m+1)))) EXIT
      END DO

      p = d(l)
      IF (m == l) GO TO 240
      IF (j == 30) GO TO 1000
      j = j + 1
    ! ========== form shift ==========
      g = (d(l+1) - p) / (2.0_prec   * e(l))
      r =  SQRT(g*g+1.0_prec  )
      g = d(m) - p + e(l) / (g +  SIGN(r, g))
      s = 1.0_prec
      c = 1.0_prec
      p = 0.0_prec
      mml = m - l
    ! ========== for i=m-1 step -1 until l DO -- ==========
      DO ii = 1, mml
        i = m - ii
        f = s * e(i)
        b = c * e(i)
        IF ( ABS(f) .lt.  ABS(g)) GO TO 150
        c = g / f
        r =  SQRT(c*c+1.0_prec  )
        e(i+1) = f * r
        s = 1.0_prec   / r
        c = c * s
        GO TO 160
  150   s = f / g
        r =  SQRT(s*s+1.0_prec  )
        e(i+1) = g * r
        c = 1.0_prec   / r
        s = s * c
  160   g = d(i+1) - p
        r = (d(i) - g) * s + 2.0_prec   * c * b
        p = s * r
        d(i+1) = g + p
        g = c * r - b
      ! ========== form first component of vector ==========
        f = z(i+1)
        z(i+1) = s * z(i) + c * f
        z(i) = c * z(i) - s * f
      END DO
      d(l) = d(l) - p
      e(l) = g
      e(m) = 0.0_prec
      GO TO 105
240 END DO
    ! ========== order eigenvalues and eigenvectors ==========
    DO ii = 2, n
      i = ii - 1
      k = i
      p = d(i)
      DO j = ii, n
        IF (d(j) .ge. p) GO TO 260
        k = j
        p = d(j)
260   END DO
      IF (k == i) GO TO 300
      d(k) = d(i)
      d(i) = p
      p = z(i)
      z(i) = z(k)
      z(k) = p
300 END DO
    GO TO 1001
    !     ========== set error -- no convergence to an
    !                eigenvalue after 30 iterations ==========
1000 ierr = l
1001 RETURN

  END SUBROUTINE gbtql2

! Euler's function Gamma (used for Laguerre and Jacobi polynomials)
!   based on a Chebyshev-type polynomial approximation given in 
!     H. Werner and R. Collinge, Math. Comput. 15 (1961), pp. 195-97
!   accurate up to 16 significant digits in the interval [0,3]
!   (approximations to the gamma function, accurate up to 18 significant
!   digits, may be found in the paper quoted above)
!--------------------------------------------------------------------
  FUNCTION  Gamma0to3(x)
    REAL(KIND=prec) :: Gamma0to3
    REAL(KIND=prec), INTENT(IN) :: x
    REAL(KIND=prec) :: a(18)
    REAL(KIND=prec) :: p, t
    INTEGER :: i

    a(1)  = 1.0_prec
    a(2)  =  .4227843350984678_prec
    a(3)  =  .4118403304263672_prec
    a(4)  =  .0815769192502609_prec
    a(5)  =  .0742490106800904_prec
    a(6)  = -.0002669810333484_prec
    a(7)  =  .0111540360240344_prec
    a(8)  = -.0028525821446197_prec
    a(9)  =  .0021036287024598_prec
    a(10) = -.0009184843690991_prec
    a(11) =  .0004874227944768_prec
    a(12) = -.0002347204018919_prec
    a(13) =  .0001115339519666_prec
    a(14) = -.0000478747983834_prec
    a(15) =  .0000175102727179_prec
    a(16) = -.0000049203750904_prec
    a(17) =  .0000009199156407_prec
    a(18) = -.0000000839940496_prec
  
    IF (x <= 1.0_prec) THEN
      t = x
    ELSE IF (x <= 2.0_prec) THEN
      t = x - 1.0_prec
    ELSE
      t = x - 2.0_prec
    END IF

    p = a(18)
    DO  i = 17,1,-1
      p = t * p + a(i)
    END DO

    IF (x <= 1.0_prec) THEN
      Gamma0to3 = p / (x * (x + 1.0_prec))
    ELSE IF (x <= 2.0_prec) THEN
      Gamma0to3 = p / x
    ELSE
      Gamma0to3 = p
    END IF

    RETURN
  END FUNCTION Gamma0to3

END MODULE general

