// use criterion::{criterion_group, criterion_main, Criterion};
// use iterate::{
//     primitives::{precond::MulVec, Complex, ComplexVector, FullMatrix, FullVector, Scalar, Vector},
//     solvers::{cocr::COCRSolver, iterative::IterativeMethod},
// };
// use num_traits::{FromPrimitive, Num, Zero};
// use rand::Rng;

// fn solve<T, M>(a: &M, b: &DVector<T>)
// where
//     T: Scalar + FromPrimitive + 'static,
//     M: MulVec<FullVector<Complex<T>>, Output = FullVector<Complex<T>>> + Sync + Send,
// {
//     let mut solver = COCRSolver::new(a, b, FullVector::<Complex<T>>::zero(b.rows()), None);
//     let b_len_squared: T = b.cplx_len_squared().sqrt();
//     let threshold = T::from_f32(2e-5).unwrap();

//     loop {
//         if let iterate::solvers::iterative::StepResult::Precision(prec) = solver.step() {
//             if prec.len_squared().sqrt() / b_len_squared < threshold {
//                 break;
//             }
//         } else {
//             break;
//         }
//     }
// }

// fn gen_cocr_system<T: Num + Copy + Send + Sync + FromPrimitive + 'static>(
//     n: usize,
// ) -> (FullMatrix<Complex<T>>, FullVector<Complex<T>>) {
//     let mut rand: rand::rngs::StdRng = rand::SeedableRng::seed_from_u64(n as u64);

//     let a = FullMatrix::<Complex<T>>::new(n, {
//         let mut data = vec![Complex::<T>::zero(); n.pow(2)];

//         for i in 0..n {
//             for j in i..n {
//                 let factor = ((i as f64 - j as f64).powi(2) / -16.0).exp();
//                 let val = Complex::<T>::new(
//                     T::from_f64(rand.gen::<f64>() * factor).unwrap(),
//                     T::from_f64(rand.gen::<f64>() * factor).unwrap(),
//                 );

//                 data[i + j * n] = val;
//                 data[j + i * n] = val;
//             }
//         }

//         data.into_boxed_slice()
//     });

//     let b = FullVector::<Complex<T>>::new(
//         (0..n)
//             .map(|_| Complex::<T>::new(T::one(), T::one()))
//             .collect(),
//     );

//     (a, b)
// }

// fn criterion_benchmark(c: &mut Criterion) {
//     for n in [100, 200] {
//         // c.bench_function(
//         //     format!("n = {}, C32", n).as_str(),
//         //     move |bencher| {
//         //         let (a, b) = gen_cocr_system::<f32>(n);

//         //         bencher.iter(|| solve(&a, &b))
//         //     }
//         // );

//         c.bench_function(format!("n = {}, C64", n).as_str(), move |bencher| {
//             let (a, b) = gen_cocr_system::<f64>(n);

//             bencher.iter(|| solve(&a, &b))
//         });

//         c.bench_function(
//             format!("n = {}, C64, preconditioned", n).as_str(),
//             move |bencher| {
//                 let (a, b) = gen_cocr_system::<f64>(n);

//                 let a = iterate::primitives::precond::symmetric_incomplete_cholesky(&a);

//                 bencher.iter(|| solve(&a, &b))
//             },
//         );
//     }
// }

// criterion_group!(benches, criterion_benchmark);
// criterion_main!(benches);
