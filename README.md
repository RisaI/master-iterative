# Iterative solver

## How to build
This package requires the nightly toolchain. You can set an override for this particular project by running `rustup override set nightly`. Then you can use `cargo build` or `cargo run`.

When updating the nightly toolchain, this project can sometimes break on incremental compilation/stale libraries. Run `cargo clean` and `cargo check` after updating your toolchain.

## How to use
Run with `cargo run -- PARAMS`. You can always use the `--help` flag.

Examples:
* `cargo run -- 2d 0 0 --no-gui --order=9`
* `cargo run -- 2d 0 0 --no-gui --model=no --view-model=wavefuncs-in --order=20`

You can leverage the `RUSTFLAGS='-C target-cpu=native'` environmental variable to enable CPU specific optimizations. Also run with `cargo run --release` for link-time optimizations.