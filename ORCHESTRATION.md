# Thesis orchestration details
__Target precision__: $|r| / |b| = 10^{-6}$

## Two-channel 1D model

### Model quality
Analytic vs numeric for quadrature order:
* 5
* 10
* 15
* 20

### Benchmark
Compare for GL orders above:
* No preconditioning
* ILU0
* Direct method

For all points plot:

__Run time:__ t(E), _might require statistics

__Required steps:__ STEPS(E)

Select quadrature order, then for preselect energies:

__Convergence graph:__ Prec(STEP)

## 2D model
Variants
* $N_2$
* $NO$
* $O_2$

### Benchmark
Compare:
* No preconditioning
* ILU0
* Partial inverse
* _? Direct ?_