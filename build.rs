use glob::glob;
use std::env;
use std::{error::Error, process::Command};

fn main() -> Result<(), Box<dyn Error>> {
    let out_dir = env::var("OUT_DIR").unwrap();

    println!("cargo:rerun-if-changed=fortran/");

    for entry in glob("fortran/*.f")? {
        let entry = entry.unwrap();

        let output = {
            let mut c = entry.clone();
            c.set_extension("o");

            String::from(c.file_name().unwrap().to_str().unwrap())
        };

        run(Command::new("gfortran")
            .arg("-ffree-line-length-none")
            .args(&["-static", "-c", "-Lgfortran", "-fPIC", "-J", &out_dir])
            .args(&["-o", &format!("{}/{}", out_dir, output)[..]])
            .arg(entry.as_path()));
    }

    for entry in glob("fortran/*.f90")? {
        let entry = entry.unwrap();

        let output = {
            let mut c = entry.clone();
            c.set_extension("o");

            String::from(c.file_name().unwrap().to_str().unwrap())
        };

        run(Command::new("gfortran")
            .arg("-ffree-line-length-none")
            .args(&["-static", "-c", "-fPIC", "-std=f2003", "-J", &out_dir])
            .args(&["-o", &format!("{}/{}", out_dir, output)])
            .arg(entry.as_path()));
    }

    // run(Command::new("gfortran")
    //     .arg("-shared")
    //     .args(&["-J", &out_dir, "-o", &format!("{}/libgeneral.so", out_dir)])
    //     .args(glob(&format!("{}/*.o", out_dir))?.map(|x| x.unwrap()))
    // );

    run(Command::new("ar")
        .arg("cr")
        .arg(&format!("{}/libgeneral.a", out_dir))
        .args(glob(&format!("{}/*.o", out_dir))?.map(|x| x.unwrap())));

    run(Command::new("gfortran")
        .stdout(std::fs::File::create(&format!("{}/general.h", out_dir))?)
        .args(&[
            "-std=f2003",
            "-ffree-line-length-none",
            "-fc-prototypes",
            "-fsyntax-only",
            "-J",
            &out_dir,
        ])
        .args(glob("fortran/*.f90")?.map(|x| x.unwrap())));

    let bindings = bindgen::Builder::default()
        .trust_clang_mangling(false)
        .header(&format!("{}/general.h", out_dir))
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Failed to build bindings.");

    bindings.write_to_file(std::path::PathBuf::from(&format!("{}/general.rs", out_dir)))?;

    println!("cargo:rustc-link-search={}", out_dir);
    println!("cargo:rustc-link-lib=gfortran");
    println!("cargo:rustc-link-lib=static=general");

    Result::Ok(())
}

fn run(cmd: &mut Command) {
    match cmd.status() {
        Ok(status) => {
            if !status.success() {
                panic!("Status code: {}", status)
            }
        }
        Err(error) => panic!("{}", error.to_string()),
    }
}
