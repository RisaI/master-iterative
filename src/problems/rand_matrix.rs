use super::Problem;
use crate::{
    primitives::{FullMatrix, FullVector, Vector},
    solvers::iterative::{self, IterativeMethod},
};
use clap::{App, ArgMatches};
use num_traits::Zero;

const MAX_ITERS: usize = 10_000;

// type Subnumeric = f128::f128;
type Subnumeric = f64;
type Numeric = crate::primitives::Complex<Subnumeric>;

pub fn get_problems() -> Vec<Problem> {
    [Problem {
        id: "rand-matrix".into(),
        arg: App::new("rand-matrix"),
        func: solve_random,
    }]
    .into()
}

fn solve_random(_: &ArgMatches) {
    use rand::Rng;

    let mut rand: rand::rngs::StdRng = rand::SeedableRng::seed_from_u64(123);
    let n = 1000;
    let a = FullMatrix::<Numeric>::new(n, {
        let mut data = vec![Numeric::zero(); n.pow(2)];

        for i in 0..n {
            for j in i..n {
                let strongly_diagonal =
                    crate::generators::matrix::random_strongly_diagonal_elem(i, j);
                let val = Numeric::new(strongly_diagonal, 0.1 * rand.gen::<f64>());

                data[i + j * n] = val;
                data[j + i * n] = val;
            }
        }

        data.into_boxed_slice()
    });

    let b = FullVector::from(
        (0..n)
            .map(|_| {
                Numeric::new(
                    num_traits::FromPrimitive::from_f64(1.0f64).unwrap(),
                    num_traits::FromPrimitive::from_f64(1.0f64).unwrap(),
                )
            })
            .collect::<Vec<_>>(),
    );

    let precond = crate::primitives::precond::ic0::symmetric_incomplete_cholesky(&a);
    let b = precond.apply_to_rhs(&b);
    println!("preconditioned");

    let b_len = b.cplx_len_squared().sqrt().norm();

    let mut solver = iterative::COCRSolver::new(&precond, &b, FullVector::zero(n), None);
    let mut steps = 0;
    let start = std::time::Instant::now();

    loop {
        steps += 1;

        if let iterative::StepResult::Precision(prec) = solver.step() {
            let ratio = prec.norm() / b_len;

            if ratio < 1.2e-5 || steps >= MAX_ITERS {
                println!(
                    "|r| / |b| = {} in {} steps, {}ms",
                    // solver.get_current().data,
                    ratio,
                    steps,
                    std::time::Instant::now().duration_since(start).as_millis(),
                );
                break;
            } else {
                // println!("{}: |r|/|b| = {:.7}", steps, ratio);
            }
        } else {
            break;
        }
    }
}
