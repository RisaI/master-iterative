use std::sync::Arc;

use super::Problem;
use crate::{
    models::{Model1D, TOY_MODEL},
    primitives::{
        grid::{Grid, Quadrature},
        physics::RadialHamiltionian,
        prelude::*,
    },
};
use clap::{App, Arg, ArgMatches};
use plotly::{
    layout::{Axis, AxisType},
    Layout, Plot, Scatter,
};

pub mod calculation;

pub fn get_problems() -> Vec<Problem> {
    [
        Problem {
            id: "multichannel".into(),
            arg: App::new("multichannel").arg(Arg::with_name("direct").short("-d")),
            func: multichannel,
        },
        Problem {
            id: "multichannel-plot".into(),
            arg: App::new("multichannel-plot").arg(
                Arg::with_name("energy")
                    .short("e")
                    .long("energy")
                    .takes_value(true)
                    .required(true),
            ),
            func: multichannel_plot,
        },
        Problem {
            id: "multichannel-ev".into(),
            arg: App::new("multichannel-ev"),
            func: multichannel_ev,
        },
        Problem {
            id: "multichannel-analytic".into(),
            arg: App::new("multichannel-analytic"),
            func: multichannel_analytic,
        },
    ]
    .into()
}

type Numeric = C64;

pub fn build_grid(
    gl_order: usize,
    theta: f64,
) -> crate::primitives::grid::QuadratureGrid<Numeric, Numeric> {
    // ANCHOR grid setup
    let mut grid = Grid::<Numeric>::empty();
    let exp = Numeric::new(0., std::f64::consts::PI * theta / 180.).exp();

    grid.append_equidist(2., 2) // 0-1-2
        .append_many([1., 3., 8., 16.].map(|v| v * exp));

    grid.into_quadrature_grid(Quadrature::GaussLobatto(gl_order))
}

fn multichannel_analytic(_matches: &ArgMatches) {
    use rayon::prelude::*;
    const STEPS: usize = 2_000;

    let layout = Layout::default().y_axis(Axis::new().type_(AxisType::Log));

    let mut plot = Plot::new();
    plot.set_layout(layout);

    for init in 0..2 {
        for final_ in 0..2 {
            let from: f64 = if init == 0 && final_ == 0 { 0.05 } else { 2.0 };
            const TO: f64 = 4.0;

            let data: Vec<(f64, f64)> = (0..=STEPS)
                .into_par_iter()
                .map(|i| {
                    let e = from + (TO - from) * i as f64 / (STEPS as f64);

                    (e, TOY_MODEL.analytic_scatter_solution(init, final_, e))
                })
                .collect();

            plot.add_trace(
                Scatter::new(data.iter().map(|p| p.0), data.iter().map(|p| p.1))
                    .legend_group("Analytic")
                    .name(format!("sigma_{{{}->{}, a}}(E)", init, final_).as_str()),
            );
        }
    }

    plot.show();
}

fn multichannel_plot(matches: &ArgMatches) {
    let energy = matches.value_of("energy").unwrap().parse::<f64>().unwrap();

    plot_solution(&TOY_MODEL, energy, 0);
}

fn multichannel(matches: &ArgMatches) {
    use rayon::prelude::*;
    const STEPS: usize = 600;

    let compute_direct = matches.is_present("direct");
    let hq_resonance = matches.is_present("hq-resonance");

    let layout = Layout::default().y_axis(Axis::new().type_(AxisType::Log));
    let mut plot = Plot::new();
    plot.set_layout(layout);

    let layout = Layout::default().y_axis(Axis::new().type_(AxisType::Log));
    let mut err_plot = Plot::new();
    err_plot.set_layout(layout);

    for init in 0..2 {
        for final_ in 0..2 {
            let from: f64 = if init | final_ == 0 { 0.05 } else { 2.0 };
            const TO: f64 = 4.0;

            let data: Vec<(f64, f64, f64)> = (0..=STEPS)
                .into_par_iter()
                .map(|i| {
                    let e = from + (TO - from) * i as f64 / (STEPS as f64);
                    let (f, f_a) = get_cross(&TOY_MODEL, e, init, final_, compute_direct);

                    println!("e = {:.3} -> f = {}", e, f);

                    (e, f, f_a)
                })
                .collect();

            plot.add_trace(
                Scatter::new(data.iter().map(|p| p.0), data.iter().map(|p| p.1))
                    .legend_group("Iterative")
                    .name(format!("sigma_{{{}->{}}}(E)", init, final_).as_str()),
            );

            if compute_direct {
                plot.add_trace(
                    Scatter::new(data.iter().map(|p| p.0), data.iter().map(|p| p.2))
                        .legend_group("Direct")
                        .name(format!("sigma_{{{}->{}, direct}}(E)", init, final_).as_str()),
                );
            }

            let data_exact: Vec<(f64, f64)> = (0..=STEPS)
                .into_par_iter()
                .map(|i| {
                    let e = from + (TO - from) * i as f64 / (STEPS as f64);

                    (e, TOY_MODEL.analytic_scatter_solution(init, final_, e))
                })
                .collect();

            plot.add_trace(
                Scatter::new(
                    data_exact.iter().map(|p| p.0),
                    data_exact.iter().map(|p| p.1),
                )
                .legend_group("Analytic")
                .name(format!("sigma_{{{}->{}, a}}(E)", init, final_).as_str()),
            );

            err_plot.add_trace(
                Scatter::new(
                    data_exact.iter().map(|p| p.0),
                    data_exact
                        .iter()
                        .map(|p| p.1)
                        .zip(data.iter().map(|p| p.1))
                        .map(|(a, b)| (a - b).abs()),
                )
                .name(format!("sigma_{{{}->{}, err}}(E)", init, final_).as_str()),
            );
        }
    }

    // More precise
    if hq_resonance {
        let from: f64 = 1.8;
        const TO: f64 = 1.9;

        let data: Vec<(f64, f64, f64)> = (0..=STEPS)
            .into_par_iter()
            .map(|i| {
                let e = from + (TO - from) * i as f64 / (STEPS as f64);
                let (f, f_a) = get_cross(&TOY_MODEL, e, 0, 0, compute_direct);

                (e, f, f_a)
            })
            .collect();

        plot.add_trace(
            Scatter::new(data.iter().map(|p| p.0), data.iter().map(|p| p.1))
                .legend_group("Iterative")
                .name(format!("sigma_{{{}->{}, prec}}(E)", 0, 0).as_str()),
        );
        plot.add_trace(
            Scatter::new(data.iter().map(|p| p.0), data.iter().map(|p| p.2))
                .legend_group("Direct")
                .name(format!("sigma_{{{}->{}, prec, direct}}(E)", 0, 0).as_str()),
        );
    }

    plot.show();
    err_plot.show();
}

/// Compute eigenvalues
///
/// __NOTE__: this is only for debugging
fn multichannel_ev(_matches: &ArgMatches) {
    let grid = build_grid(15, 0.0);
    let model = &TOY_MODEL;

    // Diagonal blocks
    for channel in 0..model.num_channels() {
        println!(" --- Channel {} ---", channel);
        let h = RadialHamiltionian {
            angular_momentum: model.angular_momentums[channel],
            mass: model.mass,
            energy_offset: (-model.energy_levels[channel]).into(),
            potential: model.potential(channel, channel),
        };

        let h = FullMatrix::from(&h.represent_on_grid(&grid)).shrink(1, 1, 1, 1);

        let evs = h.find_eigenvalues();

        for ev in evs.iter() {
            println!("{:?}", ev);
        }
    }
}

fn get_cross<const CHANNELS: usize>(
    model: &Model1D<CHANNELS>,
    energy: f64,
    init_channel: usize,
    final_channel: usize,
    compute_direct: bool,
) -> (f64, f64) {
    let opts = calculation::CalculationOptions {
        energy,
        init_channel,
        grid: Arc::new(build_grid(15, 40.)),
        precision: 10e-3,
        max_iters: None,
    };
    let calc_result = if let Ok(result) =
        calculation::run_iterative(model, &opts, Option::<fn(_, _)>::None, None)
    {
        result
    } else {
        return (0., 0.);
    };

    let cross_inv = if compute_direct {
        let psi_f = calc_result.get_channel_func(model, &opts.grid, final_channel);
        let calc_result = calculation::run_direct(model, &opts);
        let psi_scatter_analytic =
            calc_result.get_channel_func(model, &opts.grid, init_channel) + calc_result.psi_sc;

        psi_f.cplx_dot_prod(&psi_scatter_analytic).norm_sqr()
    } else {
        0.
    };

    let cross = calc_result.get_cross_section(model, &opts.grid, final_channel);
    let cross_inv = 4.0 * std::f64::consts::PI.powi(3) * cross_inv
        / calc_result.momentums[init_channel].norm_sqr();

    (cross, cross_inv)
}

/// Plot the resulting wave function
fn plot_solution<const CHANNELS: usize>(
    model: &Model1D<CHANNELS>,
    energy: f64,
    init_channel: usize,
) {
    let opts = calculation::CalculationOptions {
        energy,
        init_channel,
        grid: Arc::new(build_grid(15, 40.)),
        precision: 10e-3,
        max_iters: None,
    };
    let channel_points = opts.grid.len() - 2;

    let calc_result =
        calculation::run_iterative(model, &opts, Option::<fn(_, _)>::None, None).unwrap();

    let psi_i = opts.grid.represent_fn(model.initial_state(
        model.angular_momentums[init_channel],
        calc_result.momentums[init_channel],
        model.mass,
    ));

    let psi_scatter = &calc_result.pot_matrix
        * &(&calc_result.psi_sc + calc_result.get_channel_func(model, &opts.grid, init_channel));

    let calc_result = calculation::run_direct(model, &opts);
    let psi_scatter_analytic = &calc_result.pot_matrix
        * &(&calc_result.psi_sc + calc_result.get_channel_func(model, &opts.grid, init_channel));

    let grid_points_weights: Vec<(Numeric, Numeric)> = opts
        .grid
        .iter_points_weights()
        .skip(1)
        .take(channel_points)
        .map(|(&x, &w)| (x, w))
        .collect();
    let points_iter = grid_points_weights.iter().map(|(x, _)| x.re);

    let mut plot = Plot::new();

    plot.add_trace(
        Scatter::new(
            points_iter.clone(),
            (0..channel_points).map(|i| calc_result.pot_matrix.get([i, i]).re),
        )
        .name("v_11"),
    );

    plot.add_trace(
        Scatter::new(
            points_iter.clone(),
            (0..channel_points).map(|i| calc_result.pot_matrix.get([i + channel_points, i]).re),
        )
        .name("v_12"),
    );

    plot.add_trace(
        Scatter::new(
            points_iter.clone(),
            (0..channel_points).map(|i| (psi_scatter[i] / grid_points_weights[i].1.sqrt()).re),
        )
        .name("psi^+_1"),
    );

    plot.add_trace(
        Scatter::new(
            points_iter.clone(),
            (0..channel_points)
                .map(|i| (psi_scatter[i + channel_points] / grid_points_weights[i].1.sqrt()).re),
        )
        .name("psi^+_2"),
    );

    plot.add_trace(
        Scatter::new(
            points_iter.clone(),
            (0..channel_points)
                .map(|i| (psi_scatter_analytic[i] / grid_points_weights[i].1.sqrt()).re),
        )
        .name("psi^+_{1, inv}"),
    );

    plot.add_trace(
        Scatter::new(
            points_iter.clone(),
            (0..channel_points).map(|i| {
                (psi_scatter_analytic[i + channel_points] / grid_points_weights[i].1.sqrt()).re
            }),
        )
        .name("psi^+_{2, inv}"),
    );

    plot.add_trace(
        Scatter::new(
            points_iter.clone(),
            (0..channel_points).map(|i| (psi_i[i + 1] / grid_points_weights[i].1.sqrt()).re),
        )
        .name(&format!("psi^i_{}", init_channel)),
    );

    plot.show();
}
