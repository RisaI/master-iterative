use std::sync::Arc;

use num_traits::Zero;
use thiserror::Error;

use super::Numeric;
use crate::{
    models::Model1D,
    primitives::{grid::QuadratureGrid, physics::RadialHamiltionian, prelude::*},
    solvers::{
        cocr_prec::COCRPreconditionedSolver,
        iterative::{self, COCRSolver, IterativeMethod},
    },
};

#[derive(Clone)]
pub struct CalculationOptions {
    pub precision: f64,
    pub max_iters: Option<usize>,

    pub init_channel: usize,
    pub energy: f64,
    pub grid: Arc<QuadratureGrid<Numeric, Numeric>>,
}

pub struct CalculationResults<const CHANNELS: usize> {
    pub iters: usize,
    pub precision: f64,
    pub runtime: u128,
    pub momentums: [C64; CHANNELS],
    pub init_channel: usize,

    // Expensive part
    pub psi_sc: FullVector<C64>,
    pub pot_matrix: SparseMatrix<C64>,
}

impl<const CHANNELS: usize> CalculationResults<CHANNELS> {
    pub fn get_channel_func(
        &self,
        model: &Model1D<CHANNELS>,
        grid: &QuadratureGrid<Numeric, Numeric>,
        channel: usize,
    ) -> FullVector<C64> {
        let channel_points = grid.len() - 2;
        let mut result = FullVector::<Numeric>::zero(self.psi_sc.nrows());
        let psi_i = grid.represent_fn(model.initial_state(
            model.angular_momentums[channel],
            self.momentums[channel],
            model.mass,
        ));
        (0..channel_points).for_each(|i| {
            result[i + channel_points * channel] += psi_i[i + 1];
        });

        result
    }

    pub fn get_cross_section(
        &self,
        model: &Model1D<CHANNELS>,
        grid: &QuadratureGrid<Numeric, Numeric>,
        out: usize,
    ) -> f64 {
        let psi_f = self.get_channel_func(model, grid, out);
        let psi_out = self.get_channel_func(model, grid, self.init_channel) + &self.psi_sc;

        let cross = psi_f.dotc(&(&self.pot_matrix * &psi_out)).norm_sqr();

        4.0 * std::f64::consts::PI.powi(3) * cross / self.momentums[self.init_channel].norm_sqr()
    }
}

fn get_lhs_rhs<const CHANNELS: usize>(
    model: &Model1D<CHANNELS>,
    opts: &CalculationOptions,
) -> (
    SparseMatrix<Numeric>,
    FullVector<Numeric>,
    [Numeric; CHANNELS],
    SparseMatrix<Numeric>,
) {
    let momentums: [C64; CHANNELS] = model
        .energy_levels
        .map(|eps| Numeric::from(2.0 * model.mass * (opts.energy - eps)).sqrt());

    let grid = &opts.grid;
    let channel_points = grid.len() - 2;

    // ANCHOR matrix setup
    let mut lhs = nalgebra_sparse::CooMatrix::<Numeric>::new(
        CHANNELS * channel_points,
        CHANNELS * channel_points,
    );

    // Diagonal blocks
    for channel in 0..CHANNELS {
        let h = RadialHamiltionian {
            angular_momentum: model.angular_momentums[channel],
            mass: model.mass,
            energy_offset: (opts.energy - model.energy_levels[channel]).into(),
            potential: model.potential(channel, channel),
        };

        let h = FullMatrix::from(&h.represent_on_grid(grid)).shrink(1, 1, 1, 1);

        let channel_offset = channel * channel_points;
        for i in 0..h.rows() {
            for j in 0..h.cols() {
                let elem = h.get([i, j]);

                if !elem.is_zero() {
                    lhs.push(i + channel_offset, j + channel_offset, *elem);
                }
            }
        }
    }

    // ANCHOR potential matrix for cross section
    let mut pot_matrix = nalgebra_sparse::CooMatrix::<Numeric>::new(
        CHANNELS * channel_points,
        CHANNELS * channel_points,
    );
    for i in 0..CHANNELS {
        for j in 0..CHANNELS {
            let top = i * channel_points;
            let left = j * channel_points;

            let v = grid.represent_fn_dvr(model.potential::<C64>(i, j));

            v.iter()
                .skip(1)
                .take(channel_points)
                .enumerate()
                .for_each(|(k, &val)| {
                    // let val = v(x);
                    if !val.is_zero() {
                        pot_matrix.push(top + k, left + k, val);

                        if i != j {
                            lhs.push(top + k, left + k, val);
                        }
                    }
                });
        }
    }

    let lhs: SparseMatrix<Numeric> = nalgebra_sparse::CsrMatrix::from(&lhs).into();
    let pot_matrix: SparseMatrix<Numeric> = nalgebra_sparse::CsrMatrix::from(&pot_matrix).into();

    // ANCHOR rhs setup
    let rhs = {
        let mut result = FullVector::<Numeric>::zero(CHANNELS * channel_points);
        let psi_i = grid.represent_fn(model.initial_state(
            model.angular_momentums[opts.init_channel],
            momentums[opts.init_channel],
            model.mass,
        ));

        (0..channel_points).for_each(|i| {
            result[i + opts.init_channel * channel_points] = psi_i[i + 1];
        });

        // -V_{int} \psi_0
        -(&pot_matrix * &result)
    };

    (lhs, rhs, momentums, pot_matrix)
}

#[derive(Debug, Error)]
pub enum IterativeError {
    #[error("The calculation diverged in {0} steps")]
    Diverged(usize),

    #[error("The calculation didn't converge in required amount of steps, got to ratio {0}")]
    OutOfSteps(f64),
}

pub trait OnIterFunc = FnMut(usize, f64);

pub fn run_iterative<F: OnIterFunc, const CHANNELS: usize>(
    model: &Model1D<CHANNELS>,
    opts: &CalculationOptions,
    mut iter_fn: Option<F>,
    kickstart: Option<FullVector<C64>>,
) -> Result<CalculationResults<CHANNELS>, IterativeError> {
    let (lhs, rhs, momentums, pot_matrix) = get_lhs_rhs(model, opts);

    // ANCHOR solver
    let mut steps = 0;
    #[allow(unused_assignments)]
    let mut ratio: f64 = 1.;
    let start = std::time::Instant::now();

    let mut solver = COCRSolver::new(
        &lhs,
        &rhs,
        kickstart.unwrap_or_else(|| FullVector::zero(rhs.nrows())),
        None,
    );
    let b_len = rhs.norm();

    // ANCHOR iterative solution
    loop {
        steps += 1;

        if let iterative::StepResult::Precision(_) = solver.step() {
            let prec = (&lhs * solver.get_current() - rhs.clone()).norm();
            if prec.is_nan() {
                return Err(IterativeError::Diverged(steps));
            }

            ratio = prec / b_len;

            if let Some(iter_fn) = iter_fn.as_mut() {
                iter_fn(steps, ratio);
            }

            let max_iters = opts.max_iters.unwrap_or(usize::MAX);

            if ratio < opts.precision || steps >= max_iters {
                break;
            }
        } else {
            panic!("Unknown solver state");
        }
    }

    let runtime = std::time::Instant::now() - start;

    Ok(CalculationResults {
        iters: steps,
        precision: ratio,
        runtime: runtime.as_micros(),
        momentums,
        init_channel: opts.init_channel,
        psi_sc: solver.get_current().clone(),
        pot_matrix,
    })
}

pub fn run_iterative_preconditioned<F: OnIterFunc, const CHANNELS: usize>(
    model: &Model1D<CHANNELS>,
    opts: &CalculationOptions,
    mut iter_fn: Option<F>,
    kickstart: Option<FullVector<C64>>,
) -> Result<CalculationResults<CHANNELS>, IterativeError> {
    let (lhs, rhs, momentums, pot_matrix) = get_lhs_rhs(model, opts);

    // ANCHOR solver
    let mut steps = 0;
    #[allow(unused_assignments)]
    let mut ratio: f64 = 1.;
    let start = std::time::Instant::now();

    let precond = crate::primitives::precond::ilu0::ilu0(&lhs);
    let mut solver = COCRPreconditionedSolver::new(
        &lhs,
        precond,
        &rhs,
        kickstart.unwrap_or_else(|| FullVector::zero(rhs.nrows())),
    );

    let b_len = rhs.norm();

    // ANCHOR iterative solution
    loop {
        steps += 1;

        if let iterative::StepResult::Precision(_) = solver.step() {
            let prec = (&lhs * solver.get_current() - rhs.clone()).norm();
            if prec.is_nan() {
                return Err(IterativeError::Diverged(steps));
            }

            ratio = prec / b_len;

            if let Some(iter_fn) = iter_fn.as_mut() {
                iter_fn(steps, ratio);
            }

            let max_iters = opts.max_iters.unwrap_or(usize::MAX);

            if ratio < opts.precision || steps >= max_iters {
                break;
            }
        } else {
            panic!("Unknown solver state");
        }
    }

    let runtime = std::time::Instant::now() - start;

    Ok(CalculationResults {
        iters: steps,
        precision: ratio,
        runtime: runtime.as_micros(),
        momentums,
        init_channel: opts.init_channel,
        psi_sc: solver.get_current().clone(),
        pot_matrix,
    })
}

pub fn run_direct<const CHANNELS: usize>(
    model: &Model1D<CHANNELS>,
    opts: &CalculationOptions,
) -> CalculationResults<CHANNELS> {
    let (lhs, rhs, momentums, pot_matrix) = get_lhs_rhs(model, opts);
    let lhs: nalgebra::Matrix<_, _, _, _> = (&lhs.0).into();
    let n = lhs.ncols() as i32;

    // Lapack specific
    let mut lhs: Vec<_> = lhs.data.into();
    let mut rhs: Vec<_> = rhs.data.into();

    let mut info = 0;
    let mut ipiv = vec![0; n as usize];
    let mut work = vec![C64::zero(); n as usize];

    let start = std::time::Instant::now();
    unsafe {
        lapack::zsysv(
            b'U',
            n,
            1,
            lhs.as_mut(),
            n,
            ipiv.as_mut(),
            rhs.as_mut(),
            n,
            work.as_mut(),
            n,
            &mut info,
        );
    }
    // let psi_sc = &lhs_inverse * &rhs;

    let runtime = std::time::Instant::now() - start;

    let psi_sc = rhs.into();

    CalculationResults {
        iters: 0,
        precision: 0.,
        runtime: runtime.as_micros(),
        momentums,
        init_channel: opts.init_channel,
        psi_sc,
        pot_matrix,
    }
}
