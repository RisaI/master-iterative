use std::sync::Arc;

use num_traits::Zero;
use plotly::{common::Title, Layout, Plot, Scatter, Surface};

use crate::primitives::{FullVector, C64};

use super::ModelParams;

// ANCHOR Plotting ----------------------------------------------------------------------

pub fn view_model_2d(
    params: &ModelParams,
    m_state_f: Arc<(f64, FullVector<C64>, usize)>,
    mode: &str,
) {
    let model = params.model;

    println!(
        "Data for model {}, {} -> {}",
        model.name, params.m_state_i.2, m_state_f.2
    );

    println!(
        "E_{{m,i}} = {:.4}, E_{{m,f}} = {:.4}, \\Delta = {:.4}",
        params.m_state_i.0,
        m_state_f.0,
        m_state_f.0 - params.m_state_i.0
    );
    println!("E_b = {}", params.e_state_b.0);

    let mut plot = Plot::new();

    // Energy related scatters
    if mode == "v-0" {
        plot.set_layout(
            Layout::new().title(Title::new(&format!("Energy, GL={}", params.quad_order.0))),
        );

        plot.add_trace(
            Scatter::new(
                params.grid_m.iter_points().map(|x| x.re),
                params
                    .grid_m
                    .iter_points()
                    .map(|x| model.potential_0(*x).re),
            )
            .name("V_0"),
        );
    } else if mode == "v-b" {
        plot.set_layout(
            Layout::new().title(Title::new(&format!("$V_b, GL={}$", params.quad_order.1))),
        );

        plot.add_trace(
            Scatter::new(
                params.grid_e.iter_points().map(|x| x.re),
                params
                    .grid_e
                    .iter_points()
                    .map(|x| model.potential_b(*x).re),
            )
            .name("V_b"),
        );
    } else if mode == "v-int" {
        plot.set_layout(Layout::new().title(Title::new(&format!(
            "$V_\\text{{int}}, GL={}x{}$",
            params.quad_order.0, params.quad_order.1
        ))));

        let mut x = vec![];
        let mut y = vec![];

        plot.add_trace(
            Surface::new(
                params
                    .grid_m
                    .iter_points()
                    .enumerate()
                    .skip(1)
                    .take_while(|(_, r_m)| r_m.im.is_zero())
                    .map(|(i, r_m)| {
                        y.push(r_m.re);

                        if i == 1 {
                            let r_e_iter = params
                                .grid_e
                                .iter_points()
                                .skip(1)
                                .take_while(|r_e| r_e.im.is_zero());

                            x.extend(r_e_iter.map(|x| x.re));
                        }

                        params
                            .grid_e
                            .iter_points()
                            .skip(1)
                            .take_while(|r_e| r_e.im.is_zero())
                            .map(|r_e| model.potential_int([*r_m, *r_e]).re)
                            .collect()
                    })
                    .collect(),
            )
            .x(x)
            .y(y)
            .name("V_int"),
        );
    } else if mode == "v-eff" {
        plot.set_layout(Layout::new().title(Title::new(&format!(
            "$V_\\text{{eff}}, GL={}x{}$",
            params.quad_order.0, params.quad_order.1
        ))));

        let mut x = vec![];
        let mut y = vec![];

        plot.add_trace(
            Surface::new(
                params
                    .grid_m
                    .iter_points()
                    .enumerate()
                    .skip(1)
                    .take_while(|(_, r_m)| r_m.im.is_zero())
                    .map(|(i, r_m)| {
                        y.push(r_m.re);

                        if i == 1 {
                            let r_e_iter = params
                                .grid_e
                                .iter_points()
                                .skip(1)
                                .skip_while(|r_e| r_e.re < 0.15)
                                .take_while(|r_e| r_e.im.is_zero());

                            x.extend(r_e_iter.map(|x| x.re));
                        }

                        params
                            .grid_e
                            .iter_points()
                            .skip(1)
                            .skip_while(|r_e| r_e.re < 0.15)
                            .take_while(|r_e| r_e.im.is_zero())
                            .map(|r_e| model.potential([*r_m, *r_e]).re)
                            .collect()
                    })
                    .collect(),
            )
            .x(x)
            .y(y)
            .name("V_eff"),
        );
    } else if mode == "wavefuncs-m" {
        // Wave-func related scatters
        plot.set_layout(Layout::new().title(Title::new(&format!(
            "Molecular wave functions, GL={}",
            params.quad_order.0
        ))));

        plot.add_trace(
            Scatter::new(
                params
                    .grid_m
                    .iter_points()
                    .take_while(|x| x.im.is_zero())
                    .map(|x| x.re),
                params.m_state_i.1.as_slice().iter().map(|y| y.re),
            )
            .name("Initial molecular state"),
        );

        plot.add_trace(
            Scatter::new(
                params
                    .grid_m
                    .iter_points()
                    .take_while(|x| x.im.is_zero())
                    .map(|x| x.re),
                m_state_f.1.as_slice().iter().map(|y| y.re),
            )
            .name("Final molecular state"),
        );
    } else if mode == "wavefuncs-e" {
        // Wave-func related scatters
        plot.set_layout(Layout::new().title(Title::new(&format!(
            "Electron wave functions, GL={}",
            params.quad_order.1
        ))));

        let init_e_momentum = 0.3;

        plot.add_trace(
            Scatter::new(
                params
                    .grid_e
                    .iter_points()
                    .take_while(|x| x.im.is_zero())
                    .map(|x| x.re),
                params
                    .grid_e
                    .iter_points()
                    .map(|&r_e| model.electron_wave(init_e_momentum, r_e).re),
            )
            .name("Init e- state (k = 0.3)"),
        );

        let final_e_momentum =
            (2. * (params.m_state_i.0 - m_state_f.0) + init_e_momentum.powi(2)).sqrt();

        plot.add_trace(
            Scatter::new(
                params
                    .grid_e
                    .iter_points()
                    .take_while(|x| x.im.is_zero())
                    .map(|x| x.re),
                params
                    .grid_e
                    .iter_points()
                    .map(|&r_e| model.electron_wave(final_e_momentum, r_e).re),
            )
            .name("Final e- state (k = 0.3)"),
        );

        plot.add_trace(
            Scatter::new(
                params
                    .grid_e
                    .iter_points()
                    .skip(1)
                    .take_while(|x| x.im.is_zero())
                    .map(|x| x.re),
                params
                    .grid_e
                    .iter_points()
                    .enumerate()
                    .skip(1)
                    .map(|(i, _)| params.e_state_b.1[i].re),
            )
            .name("Bound e- state"),
        );
    } else if mode == "wavefuncs-in" {
        plot.set_layout(Layout::new().title(Title::new(&format!(
            "2D wave functions, GL={}x{}",
            params.quad_order.0, params.quad_order.1
        ))));

        let init_e_momentum = 0.3f64;

        // let grid_m_len = params.grid_m.len() - 2;
        // let grid_e_len = params.grid_e.len() - 2;

        let mut x = vec![];
        let mut y = vec![];

        plot.add_trace(
            Surface::new(
                params
                    .grid_m
                    .iter_points()
                    .enumerate()
                    .skip(1)
                    .take_while(|(_, r_m)| r_m.im.is_zero())
                    .map(|(i, r_m)| {
                        y.push(r_m.re);

                        if i == 1 {
                            let r_e_iter = params
                                .grid_e
                                .iter_points()
                                .skip(1)
                                .take_while(|r_e| r_e.im.is_zero());

                            x.extend(r_e_iter.map(|x| x.re));
                        }

                        params
                            .grid_e
                            .iter_points()
                            .skip(1)
                            .take_while(|r_e| r_e.im.is_zero())
                            .map(|r_e| {
                                (model.electron_wave(init_e_momentum, *r_e) * params.m_state_i.1[i])
                                    .re
                            })
                            .collect()
                    })
                    .collect(),
            )
            .x(x)
            .y(y)
            .name("Input state"),
        );
    } else if mode == "wavefuncs-out" {
        plot.set_layout(Layout::new().title(Title::new(&format!(
            "2D wave functions, GL={}x{}",
            params.quad_order.0, params.quad_order.1
        ))));

        let init_e_momentum = 0.3f64;
        let final_e_momentum =
            (2. * (params.m_state_i.0 - m_state_f.0) + init_e_momentum.powi(2)).sqrt();

        let mut x = vec![];
        let mut y = vec![];

        plot.add_trace(
            Surface::new(
                params
                    .grid_m
                    .iter_points()
                    .enumerate()
                    .skip(1)
                    .take_while(|(_, r_m)| r_m.im.is_zero())
                    .map(|(i, r_m)| {
                        y.push(r_m.re);

                        if i == 1 {
                            let r_e_iter = params
                                .grid_e
                                .iter_points()
                                .skip(1)
                                .take_while(|r_e| r_e.im.is_zero());

                            x.extend(r_e_iter.map(|x| x.re));
                        }

                        params
                            .grid_e
                            .iter_points()
                            .skip(1)
                            .take_while(|r_e| r_e.im.is_zero())
                            .map(|r_e| {
                                (model.electron_wave(final_e_momentum, *r_e) * m_state_f.1[i]).re
                            })
                            .collect()
                    })
                    .collect(),
            )
            .x(x)
            .y(y)
            .name("Output state"),
        );
    } else {
        println!("Unknown view mode '{}'", mode);
        return;
    }

    plot.show();
}
