use std::{
    f64::consts::PI,
    mem::MaybeUninit,
    sync::{Arc, Mutex},
};

use crate::intel_dss;
use clap::{Arg, ArgMatches};
pub use params::*;
use plotly::{common::Title, layout::Axis, Layout, Plot, Scatter};
pub use preconditioner::*;
use rayon::prelude::*;
use view::view_model_2d;

mod eigensystem;
mod gui;
mod params;
mod preconditioner;
mod view;

pub use eigensystem::*;

use crate::{
    primitives::{
        grid::{ProductGrid, QuadratureGrid},
        precond::ilu0::ILU0Preconditioner,
        FullVector, SparseMatrix, C64,
    },
    solvers::{
        cocr_prec::COCRPreconditionedSolver,
        iterative::{COCRSolver, IterativeMethod, StepResult},
    },
};

use self::gui::GuiEvent;

use super::{model_1d::calculation::IterativeError, Problem};

pub fn get_problems() -> Vec<Problem> {
    vec![Problem::new("2d", plot_2d_model).with_args(&[
        Arg::with_name("ORDER")
            .long("order")
            .takes_value(true)
            .help("Gauss-Lobatto quadrature order"),
        Arg::with_name("MODEL")
            .long("model")
            .takes_value(true)
            .possible_values(&["n2", "no", "f2", "o2"]),
        Arg::with_name("PRECOND")
            .long("preconditioner")
            .takes_value(true)
            .possible_values(&["none", "ilu0", "partial-invert", "partial-solve", "direct"])
            .default_value("none"),
        Arg::with_name("ORDERING")
            .long("ordering")
            .takes_value(true)
            .possible_values(&["me", "em"])
            .default_value("me"),
        Arg::with_name("MODEL_VIEW")
            .long("view-model")
            .help("View parameters of the model")
            .possible_values(&[
                "v-0",
                "v-b",
                "v-eff",
                "v-int",
                "wavefuncs-m",
                "wavefuncs-e",
                "wavefuncs-in",
                "wavefuncs-out",
                "none",
            ])
            .default_value("none")
            .takes_value(true),
        Arg::with_name("CACHE")
            .long("cache")
            .help("Caching mode")
            .takes_value(true)
            .default_value("full")
            .possible_values(&["none", "ignore", "full"]),
        Arg::with_name("RANGE")
            .long("range")
            .help("Format 'from;to;step' endpoints included, ex. '0.1,0.2,0.01'")
            .takes_value(true),
        Arg::with_name("PRECISION")
            .long("precision")
            .takes_value(true),
        Arg::with_name("NOGUI").long("no-gui").takes_value(false),
        Arg::with_name("INIT_M_STATE").index(1).required(true),
        Arg::with_name("FINAL_M_STATE").index(2).required(true),
    ])]
}

#[derive(Clone, Debug)]
enum CacheMode {
    Full,
    Ignore,
    None,
}

impl CacheMode {
    pub fn from_str(text: &str) -> Self {
        match text {
            "none" => Self::None,
            "ignore" => Self::Ignore,
            "full" => Self::Full,
            _ => panic!("Unknown cache mode {}", text),
        }
    }

    pub fn should_retrieve(&self) -> bool {
        matches!(self, Self::Full)
    }

    pub fn should_store(&self) -> bool {
        !matches!(self, Self::None)
    }
}

fn read_range(text: &str) -> (f64, f64, f64) {
    if text.contains('/') {
        if let [range, count] = text.split('/').collect::<Vec<_>>()[..] {
            if let [from, to] = range.split(',').collect::<Vec<_>>()[..] {
                if let (Ok(from), Ok(to), Ok(count)) = (
                    from.parse::<f64>(),
                    to.parse::<f64>(),
                    count.parse::<usize>(),
                ) {
                    return (from, to, (to - from) / (count as f64 - 1.));
                }
            }
        }
    } else {
        let values = text
            .split(',')
            .take(3)
            .filter_map(|v| v.parse::<f64>().ok())
            .collect::<Vec<_>>();

        if values.len() == 3 {
            return (values[0], values[1], values[2]);
        }
    }

    panic!("Invalid range format, use 'FROM,TO,STEP' or 'FROM,TO/NSTEPS'");
}

pub enum ModelSolver<'a> {
    None(COCRSolver<'a, FullVector<C64>, C64, SparseMatrix<C64>>),
    ILU0(
        COCRPreconditionedSolver<
            'a,
            FullVector<C64>,
            C64,
            SparseMatrix<C64>,
            ILU0Preconditioner<C64>,
        >,
    ),
    Sparse(
        COCRPreconditionedSolver<'a, FullVector<C64>, C64, SparseMatrix<C64>, SparseMatrix<C64>>,
    ),
}

impl<'a> IterativeMethod for ModelSolver<'a> {
    type ScalarType = C64;
    type WorkVec = FullVector<C64>;

    fn get_current(&self) -> &Self::WorkVec {
        match self {
            Self::None(s) => s.get_current(),
            Self::ILU0(s) => s.get_current(),
            Self::Sparse(s) => s.get_current(),
        }
    }

    fn step(&mut self) -> StepResult<Self::ScalarType> {
        match self {
            Self::None(s) => s.step(),
            Self::ILU0(s) => s.step(),
            Self::Sparse(s) => s.step(),
        }
    }
}

impl<'a> ModelSolver<'a> {
    fn set_current(&mut self, curr: FullVector<C64>) {
        match self {
            Self::None(s) => s.set_current(move |i| curr[i]),
            Self::ILU0(s) => s.set_current(move |i| curr[i]),
            Self::Sparse(s) => s.set_current(move |i| curr[i]),
        };
    }
}

/// # 2d
fn plot_2d_model(matches: &ArgMatches) {
    // Read args
    let cache_mode = matches.value_of("CACHE").map(CacheMode::from_str).unwrap();
    let no_gui = matches.is_present("NOGUI");

    let model_name = matches.value_of("MODEL").unwrap_or("n2").to_lowercase();
    let (params, system) = ModelParams::from_matches(matches);

    let final_m_state: usize = matches
        .value_of("FINAL_M_STATE")
        .and_then(|v| v.parse().ok())
        .unwrap();

    let init_m_state = params.m_state_i.2;
    let quad_order = params.quad_order;

    if matches.occurrences_of("MODEL_VIEW") > 0 {
        if let Some(mode) = matches.value_of("MODEL_VIEW") {
            view_model_2d(
                &params,
                system.get_extended_triplet(final_m_state, params.grid_m.len()),
                mode,
            );
            return;
        }
    }

    // Setup points of interest
    let (start_k, end_k, step) = if let Some(range) = matches.value_of("RANGE") {
        read_range(range)
    } else {
        match model_name.as_str() {
            "n2" => (0.02, 0.2, 0.005),
            "f2" => (0.01, 0.1, 0.005),
            "no" => (0.01, 0.08, 0.005),
            "o2" => (0.0025, 0.06, 0.0025),
            _ => todo!(),
        }
    };
    let steps = ((end_k - start_k) / step).ceil() as usize;

    // Setup the cache
    let table_name = format!(
        "2d-{model_name}_{init_m_state}-{final_m_state}_gl{}x{}_v2",
        quad_order.0, quad_order.1
    );
    let cache = crate::cache::get_cache().unwrap();
    cache
        .ensure_multi_created(&table_name, &["ve", "da"])
        .unwrap();

    cache
        .ensure_multi_created(format!("conv_{table_name}"), &["iters", "rel_err"])
        .unwrap();

    // Retrieve cache data if necessary
    let (x_values, mut results) = if let CacheMode::None = cache_mode {
        (vec![], vec![])
    } else {
        let values = cache
            .get_multi_values(&table_name, start_k, end_k, &["ve", "da"])
            .unwrap();
        (values.iter().map(|v| v[0]).collect(), values)
    };

    // Wrap the cache for multithreading
    let cache = Arc::new(Mutex::new(cache));

    // GUI thread
    let sync_tx = Arc::new(Mutex::new(gui::start_gui(
        &format!(
            "2D Model for {}: {} -> {}",
            params.model.name, params.m_state_i.2, final_m_state
        ),
        no_gui,
    )));

    // Store for convergence information
    let iterations = Arc::new(Mutex::new(Vec::<(f64, usize)>::new()));

    let model_name = params.model.name;

    // Run the calculation in parallel
    results.par_extend((0..steps).into_par_iter().filter_map({
        let tx = sync_tx.clone();
        let iterations = iterations.clone();

        move |i| {
            let t_e = start_k + i as f64 * step;
            let params = params.clone_with_energy(t_e);

            let tx = tx.lock().unwrap().clone();

            tx.send(GuiEvent::CreateTask(i, Some(format!("t_e = {:.3} ", t_e))))
                .ok();

            if cache_mode.should_retrieve()
                && x_values
                    .iter()
                    .any(|x| approx::abs_diff_eq!(*x, t_e, epsilon = step / 3.))
            {
                tx.send(GuiEvent::FinishTask(
                    i,
                    Some(format!("t_e = {t_e:.3} ... from cache")),
                ))
                .ok();
                return None;
            }

            let result = match run_model_2d(
                &params,
                |p: f64| {
                    let perc = p.log(params.target_prec).min(1.).max(0.);
                    tx.send(GuiEvent::TaskProgress(i, perc)).unwrap();
                },
                None,
            ) {
                Ok(r) => r,
                Err(err) => {
                    eprintln!("Error at E = {t_e:.4}: {err}");
                    return None;
                }
            };

            let sigma_ve = result.sigma_ve(
                &params,
                system.get_extended_triplet(final_m_state, params.grid_m.len()),
            );
            let sigma_da = result.sigma_da(&params);

            if cache_mode.should_store() {
                if let Ok(cache) = cache.lock() {
                    if let Err(e) = cache.insert_multi_values(
                        &table_name,
                        t_e,
                        &[("ve", sigma_ve), ("da", sigma_da)],
                        None,
                    ) {
                        eprintln!("Error while submitting data: {e}");
                    }
                }
            }

            iterations.lock().unwrap().push((t_e, result.iters));

            tx.send(GuiEvent::FinishTask(
                i,
                Some(format!("t_e = {t_e:.3} ... calculated")),
            ))
            .ok();

            Some([t_e, sigma_ve, sigma_da])
        }
    }));

    // Exit the GUI thread
    sync_tx.lock().unwrap().send(GuiEvent::Exit).ok();

    // Sort the results
    results.sort_by(|a, b| a[0].partial_cmp(&b[0]).unwrap());

    // Plot the result
    {
        let mut plot = Plot::new();
        plot.set_layout(
            Layout::default()
                .title(Title::new(&format!(
                    "$\\text{{2D Model of }} {model_name},~ {init_m_state} \\to {final_m_state},~ \\text{{GL}}={}x{}$", quad_order.0, quad_order.1
                )))
                .x_axis(Axis::new().title(Title::new("$T_{e,i} ~[\\text{au}]$")))
                .y_axis(Axis::new().title(Title::new("$\\sigma ~[a_0^2]$"))),
        );

        plot.add_trace(
            Scatter::new(results.iter().map(|p| p[0]), results.iter().map(|p| p[1])).name(
                format!("$\\sigma_{{VE,{init_m_state}->{final_m_state}}}(T_{{e,i}})$",).as_str(),
            ),
        );

        plot.add_trace(
            Scatter::new(
                results.iter().map(|p| p[0]),
                results
                    .iter()
                    .map(|p| if p[2].is_nan() { 0. } else { p[2] }),
            )
            .name(format!("$\\sigma_{{DA,{init_m_state}->{final_m_state}}}(T_{{e,i}})$",).as_str()),
        );

        plot.show();
    }

    // Plot convergence
    {
        let mut plot = Plot::new();
        plot.set_layout(
            Layout::default()
                .title(Title::new(&format!(
                    "$\\text{{Convergence of 2D Model of }} {model_name},~ {init_m_state} \\to {final_m_state},~ \\text{{GL}}={}x{}$",
                    quad_order.0, quad_order.1
                )))
                .x_axis(Axis::new().title(Title::new("$T_{e,i} ~[\\text{hartree}]$")))
                .y_axis(Axis::new().title(Title::new("Iterations"))),
        );

        let mut vals = Arc::try_unwrap(iterations).unwrap().into_inner().unwrap();
        vals.par_sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());

        plot.add_trace(
            Scatter::new(vals.iter().map(|p| p.0), vals.iter().map(|p| p.1)).name(
                format!("$\\sigma_{{DA,{init_m_state}->{final_m_state}}}(T_{{e,i}})$",).as_str(),
            ),
        );

        plot.show();
    }
}

// ANCHOR Calculation ----------------------------------------------

pub struct ModelRunResult {
    pub psi_sc: FullVector<C64>,
    pub iters: usize,
    pub runtime: u128,
    pub error: f64,
}

impl ModelRunResult {
    pub fn sigma_ve(&self, params: &ModelParams, m_f: Arc<(f64, FullVector<C64>, usize)>) -> f64 {
        let k_e_i = (2. * params.t_e).sqrt();
        let k_e_f = (2. * (params.t_e + params.m_state_i.0 - m_f.0)).sqrt();
        let prefactor = 4. * PI.powi(3) / k_e_i.powi(2);

        let grid = params
            .ordering
            .get_product_grid(&params.grid_m, &params.grid_e);

        let [psi_i, psi_f] = [(k_e_i, &params.m_state_i.1), (k_e_f, &m_f.1)]
            .map(|(k, m)| params.get_psi(&grid, k, m));

        let t_ve = {
            let v_int: FullVector<_> = grid
                .represent_fn_dvr(
                    |p| params.model.potential_int(params.ordering.to_me(p)),
                    true,
                )
                .into();

            psi_f.dotc(&v_int.component_mul(&(&psi_i + &self.psi_sc)))
        };

        prefactor * t_ve.norm_sqr()
    }

    pub fn sigma_da(&self, params: &ModelParams) -> f64 {
        let &ModelParams {
            model,
            ordering: ord,
            t_e,
            ..
        } = params;

        let k_e_i = (2. * t_e).sqrt();
        let prefactor = 4. * PI.powi(3) / k_e_i.powi(2);
        let grid = params
            .ordering
            .get_product_grid(&params.grid_m, &params.grid_e);

        let t_da = {
            let v_da: FullVector<_> = grid
                .represent_fn_dvr(
                    |p| {
                        model.potential_int(ord.to_me(p)) + model.potential_0(*ord.get_m(&p))
                            - model.potential_b(*ord.get_e(&p))
                    },
                    true,
                )
                .into();

            let k_da =
                (2. * params.model.mu * (t_e + params.m_state_i.0 - params.e_state_b.0)).sqrt();

            let psi_i = params.get_psi(&grid, k_e_i, &params.m_state_i.1);
            let psi_f = FullVector::from(
                grid.get_index_iterator()
                    .shrink_all(1, 1)
                    .map(|midx| {
                        let [r_m, _] = ord.to_me(grid.get_point(midx));

                        params.e_state_b.1[*ord.get_e(&midx)]
                            * model.molecular_wave(k_da, r_m)
                            * grid.get_weight(midx).sqrt()
                    })
                    .collect::<Vec<_>>(),
            );

            psi_f.dotc(&v_da.component_mul(&(psi_i + &self.psi_sc)))
        };

        prefactor * t_da.norm_sqr()
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum GridOrdering {
    MoleculeElectron,
    ElectronMolecule,
}

impl ToString for GridOrdering {
    fn to_string(&self) -> String {
        match self {
            Self::MoleculeElectron => "me".into(),
            Self::ElectronMolecule => "em".into(),
        }
    }
}

impl GridOrdering {
    pub fn get_e<'a, T>(&self, data: &'a [T; 2]) -> &'a T {
        match self {
            Self::MoleculeElectron => &data[1],
            Self::ElectronMolecule => &data[0],
        }
    }

    pub fn get_m<'a, T>(&self, data: &'a [T; 2]) -> &'a T {
        match self {
            Self::MoleculeElectron => &data[0],
            Self::ElectronMolecule => &data[1],
        }
    }

    pub fn to_me<T: Copy>(&self, data: [T; 2]) -> [T; 2] {
        match self {
            Self::MoleculeElectron => data,
            Self::ElectronMolecule => [data[1], data[0]],
        }
    }

    pub fn get_product_grid<'a>(
        &self,
        grid_m: &'a QuadratureGrid<C64, C64>,
        grid_e: &'a QuadratureGrid<C64, C64>,
    ) -> ProductGrid<C64, C64, &'a QuadratureGrid<C64, C64>, 2> {
        match self {
            Self::MoleculeElectron => ProductGrid::new([grid_m, grid_e]),
            Self::ElectronMolecule => ProductGrid::new([grid_e, grid_m]),
        }
    }
}

pub fn run_model_2d(
    params: &ModelParams,
    mut progress: impl FnMut(f64),
    init_guess: Option<FullVector<C64>>,
) -> Result<ModelRunResult, IterativeError> {
    // Create a product grid
    let &ModelParams {
        ordering: ord,
        model,
        ..
    } = params;
    let grid = ord.get_product_grid(&params.grid_m, &params.grid_e);

    // Electron momentum
    let k_e_i = (2. * params.t_e).sqrt();
    let psi_i = params.get_psi(&grid, k_e_i, &params.m_state_i.1);

    // Setup the LHS and RHS
    let mut lhs = grid.stiffness_matrix_fast(ord.to_me([-0.5 / model.mu, -0.5]), true);
    let rhs = -FullVector::from(grid.represent_fn_dvr(|p| model.potential_int(ord.to_me(p)), true))
        .component_mul(&psi_i);

    // add DVR potential minus energy to the LHS hamiltonian
    for (i, pos) in grid.get_index_iterator().shrink_all(1, 1).enumerate() {
        lhs.push(
            i,
            i,
            model.potential(ord.to_me(grid.get_point(pos))) - (params.t_e + params.m_state_i.0),
        );
    }

    // Convert LHS to a more efficient matrix
    let lhs: SparseMatrix<_> = SparseMatrix::from(&lhs);

    if let Preconditioner::Direct = params.preconditioner {
        let (psi_sc, time) = unsafe {
            fn report_error(err: i32) {
                if err != intel_dss::MKL_DSS_SUCCESS {
                    eprintln!("Intel DSS Error code: {}", err);
                    panic!();
                }
            }

            let mut handle = MaybeUninit::uninit().assume_init();
            let n = lhs.0.nrows() as i32;
            let nnz = lhs.0.nnz() as i32;
            let (row_index, columns, values) = lhs.0.disassemble();
            let [row_index, columns] =
                [row_index, columns].map(|a| a.into_iter().map(|v| v as i32).collect::<Vec<_>>());
            let params = intel_dss::MKL_DSS_MSG_LVL_WARNING
                | intel_dss::MKL_DSS_TERM_LVL_ERROR
                | intel_dss::MKL_DSS_ZERO_BASED_INDEXING;

            let start = std::time::Instant::now();
            report_error(intel_dss::dss_create_(&mut handle, &params));
            report_error(intel_dss::dss_define_structure_(
                &mut handle,
                &intel_dss::MKL_DSS_SYMMETRIC_STRUCTURE_COMPLEX,
                row_index.as_ptr(),
                &n,
                &n,
                columns.as_ptr(),
                &nnz,
            ));
            let perm = vec![];
            report_error(intel_dss::dss_reorder_(
                &mut handle,
                &intel_dss::MKL_DSS_AUTO_ORDER,
                perm.as_ptr(),
            ));
            report_error(intel_dss::dss_factor_complex_(
                &mut handle,
                &intel_dss::MKL_DSS_DEFAULTS,
                std::mem::transmute(values.as_ptr()),
            ));
            let mut solution: Vec<C64> = vec![C64::new(0., 0.); n as usize];
            report_error(intel_dss::dss_solve_complex_(
                &mut handle,
                &intel_dss::MKL_DSS_DEFAULTS,
                std::mem::transmute(rhs.as_ptr()),
                &1,
                std::mem::transmute(solution.as_mut_ptr()),
            ));
            report_error(intel_dss::dss_delete_(
                &handle,
                &intel_dss::MKL_DSS_DEFAULTS,
            ));
            (solution, start.elapsed().as_micros())
        };

        return Ok(ModelRunResult {
            psi_sc: FullVector::from(psi_sc),
            iters: 1,
            runtime: time,
            error: 0.,
        });
    }

    // ANCHOR solver
    let mut solver = params
        .preconditioner
        .create_solver(&lhs, &rhs, params, model, k_e_i);

    if let Some(vec) = init_guess {
        solver.set_current(vec);
    }

    let mut steps = 0;
    let b_len = rhs.norm();
    let mut ratio;
    let start = std::time::Instant::now();

    loop {
        steps += 1;

        if let StepResult::Precision(_) = solver.step() {
            let prec = ((&lhs * solver.get_current()) - rhs.clone()).norm();

            if prec.is_nan() {
                return Err(IterativeError::Diverged(steps));
            }

            ratio = prec / b_len;

            progress(ratio);

            if ratio < params.target_prec || params.max_iters.unwrap_or(usize::MAX) <= steps {
                break;
                // return Err(IterativeError::OutOfSteps(ratio));
            }
        } else {
            panic!("Invalid solver state.");
        }
    }

    let time = start.elapsed().as_micros();

    Ok(ModelRunResult {
        psi_sc: solver.get_current().clone(),
        iters: steps,
        runtime: time,
        error: ratio,
    })
}
