use std::{
    collections::HashMap,
    io::Stdout,
    sync::mpsc::{channel, Sender},
};

use pbr::ProgressBar;

pub enum GuiEvent {
    CreateTask(usize, Option<String>),
    TaskProgress(usize, f64),
    FinishTask(usize, Option<String>),
    Exit,
}

pub fn start_gui(title: &str, no_gui: bool) -> Sender<GuiEvent> {
    let (tx, rx) = channel::<GuiEvent>();

    println!("{}", title);
    println!();

    if no_gui {
        std::thread::spawn(move || {
            while let Ok(row) = rx.recv() {
                match row {
                    GuiEvent::Exit => return,
                    GuiEvent::CreateTask(handle, Some(text)) => {
                        println!("Started task {handle}: {text}")
                    }
                    GuiEvent::CreateTask(handle, _) => println!("Started task {handle}"),
                    GuiEvent::FinishTask(handle, Some(text)) => {
                        println!("Finished task {handle}: {text}")
                    }
                    GuiEvent::FinishTask(handle, _) => println!("Finished task {handle}"),
                    _ => {
                        // noop
                    }
                }
            }
        });
    } else {
        std::thread::spawn(move || {
            let rx = rx;
            let mut bars: HashMap<usize, ProgressBar<Stdout>> = HashMap::new();

            while let Ok(row) = rx.recv() {
                match row {
                    GuiEvent::Exit => return,
                    GuiEvent::CreateTask(handle, text) => {
                        let mut bar = ProgressBar::new(10_000);
                        bar.show_counter = false;
                        bar.show_speed = false;
                        bar.show_tick = false;
                        bar.show_time_left = false;
                        bar.set_max_refresh_rate(Some(std::time::Duration::from_millis(1000)));
                        bar.message(&text.unwrap_or_else(|| format!("handle {handle}")));
                        bar.is_multibar = true;
                        bars.insert(handle, bar);
                    }
                    GuiEvent::TaskProgress(handle, progress) => {
                        if let Some(bar) = bars.get_mut(&handle) {
                            bar.set((10_000.0 * progress) as u64);
                        }
                    }
                    GuiEvent::FinishTask(handle, text) => {
                        if let Some(bar) = bars.get_mut(&handle) {
                            bar.finish_print(text.as_deref().unwrap_or("... finished"));
                        }
                    }
                }
            }
        });
    }

    tx
}
