use std::{borrow::Borrow, ops::DivAssign, sync::Arc};

use clap::ArgMatches;
use num_traits::Zero;

use crate::{
    models::Model2D,
    primitives::{
        grid::{ProductGrid, Quadrature, QuadratureGrid},
        FullVector, Vector, C64,
    },
};

use super::{preconditioner::Preconditioner, Eigensystem, GridOrdering};

#[derive(Clone)]
pub struct ModelParams {
    pub target_prec: f64,
    pub max_iters: Option<usize>,
    pub quad_order: (usize, usize),

    pub preconditioner: Preconditioner,
    pub ordering: GridOrdering,

    pub model: &'static Model2D,
    /// Initial kinetic energy of the electron
    pub t_e: f64,

    /// Initial molecular state
    pub m_state_i: Arc<(f64, FullVector<C64>, usize)>,
    /// Electronic bound state
    pub e_state_b: Arc<(f64, FullVector<C64>)>,

    /// Molecular grid
    pub grid_m: Arc<QuadratureGrid<C64, C64>>,
    /// Electronic grid
    pub grid_e: Arc<QuadratureGrid<C64, C64>>,
}

fn read_order(text: &str) -> (usize, usize) {
    if let [m, e] = text.split(',').collect::<Vec<_>>()[..] {
        if let (Ok(m), Ok(e)) = (m.parse::<usize>(), e.parse::<usize>()) {
            return (m, e);
        }
    } else if let Ok(q) = text.parse::<usize>() {
        return (q, q);
    }

    panic!("Failed to parse quadrature order. Use the 'GL_M,GL_E' or 'GL' format.");
}

impl ModelParams {
    pub fn clone_with_energy(&self, e: f64) -> Self {
        let mut r = self.clone();
        r.t_e = e;
        r
    }

    pub fn from_matches(matches: &ArgMatches) -> (Self, Eigensystem) {
        let quad_order = matches
            .value_of("ORDER")
            .map(read_order)
            .unwrap_or((15, 15));

        let init_m_state: usize = matches
            .value_of("INIT_M_STATE")
            .and_then(|v| v.parse().ok())
            .unwrap();

        let model_name = matches.value_of("MODEL").unwrap_or("n2").to_lowercase();
        let model = match model_name.as_str() {
            "n2" => &crate::models::MODEL_2D_N2,
            "f2" => &crate::models::MODEL_2D_F2,
            "no" => &crate::models::MODEL_2D_NO,
            "o2" => &crate::models::MODEL_2D_O2,
            _ => panic!("Unknown model"),
        };

        let ordering = match matches.value_of("ORDERING").unwrap() {
            "me" => GridOrdering::MoleculeElectron,
            "em" => GridOrdering::ElectronMolecule,
            v => panic!("Unknown ordering '{v}'"),
        };

        let prec = matches
            .value_of("PRECISION")
            .and_then(|v| v.parse::<f64>().ok())
            .unwrap_or(1e-2);

        println!("{prec}");
        Self::new(
            quad_order,
            init_m_state,
            model,
            ordering,
            prec,
            0.,
            matches.value_of("PRECOND").unwrap(),
        )
    }

    pub fn get_bound_state(
        model: &'static Model2D,
        subgrid_e: &QuadratureGrid<C64, C64>,
    ) -> (f64, FullVector<C64>) {
        // -1/2 laplace
        let mut mat = subgrid_e.stiffness_matrix_whole((-0.5f64).into());

        // Add potential
        for i in 0..mat.nrows() {
            let r_e = *subgrid_e.get_point(i);
            mat.push(
                i,
                i,
                model.angular_momentum_term(r_e) + model.potential_b(r_e),
            );
        }

        let mat: nalgebra::DMatrix<C64> = (&mat).into();
        let dim = mat.nrows();
        let mat = mat.remove_columns_at(&[0, dim - 1]);
        let mat = mat.remove_rows_at(&[0, dim - 1]);

        let mut eigen = mat.symmetric_eigen();
        let perm = permutation::sort_by(eigen.eigenvalues.as_slice(), |a, b| {
            a.partial_cmp(b).unwrap()
        })
        .inverse();

        subgrid_e
            .iter_weights()
            .skip(1)
            .zip(eigen.eigenvectors.row_iter_mut())
            .for_each(|(w, mut r)| {
                r.div_assign(w.sqrt());
            });

        (
            eigen.eigenvalues[perm.apply_idx(0)],
            eigen.eigenvectors.column(perm.apply_idx(0)).clone_owned(),
        )
    }

    pub fn new(
        quad_order: (usize, usize),
        m_i: usize,
        model: &'static Model2D,
        ordering: GridOrdering,
        target_prec: f64,
        energy: f64,
        precond: &str,
    ) -> (Self, Eigensystem) {
        // Setup grid for coordinate R
        let grid_m = (model.build_grid_m)();
        let grid_sub_m = grid_m.subgrid(|p| p.im.is_zero());
        let grid_m = grid_m.into_quadrature_grid(Quadrature::GaussLobatto(quad_order.0));

        let system = Eigensystem::get_molecule_eigensystem(
            model,
            &grid_sub_m.into_quadrature_grid(Quadrature::GaussLobatto(quad_order.0)),
        );

        // Setup grid for coordinate r
        let grid_e = (model.build_grid_e)();
        let grid_sub_e = grid_e
            .subgrid(|p| p.im.is_zero())
            .into_quadrature_grid(Quadrature::GaussLobatto(quad_order.1));
        let grid_e = grid_e.into_quadrature_grid(Quadrature::GaussLobatto(quad_order.1));

        // Find EV and ES for electronic state
        let (ev_e_b, es_e_b) = {
            let (ev, v) = Self::get_bound_state(model, &grid_sub_e);

            let mut next = FullVector::<C64>::zero(grid_e.len());

            next.as_mut_slice()[1..=v.len()].copy_from_slice(v.as_slice());

            (ev, next)
        };

        (
            Self {
                target_prec,
                max_iters: None,
                model,
                m_state_i: system.get_extended_triplet(m_i, grid_m.len()),
                grid_m: Arc::new(grid_m),
                grid_e: Arc::new(grid_e),
                t_e: energy,

                e_state_b: Arc::new((ev_e_b, es_e_b)),

                preconditioner: Preconditioner::parse(precond),
                ordering,
                quad_order,
            },
            system,
        )
    }

    pub fn get_psi(
        &self,
        grid: &ProductGrid<C64, C64, impl Borrow<QuadratureGrid<C64, C64>>, 2>,
        k_e: f64,
        ms: &FullVector<C64>,
    ) -> FullVector<C64> {
        FullVector::from(
            grid.get_index_iterator()
                .shrink_all(1, 1)
                .map(|midx| {
                    let r_e = *self.ordering.get_e(&grid.get_point(midx));

                    ms[*self.ordering.get_m(&midx)]
                        * self.model.electron_wave(k_e, r_e)
                        * grid.get_weight(midx).sqrt()
                })
                .collect::<Vec<_>>(),
        )
    }
}
