use nalgebra::DMatrix;
use nalgebra_sparse::CooMatrix;

use crate::{
    models::Model2D,
    primitives::{grid::ProductGrid, precond::ilu0::ilu0, FullVector, SparseMatrix, Vector, C64},
    solvers::{cocr_prec::COCRPreconditionedSolver, iterative::COCRSolver},
};

use super::{GridOrdering, ModelParams, ModelSolver};

#[derive(Clone, Debug)]
pub enum Preconditioner {
    None,
    ILU0,
    PartialSolve,
    PartialInvert,
    Direct,
}

impl Preconditioner {
    pub fn parse(text: &str) -> Self {
        match text {
            "none" => Self::None,
            "ilu0" => Self::ILU0,
            "partial-solve" => Self::PartialSolve,
            "partial-invert" => Self::PartialInvert,
            "direct" => Self::Direct,
            _ => panic!("Unknown preconditioner {}", text),
        }
    }

    pub fn create_solver<'a>(
        &self,
        lhs: &'a SparseMatrix<C64>,
        rhs: &'a FullVector<C64>,
        model_params: &ModelParams,
        model: &Model2D,
        k_e_i: f64,
    ) -> ModelSolver<'a> {
        match self {
            Self::Direct => todo!(),
            Self::None => ModelSolver::None(COCRSolver::new(
                lhs,
                rhs,
                FullVector::zero(rhs.nrows()),
                None,
            )),
            Self::ILU0 => {
                let prec = ilu0(lhs);

                ModelSolver::ILU0(COCRPreconditionedSolver::new(
                    lhs,
                    prec,
                    rhs,
                    FullVector::zero(rhs.nrows()),
                ))
            }
            Self::PartialSolve => {
                let [grid_a, grid_b] = model_params
                    .ordering
                    .to_me([&model_params.grid_m, &model_params.grid_e]);

                let [pref_a, _pref_b]: [C64; 2] = model_params
                    .ordering
                    .to_me([(-0.5 / model.mu).into(), 0.5.into()]);

                // Solve 1D problem
                let mut mat: nalgebra::DMatrix<C64> =
                    (&grid_a.stiffness_matrix_whole(pref_a)).into();

                // let energy = 0.5 * k_e_i.powi(2);
                let energy = 0.5 * k_e_i.powi(2) + model_params.m_state_i.0;

                let pot: Box<dyn Fn(C64) -> C64> = match model_params.ordering {
                    GridOrdering::MoleculeElectron => Box::new(|r_m| model.potential_0(r_m)),
                    GridOrdering::ElectronMolecule => {
                        Box::new(|r_e| model.potential_b(r_e) + model.angular_momentum_term(r_e))
                    }
                };

                for (i, &r) in grid_a.iter_points().enumerate() {
                    mat[(i, i)] += pot(r) - energy;
                }

                let n = mat.ncols();
                mat = mat.remove_columns_at(&[0, n - 1]);
                mat = mat.remove_rows_at(&[0, n - 1]);

                let mat = mat.try_inverse().unwrap();

                // 2D grid part
                let n = ProductGrid::new([grid_a.clone(), grid_b.clone()])
                    .get_index_iterator()
                    .shrink_all(1, 1)
                    .count();
                let mut result: CooMatrix<C64> = CooMatrix::new(n, n);

                for (i, _) in grid_b
                    .iter_points()
                    .skip(1)
                    .take(grid_b.len() - 2)
                    .enumerate()
                {
                    let pos = i * mat.nrows();
                    result.push_matrix(pos, pos, &mat);
                }

                let pred = SparseMatrix((&result).into());

                ModelSolver::Sparse(COCRPreconditionedSolver::new(
                    lhs,
                    pred,
                    rhs,
                    FullVector::zero(rhs.nrows()),
                ))
            }
            Self::PartialInvert => {
                let [grid_a, grid_b] = model_params
                    .ordering
                    .to_me([&model_params.grid_m, &model_params.grid_e]);

                let [n_a, n_b] = [grid_a.len() - 2, grid_b.len() - 2];
                let n = n_a * n_b;
                let mut pred = CooMatrix::<C64>::new(n, n);

                for block_idx in 0..n_b {
                    let block_start = (block_idx * n_a) as isize;

                    let mut submatrix = DMatrix::<C64>::zeros(n_a, n_a);

                    for (i, j, v) in lhs.0.triplet_iter() {
                        let i = i as isize - block_start;
                        let j = j as isize - block_start;

                        if i < 0 || j < 0 || j >= n_a as isize {
                            continue;
                        }

                        if i >= n_a as isize {
                            break;
                        }

                        submatrix[(i as usize, j as usize)] = *v;
                    }

                    if !submatrix.try_inverse_mut() {
                        panic!("Failed to invert the submatrix");
                    }

                    pred.push_matrix(block_start as usize, block_start as usize, &submatrix);
                }

                // Noop
                ModelSolver::Sparse(COCRPreconditionedSolver::new(
                    lhs,
                    SparseMatrix((&pred).into()),
                    rhs,
                    FullVector::zero(rhs.nrows()),
                ))
            }
        }
    }
}
