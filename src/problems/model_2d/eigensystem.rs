use std::{ops::DivAssign, sync::Arc};

use nalgebra::{Dynamic, SymmetricEigen};
use permutation::Permutation;

use crate::{
    models::Model2D,
    primitives::{grid::QuadratureGrid, FullVector, Vector, C64},
};

pub struct Eigensystem {
    eigen: SymmetricEigen<C64, Dynamic>,
    perm: Permutation,
}

pub type ColumnSlice<'a> = nalgebra::Matrix<
    nalgebra::Complex<f64>,
    Dynamic,
    nalgebra::Const<1>,
    nalgebra::SliceStorage<
        'a,
        nalgebra::Complex<f64>,
        Dynamic,
        nalgebra::Const<1>,
        nalgebra::Const<1>,
        Dynamic,
    >,
>;

impl Eigensystem {
    pub fn get_molecule_eigensystem(
        model: &'static Model2D,
        subgrid_m: &QuadratureGrid<C64, C64>,
    ) -> Self {
        let mut mat = subgrid_m.stiffness_matrix_whole((-0.5 / model.mu).into());

        // Add potential
        for i in 0..mat.nrows() {
            mat.push(i, i, model.potential_0(*subgrid_m.get_point(i)));
        }

        let mat: nalgebra::DMatrix<C64> = (&mat).into();
        let dim = mat.nrows();
        let mat = mat.remove_columns_at(&[0, dim - 1]);
        let mat = mat.remove_rows_at(&[0, dim - 1]);

        let mut eigen = mat.symmetric_eigen();
        let perm = permutation::sort_by(eigen.eigenvalues.as_slice(), |a, b| {
            a.partial_cmp(b).unwrap()
        })
        .inverse();

        subgrid_m
            .iter_weights()
            .skip(1)
            .zip(eigen.eigenvectors.row_iter_mut())
            .for_each(|(w, mut r)| {
                r.div_assign(w.sqrt());
            });

        Self { eigen, perm }
    }

    pub fn get_extended_triplet(
        &self,
        idx: usize,
        total_len: usize,
    ) -> Arc<(f64, FullVector<C64>, usize)> {
        let mut next = FullVector::<C64>::zero(total_len);
        let v = self.get_evector(idx);

        next.as_mut_slice()[1..=v.len()].copy_from_slice(v.as_slice());

        Arc::new((self.get_evalue(idx), next, idx))
    }

    pub fn get_evalue(&self, idx: usize) -> f64 {
        self.eigen.eigenvalues[self.perm.apply_idx(idx)]
    }

    pub fn get_evector(&self, idx: usize) -> ColumnSlice {
        self.eigen.eigenvectors.column(self.perm.apply_idx(idx))
    }
}
