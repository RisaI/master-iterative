use clap::{App, Arg, ArgMatches};
use num_traits::Zero;

use crate::primitives::{
    grid::{Grid, ProductGrid, Quadrature, QuadratureGrid},
    C64,
};

use super::Problem;

pub fn get_problems() -> Vec<Problem> {
    [Problem {
        id: "prodgrid".into(),
        arg: App::new("prodgrid").args(&[
            Arg::with_name("reverse")
                .long("reverse")
                .short("r")
                .takes_value(false)
                .required(false),
            Arg::with_name("borderless")
                .long("borderless")
                .short("b")
                .takes_value(false)
                .required(false),
            Arg::with_name("DIM").index(1).required(true),
        ]),
        func: print_prodgrid,
    }]
    .into()
}

fn print_prodgrid(matches: &ArgMatches) {
    let grid_x: QuadratureGrid<C64, C64> = {
        let mut grid = Grid::empty();

        grid.append_many([1., 2., 3.]);

        grid.into_quadrature_grid(Quadrature::GaussLobatto(5))
    };

    let n = matches.value_of("DIM").unwrap().parse::<usize>().unwrap();
    let borderless = matches.is_present("borderless");

    let stiffness: nalgebra_sparse::CsrMatrix<C64> = (&match n {
        1 => grid_x.stiffness_matrix_whole(1.0.into()),
        2 => {
            let grid_y: QuadratureGrid<C64, C64> = {
                let mut grid = Grid::empty();

                grid.append_equidist(1.0, 4);

                grid.into_quadrature_grid(Quadrature::GaussLobatto(5))
            };

            let grid = ProductGrid::new(if matches.is_present("reverse") {
                [grid_y, grid_x]
            } else {
                [grid_x, grid_y]
            });

            grid.stiffness_matrix([1., 1.], borderless)
        }
        3 => {
            let grid = ProductGrid::new([grid_x.clone(), grid_x.clone(), grid_x]);

            grid.stiffness_matrix([1., 1., 1.], borderless)
        }
        _ => panic!(),
    })
        .into();

    for i in 0..stiffness.nrows() {
        for j in 0..stiffness.ncols() {
            let val = match stiffness.get_entry(i, j).unwrap() {
                nalgebra_sparse::SparseEntry::Zero => 0.0,
                nalgebra_sparse::SparseEntry::NonZero(v) => v.re,
            };

            if val.is_zero() {
                print!("{:.1}", val)
            } else {
                print!("{:.4}", val);
            }

            if j + 1 < stiffness.ncols() {
                print!(",");
            }
        }
        println!();
    }
}
