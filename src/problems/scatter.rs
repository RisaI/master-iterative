use crate::{
    primitives::{
        grid::*, physics::RadialHamiltionian, shrink_vec, FullMatrix, FullVector, Indexed, Matrix,
        Vector, C64,
    },
    solvers::iterative::{self, COCRSolver, IterativeMethod},
};
use clap::{App, ArgMatches};
use num_traits::Zero;

use super::Problem;

type Numeric = C64;
const PRECISION: f64 = 1e-5;
const MAX_ITERS: usize = 100_000;
const M: f64 = 1.0;
const L: usize = 1;

pub fn get_problems() -> Vec<Problem> {
    [
        Problem {
            id: "scatter-cross".into(),
            arg: App::new("scatter-cross"),
            func: get_cross_sections,
        },
        Problem {
            id: "scatter-wave".into(),
            arg: App::new("scatter-wave"),
            func: get_wave_func,
        },
    ]
    .into()
}

fn get_cross_sections(_matches: &ArgMatches) {
    use rayon::prelude::*;
    const STEPS: usize = 1_00;
    const FROM: f64 = 0.2;
    const TO: f64 = 0.4;

    let data: Vec<(f64, f64)> = (0..=STEPS)
        .into_par_iter()
        .map(|i| {
            let k = FROM + (TO - FROM) * i as f64 / (STEPS as f64);
            let f = calculate_cross(k);

            println!("k = {:.3} -> f = {}", k, f);

            (k, f)
        })
        .collect();

    let trace = plotly::scatter::Scatter::new(data.iter().map(|p| p.0), data.iter().map(|p| p.1))
        .name("sigma(k)");

    let mut plot = plotly::Plot::new();
    plot.add_trace(trace);
    plot.show();
}

fn get_wave_func(_matches: &ArgMatches) {
    plot_solution(2.0)
}

pub fn calculate_cross(k: f64) -> f64 {
    let mut grid = Grid::<Numeric>::empty();

    grid.add_vert_unchecked(1e-2);
    grid.append_equidist(16., 20);
    grid.append_equidist(32., 10);
    grid.append_equidist(
        Numeric::new(0.0, std::f64::consts::FRAC_PI_4 * 40. / 45.).exp() * 128.0,
        10,
    );

    let grid = grid.into_quadrature_grid(Quadrature::GaussLobatto(15));

    fn potential(x: Numeric) -> Numeric {
        let x_0: Numeric = 0.0.into();
        let v_0: Numeric = (-0.03).into();
        let alpha: Numeric = 0.1.into();

        let dx = x - x_0;

        v_0 * (-alpha * dx.norm()).exp()
    }

    let psi_zero = |x: Numeric| {
        let scale = (x * k).im.powi(2);

        if scale > 100.0 {
            Numeric::zero()
        } else {
            crate::generators::bessels::bessel_fn(L)(x * k) / scale.exp() // * (2.0 * M / (std::f64::consts::PI * k)).sqrt()
        }
    };

    let norm = grid.integrate_fn(|x| psi_zero(x).norm_sqr().into()).norm();

    let rhs: FullVector<Numeric> = shrink_vec(
        FullVector::from(grid.represent_fn(|x| -potential(x) * psi_zero(x) / norm.sqrt())),
        1,
        1,
    );

    let h = RadialHamiltionian {
        angular_momentum: L,
        mass: M,
        energy_offset: (k * k / (2.0 * M)).into(),
        potential,
    };
    let lhs = crate::primitives::FullMatrix::from(&h.represent_on_grid(&grid))
        .shrink(1, 1, 1, 1)
        .into_sparse();

    // ANCHOR solve

    // let lhs = iterate::primitives::precond::symmetric_incomplete_cholesky(&lhs);
    // let rhs = lhs.apply_to_rhs(&rhs);

    let mut solver = COCRSolver::new(&lhs, &rhs, FullVector::zero(rhs.nrows()), None);
    let mut steps = 0;
    let start = std::time::Instant::now();
    let b_len = rhs.cplx_len_squared().sqrt().norm();

    loop {
        steps += 1;

        if let iterative::StepResult::Precision(prec) = solver.step() {
            let ratio = prec.norm() / b_len;

            // let norm: f64 = solver.get_current().as_slice().iter().map(|y| y.len_squared()).sum::<f64>().sqrt();

            // solver.get_current_mut().as_slice_mut().iter_mut().for_each(|el| *el = el.clone() / norm);

            if ratio < PRECISION || steps >= MAX_ITERS {
                println!(
                    "|r| / |b| = {} in {} steps, {}ms",
                    // solver.get_current().data,
                    ratio,
                    steps,
                    std::time::Instant::now().duration_since(start).as_millis(),
                );

                let result: Vec<Numeric> = {
                    use std::iter::once;

                    once(0.0.into())
                        .chain(solver.get_current().as_slice().iter().copied())
                        .chain(once(0.0.into()))
                        .zip(grid.represent_fn(psi_zero).iter())
                        .map(|(a, b)| a + b)
                        .collect()
                };

                // Calculate cross-section
                // |<j_0|V|psi>
                let cross = {
                    let lhs: FullVector<_> = grid.represent_fn(psi_zero).into();
                    let dvr = h.represent_potential_dvr(&grid);
                    let rhs = dvr.mul_vec(&FullVector::from(result));

                    4.0 * std::f64::consts::PI.powi(3) * lhs.cplx_dot_prod(&rhs).norm_sqr()
                        / k.powi(2)
                };

                // To function repre
                // result.iter_mut().zip(grid.iter_weights())
                //     .for_each(|(v, w)| {
                //         *v /= w.sqrt()
                //     });

                // let trace = plotly::scatter::Scatter::new(grid.iter_points().map(|c| c.re), result.iter().map(|n| n.re)).name("Psi_{sc}");
                // let potential_plot =  plotly::scatter::Scatter::new(
                //     grid.iter_points().map(|x| x.re),
                //     grid.iter_points().map(|x| potential(x.clone()).re),
                // ).name("V");
                // let psi =  plotly::scatter::Scatter::new(
                //     grid.iter_points().map(|x| x.re),
                //     grid.iter_points().map(|x| psi_zero(x.clone()).re),
                // ).name("Psi_0");

                // let rhs =  plotly::scatter::Scatter::new(
                //     grid.iter_points().map(|x| x.re),
                //     grid.iter_points().map(|x| -potential(x.clone()).re * psi_zero(x.clone()).re),
                // ).name("RHS").visible(plotly::common::Visible::False);

                // let psi_sum =  plotly::scatter::Scatter::new(
                //     grid.iter_points().map(|x| x.re),
                //     grid.iter_points().map(|x| psi_zero(x.clone()).re)
                //         .zip(result.iter().map(|z| z.re))
                //         .map(|(p0,psc)| p0 + psc),
                // ).name("Psi_sum");

                // let mut plot = plotly::Plot::new();
                // plot.add_trace(trace);
                // plot.add_trace(potential_plot);
                // plot.add_trace(psi);
                // plot.add_trace(rhs);
                // plot.add_trace(psi_sum);

                // plot.show();

                return cross;
            } else {
                // println!("{}: |r|/|b| = {:.7}", steps, ratio);
            }
        } else {
            panic!("F");
        }
    }
}

pub fn plot_solution(k: f64) {
    let mut grid = Grid::<Numeric>::empty();

    grid.add_vert_unchecked(1e-2);
    grid.append_equidist(16., 20);
    grid.append_equidist(32., 10);
    grid.append_equidist(
        Numeric::new(0.0, std::f64::consts::FRAC_PI_4 * 40. / 45.).exp() * 32.0,
        10,
    );

    let grid = grid.into_quadrature_grid(Quadrature::GaussLobatto(15));

    fn potential(x: Numeric) -> Numeric {
        let x_0: Numeric = 0.0.into();
        let v_0: Numeric = (-0.03).into();
        let alpha: Numeric = 0.1.into();

        let dx = x - x_0;

        v_0 * (-alpha * dx.norm()).exp()
    }

    let psi_zero = |x: Numeric| {
        let scale = (x * k).im.powi(2);

        if scale > 100.0 {
            Numeric::zero()
        } else {
            crate::generators::bessels::bessel_fn(L)(x * k) / scale.exp()
        }
    };

    let norm = grid.integrate_fn(|x| psi_zero(x).norm_sqr().into()).norm();

    let mut rhs: FullVector<Numeric> =
        FullVector::from(grid.represent_fn(|x| -potential(x) * psi_zero(x) / norm.sqrt()));

    let h = RadialHamiltionian {
        angular_momentum: 1,
        mass: M,
        energy_offset: (k * k / (2.0 * M)).into(),
        potential,
    };
    let mut lhs = FullMatrix::from(&h.represent_on_grid(&grid));

    // Boundary cond
    let (left, right) = (0.0, 0.0);
    for i in 0..rhs.nrows() {
        rhs[i] -= *lhs.get([i, 0]) * left * (*grid.iter_weights().next().unwrap()).sqrt()
            + *lhs.get([i, lhs.cols() - 1]) * right * (*grid.iter_weights().last().unwrap()).sqrt();
    }

    rhs = shrink_vec(rhs, 1, 1);
    lhs = lhs.shrink(1, 1, 1, 1);

    let lhs = lhs.into_sparse();
    // SOLVE

    // let lhs = iterate::primitives::precond::symmetric_incomplete_cholesky(&lhs);
    // let rhs = lhs.apply_to_rhs(&rhs);

    let mut solver = COCRSolver::new(&lhs, &rhs, FullVector::zero(rhs.nrows()), None);
    let mut steps = 0;
    let start = std::time::Instant::now();
    let b_len = rhs.cplx_len_squared().sqrt().norm();

    loop {
        steps += 1;

        if let iterative::StepResult::Precision(prec) = solver.step() {
            let ratio = prec.norm() / b_len;

            if ratio < PRECISION || steps >= MAX_ITERS {
                println!(
                    "|r| / |b| = {} in {} steps, {}ms",
                    // solver.get_current().data,
                    ratio,
                    steps,
                    std::time::Instant::now().duration_since(start).as_millis(),
                );

                let mut result: Box<[Numeric]> = {
                    use std::iter::once;

                    once(left * grid.iter_points().next().unwrap().sqrt())
                        .chain(solver.get_current().as_slice().iter().copied())
                        .chain(once(right * grid.iter_points().last().unwrap().sqrt()))
                        .collect()
                };

                // To function repre
                result
                    .iter_mut()
                    .zip(grid.iter_weights())
                    .for_each(|(v, w)| *v /= w.sqrt());

                let trace = plotly::scatter::Scatter::new(
                    grid.iter_points().map(|c| c.re),
                    result.iter().map(|n| n.re),
                )
                .name("Psi_{sc}");
                let potential_plot = plotly::scatter::Scatter::new(
                    grid.iter_points().map(|x| x.re),
                    grid.iter_points().map(|x| potential(*x).re),
                )
                .name("V");
                let psi = plotly::scatter::Scatter::new(
                    grid.iter_points().map(|x| x.re),
                    grid.iter_points().map(|x| psi_zero(*x).re),
                )
                .name("Psi_0");

                let rhs = plotly::scatter::Scatter::new(
                    grid.iter_points().map(|x| x.re),
                    grid.iter_points()
                        .map(|x| -potential(*x).re * psi_zero(*x).re),
                )
                .name("RHS")
                .visible(plotly::common::Visible::False);

                let psi_sum = plotly::scatter::Scatter::new(
                    grid.iter_points().map(|x| x.re),
                    grid.iter_points()
                        .map(|x| psi_zero(*x))
                        .zip(result.iter())
                        .map(|(p0, psc)| (p0 + psc).re),
                )
                .name("Psi_sum");

                let psi_sqr = plotly::scatter::Scatter::new(
                    grid.iter_points().map(|x| x.re),
                    grid.iter_points()
                        .map(|x| psi_zero(*x))
                        .zip(result.iter())
                        .map(|(p0, psc)| (p0 + psc).norm_sqr()),
                )
                .name("Psi_sum^2");

                let mut plot = plotly::Plot::new();
                plot.add_trace(trace);
                plot.add_trace(potential_plot);
                plot.add_trace(psi);
                plot.add_trace(rhs);
                plot.add_trace(psi_sum);
                plot.add_trace(psi_sqr);

                plot.show();

                break;
            } else {
                // println!("{}: |r|/|b| = {:.7}", steps, ratio);
            }
        } else {
            panic!("F");
        }
    }
}
