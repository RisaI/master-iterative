use super::Problem;
use crate::{
    primitives::{FullMatrix, FullVector, Vector},
    solvers::iterative::{self, IterativeMethod},
};
use clap::{App, ArgMatches};
use num_traits::Zero;

const MAX_ITERS: usize = 10_000;

type Subnumeric = f64;
type Numeric = crate::primitives::Complex<Subnumeric>;

pub fn get_problems() -> Vec<Problem> {
    [Problem {
        id: "dwg961b".into(),
        arg: App::new("dwg961b"),
        func: solve_dwg961b,
    }]
    .into()
}

fn solve_dwg961b(_: &ArgMatches) {
    let (header, reader) = {
        let mat = crate::hb::HBReader::new(std::fs::File::open("dwg961b.rb").unwrap()).unwrap();

        mat.read().unwrap()
    };

    assert!(
        matches!(
            header.mat_type.symmetry,
            crate::hb::HBSymmetryType::Symmetric
        ),
        "The COCR algorithm only supports symmetric matrices"
    );
    let n = header.mat_type.cols;

    let a = FullMatrix::new(n, {
        let mut data = vec![Numeric::zero(); n.pow(2)];

        reader.iter().for_each(|r| {
            let val = match r.2 {
                crate::hb::SparseMatElem::Complex(re, im) => Numeric::new(
                    num_traits::FromPrimitive::from_f64(re).unwrap(),
                    num_traits::FromPrimitive::from_f64(im).unwrap(),
                ),
                crate::hb::SparseMatElem::Real(re) => Numeric::new(
                    num_traits::FromPrimitive::from_f64(re).unwrap(),
                    Subnumeric::zero(),
                ),
            };

            data[r.1 + r.0 * n] = val;

            if r.0 != r.1 {
                data[r.0 + r.1 * n] = val;
            }
        });

        data.into_boxed_slice()
    });

    let b: FullVector<_> = (0..n)
        .map(|_| {
            Numeric::new(
                num_traits::FromPrimitive::from_f64(1.0f64).unwrap(),
                num_traits::FromPrimitive::from_f64(1.0f64).unwrap(),
            )
        })
        .collect::<Vec<_>>()
        .into();

    let precond = crate::primitives::precond::ic0::symmetric_incomplete_cholesky(&a);
    let b: FullVector<Numeric> = precond.apply_to_rhs(&b);
    // let precond = a;

    println!("preconditioned");

    let b_len = b.cplx_len_squared().sqrt().norm();

    let mut solver = iterative::COCRSolver::new(&precond, &b, FullVector::zero(n), None);
    let mut steps = 0;
    let start = std::time::Instant::now();

    let mut history: Vec<Subnumeric> = Vec::with_capacity(MAX_ITERS / 2);

    loop {
        steps += 1;

        if let iterative::StepResult::Precision(prec) = solver.step() {
            let ratio = prec.norm() / b_len;

            history.push(ratio);

            if ratio < 1.2e-5 || steps >= MAX_ITERS {
                println!(
                    "|r| / |b| = {} in {} steps, {}ms",
                    // solver.get_current().data,
                    ratio,
                    steps,
                    std::time::Instant::now().duration_since(start).as_millis(),
                );

                let layout =
                    plotly::Layout::new().title(plotly::common::Title::new("Convergence graph"));

                let trace =
                    plotly::Scatter::new(1..=history.len(), history.iter().map(|h| h.log(10.0)));

                let mut plot = plotly::Plot::new();
                plot.set_layout(layout);
                plot.add_trace(trace);
                plot.show();

                break;
            } else {
                println!("{}: |r|/|b| = {:.7}", steps, ratio);
            }
        } else {
            break;
        }
    }
}
