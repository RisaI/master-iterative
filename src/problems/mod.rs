use clap::{App, Arg, ArgMatches};

pub mod dwg961b;
pub mod mathematica;
pub mod model_1d;
pub mod model_2d;
pub mod product_grid;
pub mod rand_matrix;
pub mod scatter;
pub mod stiffness_test;

pub struct Problem {
    pub id: String,
    pub arg: App<'static, 'static>,
    pub func: fn(&ArgMatches),
}

impl Problem {
    pub fn new(name: impl Into<String>, func: fn(&ArgMatches)) -> Self {
        let name = name.into();

        Self {
            id: name.clone(),
            func,
            arg: App::new(name),
        }
    }

    pub fn with_args(mut self, args: &[Arg<'static, 'static>]) -> Self {
        self.arg = self.arg.args(args);

        self
    }

    pub fn mutate_app(mut self, f: impl FnOnce(App) -> App<'static, 'static>) -> Self {
        self.arg = f(self.arg);

        self
    }
}

pub fn get_problems() -> Vec<Problem> {
    [
        [Problem {
            id: "integrate".into(),
            arg: App::new("integrate"),
            func: integrate_test,
        }]
        .into(),
        dwg961b::get_problems(),
        rand_matrix::get_problems(),
        scatter::get_problems(),
        mathematica::get_problems(),
        model_1d::get_problems(),
        product_grid::get_problems(),
        model_2d::get_problems(),
        stiffness_test::get_problems(),
    ]
    .into_iter()
    .flatten()
    .collect()
}

fn integrate_test(_: &ArgMatches) {
    use crate::primitives::grid::{Grid, Quadrature};
    let mut grid: Grid<f64> = Grid::empty();

    grid.add_vert(0.0);
    grid.append_equidist(10.0, 10);

    let grid = grid.into_quadrature_grid(Quadrature::GaussLobatto(5));

    println!("\\int_0^10 x^2 = {}", grid.integrate_fn(|x| x * x));
}
