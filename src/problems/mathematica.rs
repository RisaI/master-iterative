use super::Problem;
use crate::{
    primitives::{shrink_vec, FullMatrix, FullVector, Indexed, Matrix, Vector},
    solvers::iterative::{self, IterativeMethod},
};
use clap::ArgMatches;
use num_traits::Zero;
use plotly::Scatter;

const MAX_ITERS: usize = 10_000;

type Numeric = crate::primitives::C64;

pub fn get_problems() -> Vec<Problem> {
    vec![Problem::new("mathematica", solve_mathematica_problem)]
}

fn solve_mathematica_problem(_: &ArgMatches) {
    use crate::primitives::{grid::*, physics::RadialHamiltionian};

    let mut grid = Grid::<Numeric>::empty();

    grid.append_equidist(0.50, 4);
    grid.append_equidist(0.25, 20);
    grid.append_equidist(0.25, 30);

    let grid = grid.into_quadrature_grid(Quadrature::GaussLobatto(15));

    fn rhs_fn(x: Numeric) -> Numeric {
        let factor: Numeric = Numeric::from(20.0);
        let pi = Numeric::from(std::f64::consts::PI);
        let x3 = x * x * x;
        let inner = Numeric::from(20.0) * pi * x3;

        factor
            * (Numeric::from(-1.0) + Numeric::from(3.0) * pi * x * Numeric::from(inner.re.sin())
                - Numeric::from(90.0) * pi * pi * x3 * x * Numeric::from(inner.re.sin()))
    }

    let mut rhs: FullVector<Numeric> = FullVector::from(grid.represent_fn(rhs_fn));

    let mut lhs: FullMatrix<Numeric> = (&RadialHamiltionian {
        angular_momentum: Zero::zero(),
        mass: 1.0,
        energy_offset: 0.0.into(),
        potential: |_| 0.0.into(),
    }
    .represent_on_grid(&grid))
        .into();

    // ANCHOR Boundary cond
    let (left, right) = (1.0, 3.0);
    for i in 0..rhs.nrows() {
        rhs[i] -= *lhs.get([i, 0]) * left * (*grid.iter_weights().next().unwrap()).sqrt()
            + *lhs.get([i, lhs.cols() - 1]) * right * (*grid.iter_weights().last().unwrap()).sqrt();
    }

    rhs = shrink_vec(rhs, 1, 1);
    lhs = lhs.shrink(1, 1, 1, 1);

    // ANCHOR solve

    let mut solver = iterative::COCRSolver::new(&lhs, &rhs, FullVector::zero(rhs.nrows()), None);
    let mut steps = 0;
    let start = std::time::Instant::now();
    let b_len = rhs.cplx_len_squared().sqrt().norm();

    loop {
        steps += 1;

        if let iterative::StepResult::Precision(prec) = solver.step() {
            let ratio = prec.norm() / b_len;

            if ratio < 1e-6 || steps >= MAX_ITERS {
                println!(
                    "|r| / |b| = {} in {} steps, {}ms",
                    ratio,
                    steps,
                    std::time::Instant::now().duration_since(start).as_millis(),
                );

                let result = {
                    use std::iter::once;
                    let middle = solver
                        .get_current()
                        .as_slice()
                        .iter()
                        .zip(grid.iter_weights().skip(1))
                        .map(|(y, w)| (*y / w.sqrt()).re);

                    once(left).chain(middle).chain(once(right))
                };

                let trace: Box<Scatter<f64, f64>> =
                    Scatter::new(grid.iter_points().map(|c| c.re), result).name("f");

                let mut plot = plotly::Plot::new();
                plot.add_trace(trace);

                plot.show();

                break;
            } else {
                println!(
                    "{}: |r|/|b| = {:.7}, |r| = {:.7}",
                    steps,
                    ratio,
                    prec.norm()
                );
            }
        } else {
            break;
        }
    }
}
