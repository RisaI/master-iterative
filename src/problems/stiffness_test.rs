use crate::primitives::{
    grid::{Grid, Quadrature, QuadratureGrid},
    FullVector, SparseMatrix, C64,
};
use clap::{Arg, ArgMatches};
use nalgebra::Normed;
use plotly::{common::Title, layout::Axis, Layout, Plot, Scatter};

use super::Problem;

pub fn get_problems() -> Vec<Problem> {
    vec![Problem::new("stiff_test", stiff_test)
        .with_args(&[Arg::with_name("ORDER").long("order").takes_value(true)])]
}

struct StiffTestParams {
    pub order: usize,
    pub density: usize,
}

struct StiffTestResult {
    pub grid: QuadratureGrid<C64, C64>,
    pub analytic_f: Vec<C64>,
    pub analytic_f_d2: Vec<C64>,
    pub calculated_f_d2: Vec<C64>,
}

impl StiffTestResult {
    #[allow(dead_code)]
    pub fn render(&self, plot: &mut Plot) {
        plot.add_traces(vec![
            Scatter::new(
                self.grid.iter_points().map(|x| x.re),
                self.analytic_f.iter().map(|x| x.re),
            )
            .name("Analytic f(x)"),
            Scatter::new(
                self.grid.iter_points().map(|x| x.re),
                self.analytic_f_d2.iter().map(|y| y.re),
            )
            .name("Analytic f''(x)"),
            Scatter::new(
                self.grid.iter_points().map(|x| x.re),
                self.calculated_f_d2.iter().map(|y| y.re),
            )
            .name("Calculated f''(x)"),
            Scatter::new(
                self.grid.iter_points().skip(1).map(|x| x.re),
                self.iter_diff().map(|y| y.re),
            )
            .name("Δf''(x)"),
            Scatter::new(
                self.grid.iter_points().skip(1).map(|x| x.re),
                self.iter_diff()
                    .zip(self.analytic_f_d2.iter().skip(1))
                    .map(|(y, f)| if f.norm() < 1e-6 { 0. } else { (y / f).norm() }),
            )
            .name("Normalized Δf''(x)"),
        ]);
    }

    pub fn max_error(&self) -> f64 {
        self.iter_diff().map(|d| d.norm()).reduce(f64::max).unwrap()
    }

    pub fn mean_error(&self) -> f64 {
        self.iter_diff().map(|d| d.norm()).sum::<f64>() / self.iter_diff().count() as f64
    }

    pub fn iter_diff(&self) -> impl Iterator<Item = C64> + '_ {
        let len = self.calculated_f_d2.len() - 2;

        self.calculated_f_d2
            .iter()
            .zip(self.analytic_f_d2.iter())
            .skip(1)
            .take(len)
            .map(|(x, y)| *x - *y)
    }
}

fn stiff_test(_matches: &ArgMatches) {
    let order = _matches
        .value_of("ORDER")
        .map(|o| o.parse::<usize>().unwrap());

    let mut results: Vec<(String, Vec<f64>, Vec<f64>)> = vec![];

    // Order dependence
    {
        let mut x = vec![];
        let mut y_mean = vec![];
        let mut y_max = vec![];

        for order in 10..=40 {
            let params = StiffTestParams { order, density: 4 };
            let result = stiff_test_calc(&params);

            x.push(order as f64);
            y_mean.push(result.mean_error());
            y_max.push(result.max_error());
        }

        results.push(("order_mean".into(), x.clone(), y_mean));
        results.push(("order_max".into(), x.clone(), y_max));
    }

    // Density dependence
    {
        let mut x = vec![];
        let mut y_mean = vec![];
        let mut y_max = vec![];

        for density in 4..=40 {
            let params = StiffTestParams { order: 20, density };
            let result = stiff_test_calc(&params);

            x.push(density as f64);
            y_mean.push(result.mean_error());
            y_max.push(result.max_error());
        }

        results.push(("density_mean".into(), x.clone(), y_mean));
        results.push(("density_max".into(), x.clone(), y_max));
    }

    // Plot dependence
    {
        let mut plot = Plot::new();

        for (name, x, y) in results {
            plot.add_trace(Scatter::new(x, y).name(&name));
        }
        // result.render(&mut plot);

        plot.show();
    }

    // Surface dependence plot
    {
        let mut plot = Plot::new();

        let mut data: Vec<Vec<f64>> = vec![];
        let mut x: Vec<usize> = vec![];
        let mut y: Vec<usize> = vec![];

        for density in 6..=16 {
            y.push(density);
            let mut row = vec![];

            for order in 16..=28 {
                if density == 6 {
                    x.push(order);
                }
                let params = StiffTestParams { density, order };
                let result = stiff_test_calc(&params);

                row.push(result.mean_error());
            }
            data.push(row);
        }

        plot.add_trace(plotly::Surface::new(data).x(x).y(y).name("Error"));
        plot.set_layout(
            Layout::new()
                .x_axis(Axis::new().title(Title::new("Order")))
                .y_axis(Axis::new().title(Title::new("Density"))),
        );

        plot.show();
    }

    // Plot solution
    if let Some(order) = order {
        let mut plot = Plot::new();

        let params = StiffTestParams { density: 8, order };
        let result = stiff_test_calc(&params);
        result.render(&mut plot);

        plot.show();
    }
}

fn stiff_test_calc(params: &StiffTestParams) -> StiffTestResult {
    let mut grid: Grid<C64> = Grid::empty();

    grid.append_many([1., 0.5]);
    grid.append_equidist(1.0, params.density);
    grid.append_many([0.5, 1., 2.]);
    // grid.append_many([1.5, 1., 0.5]);
    // grid.append_equidist(4., 16);
    // grid.append_many([0.5, 1., 1.5]);

    grid.append_equidist(C64::new(15., 15.), 4);

    // const PREFACTOR: f64 = -0.5 / 12_766.36;
    const PREFACTOR: f64 = 1.;
    const MU: f64 = 2.03;
    const SIGMA: f64 = 0.006;
    let f = |x: C64| (-(x - MU).powi(2) / (2. * SIGMA)).exp();
    let f_d2 = |x: C64| f(x) * ((x - MU).powi(2) - SIGMA) / SIGMA.powi(2);

    let grid = grid.into_quadrature_grid(Quadrature::GaussLobatto(params.order));

    let f_repre = grid.represent_fn_dvr(f);
    let mut f_d2_repre = grid.represent_fn_dvr(f_d2);

    f_d2_repre.iter_mut().for_each(|y| *y *= PREFACTOR);

    let stiffy: SparseMatrix<_> = (&grid.stiffness_matrix_whole(PREFACTOR.into())).into();

    let mut f_d2_calculated_repre: FullVector<_> =
        &stiffy * &FullVector::from(grid.represent_fn(f));

    f_d2_calculated_repre
        .as_mut_slice()
        .iter_mut()
        .zip(grid.iter_weights())
        .for_each(|(y, w)| *y /= w.sqrt());

    if let Some(v) = f_d2_calculated_repre.as_mut_slice().first_mut() {
        *v = 0.0.into();
    }
    if let Some(v) = f_d2_calculated_repre.as_mut_slice().last_mut() {
        *v = 0.0.into();
    }

    StiffTestResult {
        grid,
        analytic_f: f_repre,
        analytic_f_d2: f_d2_repre,
        calculated_f_d2: f_d2_calculated_repre.into_iter().copied().collect(),
    }
}
