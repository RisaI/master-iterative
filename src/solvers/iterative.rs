use crate::primitives::ScalarField;
use crate::primitives::Vector;

// Reexport methods
pub use super::cocr::*;
pub use super::jacobi::*;

pub enum StepResult<T: ScalarField> {
    Failure,
    Exact,
    Precision(T),
}

pub trait IterativeMethod {
    type ScalarType: ScalarField;
    type WorkVec: Vector;

    fn get_current(&self) -> &Self::WorkVec;
    fn step(&mut self) -> StepResult<Self::ScalarType>;
}
