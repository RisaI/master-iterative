use std::ops::Mul;

use simba::scalar::ComplexField;

use crate::primitives::{Matrix, TriangularMatrix, Vector};

pub struct COCRSolver<'a, W, T, M> {
    a: &'a M,

    x: W,
    p: W,
    q: W,
    r: W,

    alpha: T,
    beta: T,

    residuum_inverse: Option<TriangularMatrix<T, true>>,
}

// use rayon::prelude::*;

use super::iterative::IterativeMethod;

impl<'a, W, T, M> COCRSolver<'a, W, T, M>
where
    W: Vector<Element = T> + 'static,
    T: ComplexField + Copy,
    for<'b> &'a M: Mul<&'b W, Output = W>,
{
    pub fn new<V: Vector<Element = T> + Clone + Into<W>>(
        a: &'a M,
        b: &'a V,
        x0: W,
        residuum_inverse: Option<TriangularMatrix<T, true>>,
    ) -> Self {
        // assert_eq!(a.rows(), a.cols()); // Must be a square matrix!
        // assert_eq!(a.cols(), b.rows()); // The LHS must be compatible with the RHS

        let r: W = b.clone().into() - a * &x0;

        Self {
            a, // The problem

            x: x0,
            p: W::zero(b.rows()),
            q: W::zero(b.rows()),
            r,

            alpha: T::zero(),
            beta: T::zero(),
            residuum_inverse,
        }
    }

    pub fn set_current<F: Fn(usize) -> T>(&mut self, f: F) {
        for i in 0..self.x.rows() {
            self.x[i] = f(i);
        }
    }

    pub fn get_current(&self) -> &W {
        &self.x
    }

    pub fn get_current_mut(&mut self) -> &mut W {
        &mut self.x
    }
}

impl<'a, W, T, M> IterativeMethod for COCRSolver<'a, W, T, M>
where
    W: Vector<Element = T> + Clone + 'static,
    T: ComplexField + Copy,
    for<'b> &'a M: Mul<&'b W, Output = W>,
{
    type ScalarType = T;
    type WorkVec = W;

    fn get_current(&self) -> &Self::WorkVec {
        &self.x
    }

    fn step(&mut self) -> super::iterative::StepResult<T> {
        // p_n = r_n + \beta_{n-1} p_{n-1}
        self.p.scale(self.beta);
        self.p.add_assign_ref(&self.r);

        // let a_form = TwoForm::wrap(self.a);

        // A p_n
        self.q = self.a * &self.p;

        // (\bar{r_n}, A r_n)
        // let rar = a_form.product(&self.r, &self.r);
        let rar = self.r.dot_prod(&(self.a * &self.r));

        // \alpha_n = (r_n, Ar_n) / (\bar{Ap_n}, Ap_n)
        self.alpha = rar / self.q.len_squared();

        // x_{n+1} = x_{n} + \alpha_n p_n
        self.x.add_assign_ref_scaled(&self.p, self.alpha);

        // r_{n+1} = r_n - \alpha_n Ap_n
        self.r.add_assign_ref_scaled(&self.q, -self.alpha);

        // \beta_n = (\bar{r_{n+1}}, Ar_{n+1}) / (\bar{r_n}, Ar_n)
        // self.beta = a_form.product(&self.r, &self.r) / rar;
        self.beta = self.r.dot_prod(&(self.a * &self.r)) / rar;

        super::iterative::StepResult::Precision(if let Some(ref t) = self.residuum_inverse {
            t.mul_vec(&self.r).cplx_len_squared().sqrt()
        } else {
            self.r.cplx_len_squared().sqrt()
        })
    }
}
