use super::iterative::IterativeMethod;
use crate::primitives::{FullVector, Vector};
use simba::scalar::ComplexField;
use std::ops::Mul;

/// https://arxiv.org/abs/1405.6297
pub struct COCRPreconditionedSolver<'a, W, T, M, P> {
    a: &'a M,
    prec: P,

    x: W,
    r: W,
    r_tilde: W,
    p: W,
    q: W,

    alpha: T,
    beta: T,
    rho: T,
}

impl<'a, W, T, M, P> COCRPreconditionedSolver<'a, W, T, M, P>
where
    W: Vector<Element = T> + Clone + 'static,
    T: ComplexField + Copy,
    for<'b> &'a M: Mul<&'b W, Output = W>,
    for<'b, 'c> &'c P: Mul<&'b W, Output = W>,
{
    pub fn new<V: Vector<Element = T> + Clone + Into<W>>(
        a: &'a M,
        prec: P,
        b: &'a V,
        x0: W,
    ) -> Self {
        // assert_eq!(a.rows(), a.cols()); // Must be a square matrix!
        // assert_eq!(a.ncols(), b.rows()); // The LHS must be compatible with the RHS

        let r: W = b.clone().into() - a * &x0;
        let r_tilde = &prec * &r;
        let q = a * &r_tilde;
        let rho = r_tilde.dot_prod(&q);

        Self {
            a, // The problem
            prec,

            x: x0,
            p: r_tilde.clone(),
            q,
            r,
            r_tilde,

            alpha: T::zero(),
            beta: T::zero(),
            rho,
        }
    }

    pub fn set_current<F: Fn(usize) -> T>(&mut self, f: F) {
        for i in 0..self.x.rows() {
            self.x[i] = f(i);
        }
    }

    pub fn get_current(&self) -> &W {
        &self.x
    }

    pub fn get_current_mut(&mut self) -> &mut W {
        &mut self.x
    }
}

impl<'a, T, M, P> IterativeMethod for COCRPreconditionedSolver<'a, FullVector<T>, T, M, P>
where
    T: ComplexField + From<T::RealField> + Copy,
    for<'b> &'a M: Mul<&'b FullVector<T>, Output = FullVector<T>>,
    for<'b, 'c> &'c P: Mul<&'b FullVector<T>, Output = FullVector<T>>,
{
    type ScalarType = T;
    type WorkVec = FullVector<T>;

    fn get_current(&self) -> &Self::WorkVec {
        &self.x
    }

    fn step(&mut self) -> super::iterative::StepResult<T> {
        let q_tilde = &self.prec * &self.q;

        self.alpha = self.rho / q_tilde.dot_prod(&self.q);

        self.x += &self.p * self.alpha;
        self.r -= &self.q * self.alpha;
        self.r_tilde -= &q_tilde * self.alpha;

        let t = self.a * &self.r_tilde;

        self.beta = self.rho;
        self.rho = self.r_tilde.dot(&t);
        self.beta = self.rho / self.beta;

        self.p = &self.r_tilde + &self.p * self.beta;
        self.q = t + &self.q * self.beta;

        super::iterative::StepResult::Precision(self.r.norm().into())
    }
}
