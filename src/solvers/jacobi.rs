use std::marker::PhantomData;

use crate::primitives::{Matrix, ScalarField, Vector};

pub struct JacobiSolver<
    'a,
    W: Vector<Element = T> + Send,
    T: ScalarField,
    M: Matrix<Element = T>,
    V: Vector<Element = T> + Clone,
> {
    a: &'a M,
    b: &'a V,

    x: W,
    work_vec: W,
    phantom: PhantomData<T>,
}

use nalgebra::{ComplexField, Scalar};

use super::iterative::IterativeMethod;

impl<'a, W, T, M, V> JacobiSolver<'a, W, T, M, V>
where
    W: Vector<Element = T> + Send + Sync,
    T: ScalarField,
    M: Matrix<Element = T> + Send + Sync,
    V: Vector<Element = T> + Clone,
{
    pub fn new(a: &'a M, b: &'a V) -> Self {
        assert_eq!(a.rows(), a.cols()); // Must be a square matrix!
        assert_eq!(a.cols(), b.rows()); // The LHS must be compatible with the RHS

        Self {
            a,
            b,
            x: W::zero(a.cols()),
            work_vec: W::zero(a.cols()),
            phantom: Default::default(),
        }
    }

    pub fn set_current<F: Fn(usize) -> T>(&mut self, f: F) {
        for i in 0..self.x.rows() {
            self.x[i] = f(i);
        }
    }

    pub fn get_current(&self) -> &W {
        &self.x
    }
}

impl<'a, W, T, M, V> IterativeMethod for JacobiSolver<'a, W, T, M, V>
where
    W: Vector<Element = T> + Send + Sync,
    T: ComplexField + Scalar + Send + Sync + Copy,
    M: Matrix<Element = T> + Send + Sync,
    V: Vector<Element = T> + Send + Sync + Clone,
{
    type ScalarType = T;
    type WorkVec = W;

    fn get_current(&self) -> &Self::WorkVec {
        &self.x
    }

    #[allow(unused_variables)]
    fn step(&mut self) -> super::iterative::StepResult<T> {
        let threads = rayon::current_num_threads();

        let per_thread = self.x.rows() / threads + if self.x.rows() % threads > 0 { 1 } else { 0 };

        let (a, b, x) = (&self.a, &self.b, &self.x);
        let work_vec = &mut self.work_vec;

        // work_vec
        //     .as_slice_mut()
        //     .par_chunks_mut(per_thread)
        //     .enumerate()
        //     .for_each(move |(chunk_idx, chunk)| {
        //         for (idx, v) in chunk.iter_mut().enumerate() {
        //             let i = idx + chunk_idx * per_thread;

        //             let lu = {
        //                 let mut acc = T::zero();

        //                 for j in 0..x.rows() {
        //                     if i == j {
        //                         continue;
        //                     }

        //                     acc += *a.get([i, j]) * x[j];
        //                 }

        //                 acc
        //             };

        //             *v = (b[i] - lu) / *a.get([i, i]);
        //         }
        //     });

        std::mem::swap(&mut self.x, &mut self.work_vec);

        // let dist = self.a.mul_vec(&self.x).dist_squared(self.b).sqrt();
        let dist = self.x.dist_squared(&self.work_vec).sqrt();

        super::iterative::StepResult::Precision(dist)
    }
}
