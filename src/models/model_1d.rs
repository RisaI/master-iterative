use crate::primitives::C64;
use nalgebra::ComplexField;
use num_traits::Zero;

/// A toy model used for two-channel hydrogen-like calculations
pub const TOY_MODEL: Model1D<2> = Model1D {
    mass: 1.,
    r0: 1.,
    energy_levels: [0., 2.],
    angular_momentums: [0, 0],
    potentials: [[-2., -0.5], [-0.5, -2.]],
};

pub struct Model1D<const CHANNELS: usize> {
    pub mass: f64,
    pub r0: f64,
    pub energy_levels: [f64; CHANNELS],
    pub angular_momentums: [usize; CHANNELS],
    pub potentials: [[f64; CHANNELS]; CHANNELS],
}

impl<const CHANNELS: usize> Model1D<CHANNELS> {
    pub fn num_channels(&self) -> usize {
        CHANNELS
    }

    pub fn potential<N>(&self, i: usize, j: usize) -> impl Fn(C64) -> N
    where
        N: From<f64>,
    {
        let potentials = self.potentials;
        let r0 = self.r0;

        move |x| {
            N::from(if (r0 - x.re).abs() < 1e-3 {
                potentials[i][j] / 2.
            } else if x.re < r0 {
                potentials[i][j]
            } else {
                0.
            })
        }
    }

    /// Compute the analytic solution for a scatter problem
    pub fn analytic_scatter_solution(&self, init: usize, f: usize, e: f64) -> f64 {
        use std::f64::consts::PI;

        let ks: [C64; CHANNELS] = self
            .energy_levels
            .map(|eps| C64::from(2.0 * self.mass * (e - eps)).sqrt());

        let [rr1, rr2] =
            [0, 1].map(|i| ks[i].powi(2) - 2. * self.potentials[i][i] * self.r0.powi(2));

        let ee1 = 0.5
            * (rr1
                + rr2
                + ((rr1 - rr2).powi(2) + 16. * self.potentials[0][1].powi(2) * self.r0.powi(4))
                    .sqrt());
        let ee2 = 0.5
            * (rr1 + rr2
                - ((rr1 - rr2).powi(2) + 16. * self.potentials[0][1].powi(2) * self.r0.powi(4))
                    .sqrt());

        let b1 = ee1 - rr1;
        let b2 = ee2 - rr1;

        let c1 = ee1.sqrt() / ee1.sqrt().tan();
        let c2 = ee2.sqrt() / ee2.sqrt().tan();

        let i = C64::i();

        let gg = |k1: C64, k2: C64| {
            i * (k1 * (c1 * b1 - c2 * b2) + k2 * (c2 * b1 - c1 * b2))
                + (b1 - b2) * (k1 * k2 - c1 * c2)
        };

        (match (init, f) {
            (0, 0) | (1, 1) => {
                let m = (-1.0f64).powi(init as i32);

                1. - gg(-m * ks[0], m * ks[1]) * (-2. * i * ks[init]).exp() / gg(ks[0], ks[1])
            }

            (0, 1) | (1, 0) => {
                2. * (c1 - c2) * (b1 * b2 * ks[0] * ks[1]).sqrt() * (-i * (ks[0] + ks[1])).exp()
                    / gg(ks[0], ks[1])
            }
            _ => C64::zero(),
        })
        .norm_sqr()
            * PI
            / ks[init].norm_sqr()
    }

    pub fn initial_state(&self, l: usize, k: C64, m: f64) -> impl (Fn(C64) -> C64) {
        move |x| {
            let scale = (x * k).im.powi(2);

            if scale > 10.0 {
                C64::zero()
            } else {
                (2.0 * m / (std::f64::consts::PI * k)).sqrt()
                    * crate::generators::bessels::bessel_fn(l)(x * k)
                    / scale.exp()
            }
        }
    }
}
