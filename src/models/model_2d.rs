use crate::primitives::{grid::Grid, C64};
use num_traits::Zero;
use std::f64::consts::FRAC_PI_4;

fn cplx_rf(r: f64, a: f64) -> C64 {
    C64 {
        re: r * a.cos(),
        im: r * a.sin(),
    }
}

const RAD_40DEG: f64 = 0.69813170079;

fn grid_m_n2() -> Grid<C64> {
    let mut grid = Grid::empty();

    grid.append_many([1., 0.5])
        .append_equidist(1.5, 6)
        .append_many([1., 2.])
        .append_many([cplx_rf(5., FRAC_PI_4), cplx_rf(15., FRAC_PI_4)])
        .append_equidist(cplx_rf(80., FRAC_PI_4), 4);

    grid
}

fn grid_e_n2() -> Grid<C64> {
    let mut grid = Grid::empty();

    grid.append_many([1., 2., 3., 4.])
        .append_equidist(40., 8)
        .append_many([cplx_rf(5., RAD_40DEG), cplx_rf(15., RAD_40DEG)])
        .append_equidist(cplx_rf(80., RAD_40DEG), 4);

    grid
}

pub static MODEL_2D_N2: Model2D = Model2D {
    name: "N_2",
    mu: 12_766.36,
    l: 2, // d wave

    mol_pot: MolecularPotential::Morse {
        d_0: 0.751_02,
        alpha_0: 1.153_5,
        r_0: 2.019_43,
    },

    lambda_infty: 6.210_66,
    int_pot: InteractionPotential::FourParams {
        lambda_c: 5.380_22,
        lambda_one: 1.057_08,
        r_c: 2.405,
        r_lambda: -27.983_3,
    },
    alpha_c: 0.4,

    build_grid_e: grid_e_n2,
    build_grid_m: grid_m_n2,
};

fn grid_m_no() -> Grid<C64> {
    let mut grid = Grid::empty();

    grid.append_many([1.5, 0.5])
        .append_equidist(2., 8)
        .append_equidist(1.5, 3)
        .append_many([1., 2.])
        .append_many([
            cplx_rf(5., FRAC_PI_4),
            cplx_rf(15., FRAC_PI_4),
            cplx_rf(20., FRAC_PI_4),
            cplx_rf(20., FRAC_PI_4),
            cplx_rf(20., FRAC_PI_4),
            cplx_rf(20., FRAC_PI_4),
        ]);

    grid
}

fn grid_e_no() -> Grid<C64> {
    let mut grid = Grid::empty();

    grid.append_many([1., 2., 3., 4.])
        .append_equidist(40., 8)
        .append_many([cplx_rf(5., RAD_40DEG), cplx_rf(15., RAD_40DEG)])
        .append_equidist(cplx_rf(80., RAD_40DEG), 4);

    grid
}

pub static MODEL_2D_NO: Model2D = Model2D {
    name: "NO",
    mu: 13_614.16,
    l: 1, // p wave

    mol_pot: MolecularPotential::Morse {
        d_0: 0.236_3,
        alpha_0: 1.571,
        r_0: 2.157,
    },

    lambda_infty: 6.367,
    int_pot: InteractionPotential::FourParams {
        lambda_c: 6.05,
        lambda_one: 5.,
        r_c: 2.285,
        r_lambda: 2.084_3,
    },
    alpha_c: 1.,

    build_grid_e: grid_e_no,
    build_grid_m: grid_m_no,
};

fn grid_m_f2() -> Grid<C64> {
    let mut grid = Grid::empty();

    grid.add_vert_unchecked(C64::new(2., 0.))
        .append_equidist(8., 80)
        .append_equidist(cplx_rf(15., FRAC_PI_4), 40);

    grid
}

fn grid_e_f2() -> Grid<C64> {
    let mut grid = Grid::empty();

    grid.append_many([0.5, 0.5, 0.5, 1., 2.])
        .append_equidist(60., 8)
        .append_many([cplx_rf(5., RAD_40DEG), cplx_rf(15., RAD_40DEG)])
        .append_equidist(cplx_rf(80., RAD_40DEG), 4);

    grid
}

pub static MODEL_2D_F2: Model2D = Model2D {
    name: "F_2",
    mu: 17_315.99,
    l: 1, // p wave

    mol_pot: MolecularPotential::Morse {
        d_0: 0.059_8,
        alpha_0: 1.516_1,
        r_0: 2.690_6,
    },

    lambda_infty: 18.849,
    int_pot: InteractionPotential::FourParams {
        lambda_c: 18.145,
        lambda_one: 3.213,
        r_c: 2.595,
        r_lambda: 1.832,
    },
    alpha_c: 3.,

    build_grid_e: grid_e_f2,
    build_grid_m: grid_m_f2,
};

fn grid_m_o2() -> Grid<C64> {
    let mut grid = Grid::empty();

    grid.append_equidist(1.5, 2)
        .append_equidist(3.5, 35)
        .append_equidist(10., 10);

    grid
}

fn grid_e_o2() -> Grid<C64> {
    let mut grid = Grid::empty();

    grid.append_equidist(1.5, 6)
        .append_many([0.5, 1., 3.])
        .append_equidist(65., 13)
        .append_many([cplx_rf(5., RAD_40DEG), cplx_rf(15., RAD_40DEG)])
        .append_equidist(cplx_rf(80., RAD_40DEG), 4)
        .append_equidist(cplx_rf(100., RAD_40DEG), 2);

    grid
}

pub static MODEL_2D_O2: Model2D = Model2D {
    name: "O_2",
    mu: 14_682.6,
    l: 2, // d wave

    mol_pot: MolecularPotential::Oxygen {
        d_0: 0.231_78,
        alpha_0: 1.962_03,
        r_0: 2.299_89,
    },

    lambda_infty: 1.979_2,
    int_pot: InteractionPotential::SixParams {
        lambda_1: -38.940_1,
        alpha_1: 1.639_86,
        r_1: -0.448_295,

        lambda_2: 0.230_156,
        alpha_2: 1.480_6,
        r_2: 4.464_78,
    },

    alpha_c: 0.133,

    build_grid_e: grid_e_o2,
    build_grid_m: grid_m_o2,
};

#[derive(Clone)]
pub enum InteractionPotential {
    FourParams {
        lambda_c: f64,
        lambda_one: f64,
        r_c: f64,
        r_lambda: f64,
    },
    SixParams {
        lambda_1: f64,
        alpha_1: f64,
        r_1: f64,

        lambda_2: f64,
        alpha_2: f64,
        r_2: f64,
    },
}

impl InteractionPotential {
    /// # \lambda(R) - lambda_infty
    pub fn lambda(&self, r_m: C64, lambda_infty: f64) -> C64 {
        match &self {
            Self::FourParams {
                lambda_c,
                lambda_one,
                r_c,
                r_lambda,
            } => {
                // # \lambda_0
                let lambda_zero = C64::from(lambda_c - lambda_infty)
                    * (1. + (lambda_one * (r_c - r_lambda)).exp());

                lambda_infty + lambda_zero / (1. + (lambda_one * (r_m - r_lambda)).exp())
            }
            Self::SixParams {
                lambda_1,
                alpha_1,
                r_1,

                lambda_2,
                alpha_2,
                r_2,
            } => {
                lambda_infty
                    + lambda_1 / (1. + (alpha_1 * (r_m - r_1)).exp())
                    + lambda_2 / (1. + (alpha_2 * (r_m - r_2)).exp())
            }
        }
    }
}

#[derive(Clone)]
pub enum MolecularPotential {
    Morse { d_0: f64, alpha_0: f64, r_0: f64 },
    Oxygen { d_0: f64, alpha_0: f64, r_0: f64 },
}

impl MolecularPotential {
    pub fn v_0(&self, r_m: C64) -> C64 {
        match &self {
            MolecularPotential::Morse { d_0, alpha_0, r_0 } => {
                d_0 * ((-2. * alpha_0 * (r_m - r_0)).exp() - 2. * (-alpha_0 * (r_m - r_0)).exp())
            }

            MolecularPotential::Oxygen { d_0, alpha_0, r_0 } => {
                if r_m.re < 0.6 {
                    return C64::new(4.4, 0.);
                }

                let z1 = r_m - r_0;
                let z2 = r_m - 4.25451;
                let v_0 = -(d_0 / 2.)
                    * (-alpha_0 * z1).exp()
                    * (1. + z1 * (alpha_0 + z1 * (-0.197_253 - 0.453_587 * z1)))
                    * (1. - (0.589_844 * (z2 + (3.19454 * z2).exp() - 1.0)).re.tanh());

                let z1 = r_m.powi(2);
                let z2 = r_m - 2.63224;

                v_0 - (1.263_645 - 28.921_8 / z1) / z1.powi(2)
                    * 0.5
                    * (1. + (0.270_386 * (z2 + 1. - (-2.555_52 * z2).exp())).re.tanh())
            }
        }
    }
}

#[derive(Clone)]
pub struct Model2D {
    pub name: &'static str,

    /// Reduced mass of the diatomic
    pub mu: f64,
    /// Electron angular momentum
    pub l: usize,

    // Morse potential parameters
    pub mol_pot: MolecularPotential,

    // Interaction potential parameters
    pub lambda_infty: f64,
    pub int_pot: InteractionPotential,

    // Constant alpha
    pub alpha_c: f64,

    // Grid vertices
    pub build_grid_e: fn() -> Grid<C64>,
    pub build_grid_m: fn() -> Grid<C64>,
}

impl Model2D {
    // ANCHOR Potentials

    /// # V_0(R)
    pub fn potential_0(&self, r_m: C64) -> C64 {
        self.mol_pot.v_0(r_m)
    }

    /// # V_{int}(R, r)
    pub fn potential_int(&self, [r_m, r_e]: [C64; 2]) -> C64 {
        -self.lambda(r_m) * (-self.alpha_c * r_e.powi(2)).exp()
    }

    /// # V_{eff}(R, r)
    pub fn potential(&self, [r_m, r_e]: [C64; 2]) -> C64 {
        self.potential_int([r_m, r_e]) + self.potential_0(r_m) + self.angular_momentum_term(r_e)
    }

    /// # V_b(r)
    pub fn potential_b(&self, r_e: C64) -> C64 {
        -self.lambda_infty * (-self.alpha_c * r_e.powi(2)).exp()
    }

    pub fn angular_momentum_term(&self, r_e: C64) -> C64 {
        let l = self.l as f64;
        l * (l + 1.) / (2. * r_e.powi(2))
    }

    // ANCHOR Composite parameters
    pub fn lambda(&self, r_m: C64) -> C64 {
        self.int_pot.lambda(r_m, self.lambda_infty)
    }
    // ANCHOR Free particle states

    pub fn electron_wave(&self, k: f64, r_e: C64) -> C64 {
        use std::f64::consts::PI;

        fn bessel(order: usize, x: C64) -> C64 {
            if !x.im.is_zero() {
                return 0.0.into();
            }

            match order {
                0 => x.sin() / x,
                1 => x.sin() / x.powi(2) - x.cos() / x,
                2 => (3. / x.powi(2) - 1.) * x.sin() / x - 3. * x.cos() / x.powi(2),
                _ => todo!(),
            }
        }

        (2. * k / PI).sqrt() * r_e * bessel(self.l, r_e * k)
    }

    pub fn molecular_wave(&self, k_m: f64, r_m: C64) -> C64 {
        use std::f64::consts::PI;

        if !r_m.im.is_zero() {
            return 0.0.into();
        }

        (2. * self.mu / (PI * k_m)).sqrt() * (k_m * r_m).sin()
    }
}
