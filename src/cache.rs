use std::path::Path;

use rusqlite::{params, Connection};

type _Result<T> = Result<T, Box<dyn std::error::Error>>;

pub struct Cache(Connection);

impl From<Connection> for Cache {
    fn from(c: Connection) -> Self {
        Self(c)
    }
}

impl Cache {
    pub fn ensure_created(&self, table: impl AsRef<str>) -> _Result<()> {
        self.0.execute(
            &format!(
                "CREATE TABLE IF NOT EXISTS [{}] (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    note VARCHAR(255),
                    x REALS NOT NULL,
                    y REALS NOT NULL,
                    UNIQUE(x)
                )",
                table.as_ref()
            ),
            [],
        )?;

        Ok(())
    }

    pub fn ensure_multi_created(&self, table: impl AsRef<str>, cols: &[&str]) -> _Result<()> {
        self.0.execute(
            &format!(
                "CREATE TABLE IF NOT EXISTS [{}] (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    note VARCHAR(255),
                    x REALS NOT NULL,
                    {} UNIQUE(x)
                )",
                table.as_ref(),
                cols.iter()
                    .map(|s| format!("{} REALS,\n", s))
                    .collect::<String>()
            ),
            [],
        )?;

        Ok(())
    }

    pub fn insert_value(&self, table: &str, x: f64, y: f64, note: Option<&str>) -> _Result<()> {
        self.0.execute(
            &format!(
                "INSERT OR REPLACE INTO [{}] (x, y, note) VALUES (?1, ?2, ?3)",
                table
            ),
            params![x, y, note.unwrap_or("NULL")],
        )?;

        Ok(())
    }

    pub fn insert_multi_values(
        &self,
        table: &str,
        x: f64,
        ys: &[(&str, f64)],
        note: Option<&str>,
    ) -> _Result<()> {
        self.0.execute(
            &format!(
                "INSERT OR REPLACE INTO [{}] (x, {}note) VALUES (?1, {}?2)",
                table,
                ys.iter()
                    .map(|(key, _)| format!("{key}, "))
                    .collect::<String>(),
                ys.iter()
                    .map(|(_, val)| if val.is_nan() {
                        "NULL, ".into()
                    } else {
                        format!("{val}, ")
                    })
                    .collect::<String>()
            ),
            params![x, note.unwrap_or("NULL")],
        )?;

        Ok(())
    }

    pub fn get_values(
        &self,
        table: impl AsRef<str>,
        x_from: f64,
        x_to: f64,
    ) -> _Result<Vec<(f64, f64)>> {
        let mut stm = self.0.prepare(&format!(
            "SELECT x, y FROM [{}] WHERE x >= ?1 AND x <= ?2 ORDER BY x",
            table.as_ref()
        ))?;

        let val_iter = stm.query_map(params![x_from, x_to], |v| {
            Ok((v.get::<_, f64>(0)?, v.get::<_, f64>(1)?))
        })?;

        Ok(val_iter.flatten().collect())
    }

    pub fn get_multi_values<const COLS: usize>(
        &self,
        table: &str,
        x_from: f64,
        x_to: f64,
        keys: &[&str; COLS],
    ) -> _Result<Vec<[f64; COLS + 1]>> {
        let mut stm = self.0.prepare(&format!(
            "SELECT x, {} FROM [{}] WHERE x >= ?1 AND x <= ?2 ORDER BY x",
            keys.join(", "),
            table,
        ))?;

        let pattern = [(); COLS + 1];

        let val_iter = stm
            .query_map(params![x_from, x_to], |v| {
                let mut i = 0;
                Ok(pattern.map(|_| {
                    i += 1;
                    v.get::<_, Option<f64>>(i - 1)
                        .unwrap()
                        .unwrap_or(std::f64::NAN)
                }))
            })?
            .flatten();

        Ok(val_iter.collect())
    }
}

pub fn get_cache() -> _Result<Cache> {
    get_cache_from_path("cache.db")
}

pub fn get_cache_from_path(path: impl AsRef<Path>) -> _Result<Cache> {
    rusqlite::Connection::open(path)
        .map(|c| c.into())
        .map_err(|e| e.into())
}
