use simba::scalar::ComplexField;

pub fn bessel_fn<T: ComplexField + Copy>(order: usize) -> Box<dyn Fn(T) -> T> {
    match order {
        0 => Box::new(|x: T| x.sin()),
        1 => Box::new(|x: T| x.sin() / x - x.cos()),
        _ => todo!(),
    }
}
