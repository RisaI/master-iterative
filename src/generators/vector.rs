pub fn rand_f64_elem(_row: usize) -> f64 {
    (0.5 - rand::random::<f64>()) * 4.0
}
