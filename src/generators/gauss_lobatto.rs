use simba::scalar::ComplexField;

use crate::bindings::GaussQuadBind;

/// Gauss-Lobatto points and weights of order `n` for the interval `[-1, 1]`
pub fn points_and_weights(n: usize) -> (Vec<f64>, Vec<f64>) {
    let mut endpts = [-1., 1.];
    let mut b = vec![0.; n];
    let mut t = vec![0.; n];
    let mut w = vec![0.; n];

    unsafe {
        let n = n as i32;

        GaussQuadBind(
            &1,
            &n,
            &0.,
            &0.,
            &2,
            endpts.as_mut_ptr(),
            b.as_mut_ptr(),
            t.as_mut_ptr(),
            w.as_mut_ptr(),
        );
    }

    (t, w)
}

/// Gauss-Lobatto points and weights for a given interval
/// Correctly places the endpoints and scales the weights by `interval length / 2`
/// ## Safety
/// Panics if `n < 2`
pub fn scaled_points_and_weights<T: ComplexField + Copy>(
    from: T,
    to: T,
    n: usize,
) -> (Vec<T>, Vec<T>) {
    use std::iter::once;

    let (t, w) = points_and_weights(n);

    let half_len = (to - from) / T::from_subset(&2.);

    (
        // Collect points, replace first and last with arguments
        once(from)
            .chain(
                t.into_iter()
                    .skip(1)
                    .take(n - 2)
                    .map(|x| from + half_len * T::from_subset(&(1. + x))),
            )
            .chain(once(to))
            .collect(),
        // Collect scaled weights
        w.into_iter()
            .map(|w| T::from_subset(&w) * half_len)
            .collect(),
    )
}
