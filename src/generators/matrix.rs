use crate::primitives::C64;

pub fn random_strongly_diagonal_elem(row: usize, col: usize) -> f64 {
    let dist = row as f64 - col as f64;

    (-dist.powi(2)).exp() * (3.0 + 0.5 * rand::random::<f64>())
}

pub fn sym_strongly_diagonal_elem(row: usize, col: usize) -> C64 {
    let dist = row as f64 - col as f64;

    (-dist.powi(2)).exp().into()
}
