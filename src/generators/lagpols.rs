#[macro_export]
macro_rules! matidx {
    ($width:expr; $i:expr, $j:expr) => {
        $i * $width + $j
    };
}

pub fn derivatives(n: usize) -> Vec<f64> {
    let mut res = vec![0.; n * n];

    let glx = super::gauss_lobatto::points_and_weights(n).0;

    for i in 0..n {
        res[matidx!(n; i, i)] = (0..n)
            .filter_map(|s| {
                if s == i {
                    None
                } else {
                    Some(1. / (glx[i] - glx[s]))
                }
            })
            .sum();

        for k in (i + 1)..n {
            let hlp: f64 = (0..n)
                .filter_map(|j| {
                    if i == j || k == j {
                        None
                    } else {
                        Some((glx[k] - glx[j]) / (glx[i] - glx[j]))
                    }
                })
                .product();

            let hlp_inv: f64 = (0..n)
                .filter_map(|j| {
                    if i == j || k == j {
                        None
                    } else {
                        Some((glx[i] - glx[j]) / (glx[k] - glx[j]))
                    }
                })
                .product();

            res[matidx!(n; i, k)] = hlp / (glx[i] - glx[k]);
            res[matidx!(n; k, i)] = hlp_inv / (glx[k] - glx[i]);
        }
    }

    res
}
