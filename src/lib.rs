#![allow(incomplete_features)]
#![feature(trait_alias)]
#![feature(generic_const_exprs)]
#![feature(const_fn_floating_point_arithmetic)]
#![allow(clippy::needless_borrow)]
extern crate lapack;
extern crate lapack_src;
pub mod bindings;
pub mod cache;
mod dss;
pub mod generators;
pub mod hb;
pub mod models;
pub mod primitives;
pub mod problems;
pub mod solvers;
pub(crate) use dss as intel_dss;
