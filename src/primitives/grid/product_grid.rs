use std::{
    borrow::Borrow,
    iter::{Product, Sum},
    marker::PhantomData,
    ops::{Mul, Range},
};

use super::{MultiIndexIterator, QuadratureGrid};
use simba::scalar::ComplexField;

pub struct ProductGrid<X, W, B: Borrow<QuadratureGrid<X, W>>, const DIM: usize> {
    grids: [B; DIM],
    x_phantom: PhantomData<X>,
    w_phantom: PhantomData<W>,
}

impl<X, W, B: Borrow<QuadratureGrid<X, W>>, const DIM: usize> ProductGrid<X, W, B, DIM> {
    pub fn new(grids: [B; DIM]) -> Self {
        Self {
            grids,
            x_phantom: PhantomData::default(),
            w_phantom: PhantomData::default(),
        }
    }

    pub fn get_point(&self, idx: [usize; DIM]) -> [X; DIM]
    where
        X: Clone,
    {
        let mut i = 0;
        idx.map(|idx| {
            i += 1;
            self.grids[i - 1].borrow()[idx].clone()
        })
    }

    pub fn get_weight(&self, idx: [usize; DIM]) -> W
    where
        W: ComplexField + Product + Clone,
    {
        idx.into_iter()
            .enumerate()
            .map(|(grid_idx, point_idx)| {
                self.grids[grid_idx].borrow().get_weight(point_idx).clone()
            })
            .product()
    }

    pub fn get_index_iterator(&self) -> MultiIndexIterator<DIM> {
        MultiIndexIterator::new(
            self.grids
                .iter()
                .map(|g| 0..g.borrow().len())
                .collect::<Vec<Range<usize>>>()
                .try_into()
                .unwrap(),
        )
    }

    pub fn represent_fn_dvr<F, Y>(&self, f: F, borderless: bool) -> Vec<Y>
    where
        X: Clone,
        F: Fn([X; DIM]) -> Y,
    {
        let shrink = if borderless { 1 } else { 0 };
        let f = f.borrow();

        self.get_index_iterator()
            .shrink_all(shrink, shrink)
            .map(|idx| f(self.get_point(idx)))
            .collect()
    }

    pub fn represent_fn<F>(&self, f: F, borderless: bool) -> Vec<W>
    where
        X: Clone,
        W: ComplexField + Clone + Product,
        F: Fn([X; DIM]) -> W,
    {
        let shrink = if borderless { 1 } else { 0 };

        self.get_index_iterator()
            .shrink_all(shrink, shrink)
            .map(|idx| f(self.get_point(idx)) * self.get_weight(idx).sqrt())
            .collect()
    }

    pub fn integrate_fn<F>(&self, f: F) -> W
    where
        X: Clone,
        W: ComplexField + Clone + Sum + Product,
        F: Fn([X; DIM]) -> W,
    {
        self.get_index_iterator()
            .map(|idx| f(self.get_point(idx)) * self.get_weight(idx))
            .sum()
    }
}

impl<T, B: Borrow<QuadratureGrid<T, T>>, const DIM: usize> ProductGrid<T, T, B, DIM> {
    pub fn stiffness_matrix<P>(
        &self,
        prefactors: [P; DIM],
        borderless: bool,
    ) -> nalgebra_sparse::CooMatrix<T>
    where
        P: Into<T> + Copy,
        T: ComplexField + Sum + Copy,
    {
        let substiffness: Vec<nalgebra_sparse::CsrMatrix<T>> = self
            .grids
            .iter()
            .zip(prefactors.iter())
            .map(|(g, &p)| (&g.borrow().stiffness_matrix_whole(p.into())).into())
            .collect();

        let shrink = if borderless { 1 } else { 0 };
        let n = self.get_index_iterator().shrink_all(shrink, shrink).len();

        let mut result = nalgebra_sparse::CooMatrix::new(n, n);

        for (i, x_i) in self
            .get_index_iterator()
            .shrink_all(shrink, shrink)
            .enumerate()
        {
            for (j, x_j) in self
                .get_index_iterator()
                .shrink_all(shrink, shrink)
                .enumerate()
            {
                if j < i {
                    continue;
                }

                let elem: T = (0..substiffness.len())
                    .filter_map(|k| {
                        let non_null = (0..substiffness.len()).all(|l| l == k || x_i[l] == x_j[l]);

                        match non_null {
                            true => match substiffness[k].get_entry(x_i[k], x_j[k]).unwrap() {
                                nalgebra_sparse::SparseEntry::NonZero(v) => Some(*v),
                                nalgebra_sparse::SparseEntry::Zero => None,
                            },
                            false => None,
                        }
                    })
                    .sum();

                if !elem.is_zero() {
                    result.push(i, j, elem);
                    if j != i {
                        result.push(j, i, elem);
                    }
                }
            }
        }

        result
    }
}

impl<T, B: Borrow<QuadratureGrid<T, T>>> ProductGrid<T, T, B, 2> {
    pub fn stiffness_matrix_fast<P>(
        &self,
        prefactors: [P; 2],
        borderless: bool,
    ) -> nalgebra_sparse::CooMatrix<T>
    where
        P: Into<T> + Copy,
        T: ComplexField + Sum + Copy,
    {
        let [sub_a, sub_b] = {
            let mut substiffness: Vec<nalgebra::DMatrix<T>> = self
                .grids
                .iter()
                .zip(prefactors.iter())
                .map(|(g, &p)| (&g.borrow().stiffness_matrix_whole(p.into())).into())
                .collect();

            if borderless {
                substiffness = substiffness
                    .into_iter()
                    .map(|mut s| {
                        let n = s.ncols();

                        s = s.remove_columns_at(&[0, n - 1]);
                        s = s.remove_rows_at(&[0, n - 1]);

                        s
                    })
                    .collect();
            }

            let b = substiffness.pop().unwrap();
            let a = substiffness.pop().unwrap();

            [a, b]
        };

        let shrink = if borderless { 1 } else { 0 };
        let n = self.get_index_iterator().shrink_all(shrink, shrink).len();

        let mut result = nalgebra_sparse::CooMatrix::new(n, n);

        for i in 0..sub_a.nrows() {
            let mut had_nz = false;
            for j in i..sub_a.ncols() {
                let elem = sub_a[(i, j)];

                if had_nz && elem.is_zero() {
                    break;
                } else if !elem.is_zero() {
                    had_nz = true;
                } else {
                    continue;
                }

                for k in 0..sub_b.nrows() {
                    let off = k * sub_a.ncols();

                    if i == j {
                        result.push(off + i, off + i, elem + sub_b[(k, k)]);
                    } else {
                        result.push(off + i, off + j, elem);
                        result.push(off + j, off + i, elem);
                    }
                }
            }
        }

        for i in 0..sub_b.nrows() {
            let mut had_nz = false;
            for j in (i + 1)..sub_b.ncols() {
                let elem = sub_b[(i, j)];

                if had_nz && elem.is_zero() {
                    break;
                } else if !elem.is_zero() {
                    had_nz = true;
                } else {
                    continue;
                }

                for k in 0..sub_a.nrows() {
                    let a = i * sub_a.nrows();
                    let b = j * sub_a.ncols();
                    result.push(a + k, b + k, elem);
                    result.push(b + k, a + k, elem);
                }
            }
        }

        result
    }
}

impl<X, W, B: Borrow<QuadratureGrid<X, W>>, const DIM0: usize, const DIM1: usize>
    Mul<ProductGrid<X, W, B, DIM1>> for ProductGrid<X, W, B, DIM0>
where
    ProductGrid<X, W, B, { DIM0 + DIM1 }>: Sized,
{
    type Output = ProductGrid<X, W, B, { DIM0 + DIM1 }>;

    fn mul(self, rhs: ProductGrid<X, W, B, DIM1>) -> Self::Output {
        ProductGrid::new(
            if let Ok(grids) = self
                .grids
                .into_iter()
                .chain(rhs.grids.into_iter())
                .collect::<Vec<B>>()
                .try_into()
            {
                grids
            } else {
                panic!("Failed to multiply grids: couldn't collect chained iterators.")
            },
        )
    }
}

impl<X, W, B: Borrow<QuadratureGrid<X, W>>, const DIM: usize> Mul<B> for ProductGrid<X, W, B, DIM>
where
    ProductGrid<X, W, B, { DIM + 1 }>: Sized,
{
    type Output = ProductGrid<X, W, B, { DIM + 1 }>;

    fn mul(self, rhs: B) -> Self::Output {
        self * ProductGrid::new([rhs])
    }
}
