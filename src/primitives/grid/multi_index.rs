#![allow(clippy::len_without_is_empty)]
use std::ops::Range;

pub type MultiIndex<const DIM: usize> = [usize; DIM];

#[derive(Clone)]
pub struct MultiIndexIterator<const DIM: usize> {
    ranges: [Range<usize>; DIM],
    current: Option<MultiIndex<DIM>>,
}

impl<const DIM: usize> MultiIndexIterator<DIM> {
    pub fn new(ranges: [Range<usize>; DIM]) -> Self {
        Self {
            current: Some(ranges.clone().map(|r| r.start)),
            ranges,
        }
    }

    pub fn len(&self) -> usize {
        self.ranges.iter().map(|r| r.end - r.start).product()
    }

    pub fn shrink(self, len: [(usize, usize); DIM]) -> Self {
        let mut next_ranges = self.ranges;

        for i in 0..DIM {
            next_ranges[i].start += len[i].0;
            next_ranges[i].end -= len[i].1;
        }

        Self::new(next_ranges)
    }

    pub fn shrink_sym(self, len: [usize; DIM]) -> Self {
        self.shrink(len.map(|l| (l, l)))
    }

    pub fn shrink_all(self, at_start: usize, at_end: usize) -> Self {
        self.shrink([(at_start, at_end); DIM])
    }
}

impl<const DIM: usize> Iterator for MultiIndexIterator<DIM> {
    type Item = MultiIndex<DIM>;

    fn next(&mut self) -> Option<Self::Item> {
        let current = self.current;

        if let Some(current) = self.current.as_mut() {
            if current[0] < self.ranges[0].end - 1 {
                current[0] += 1;
            } else {
                let mut incr_idx = None;

                for (i, &cur) in current.iter().enumerate().take(DIM).skip(1) {
                    if cur < self.ranges[i].end - 1 {
                        incr_idx = Some(i);
                        break;
                    }
                }

                if let Some(incr_idx) = incr_idx {
                    current[incr_idx] += 1;

                    for (i, item) in current.iter_mut().enumerate().take(incr_idx) {
                        *item = self.ranges[i].start;
                    }
                } else {
                    self.current = None;
                }
            }
        }

        current
    }
}
