#![allow(clippy::len_without_is_empty)]
use simba::scalar::ComplexField;

use crate::generators::gauss_lobatto;

use super::ProductGrid;

#[derive(Clone)]
pub struct Grid<X> {
    verts: Vec<X>,
}

impl<X> Default for Grid<X> {
    fn default() -> Self {
        Self { verts: vec![] }
    }
}

impl<X> Grid<X> {
    pub fn empty() -> Self {
        Default::default()
    }

    pub fn is_empty(&self) -> bool {
        self.verts.is_empty()
    }

    pub fn add_vert_unchecked(&mut self, point: impl Into<X>) -> &mut Self {
        self.verts.push(point.into());

        self
    }

    pub fn subgrid(&self, filter: impl Fn(&&X) -> bool) -> Self
    where
        X: Clone,
    {
        Self {
            verts: self.verts.iter().filter(filter).cloned().collect(),
        }
    }
}

impl<X> Grid<X>
where
    X: ComplexField + Copy,
{
    pub fn append_equidist(&mut self, len: impl Into<X>, segments: usize) -> &mut Self {
        if self.verts.is_empty() {
            self.verts.push(X::zero())
        }

        let start = *self.verts.last().unwrap();
        let div = len.into() / X::from_subset(&(segments as f64));

        for i in 1..=segments {
            self.verts.push(start + X::from_subset(&(i as f64)) * div);
        }

        self
    }

    pub fn append_many<T: Into<X>>(&mut self, dists: impl IntoIterator<Item = T>) -> &mut Self {
        if self.verts.is_empty() {
            self.verts.push(X::zero())
        }

        let mut next = *self.verts.last().unwrap();

        for d in dists {
            next += d.into();
            self.verts.push(next);
        }

        self
    }

    pub fn prepend_equidist(&mut self, len: impl Into<X>, segments: usize) -> &mut Self {
        if self.verts.is_empty() {
            self.verts.push(X::zero())
        }

        let len = len.into();
        let start = *self.verts.first().unwrap() - len;
        let div = len / X::from_subset(&(segments as f64));

        for i in 0..segments {
            self.verts.push(start + X::from_subset(&(i as f64)) * div);
        }

        self
    }

    pub fn into_quadrature_grid(self, quad: Quadrature) -> QuadratureGrid<X, X> {
        assert!(self.verts.len() > 1);

        let (points, weights) = match &quad {
            Quadrature::Simple => {
                let points = self.verts.clone();
                let weight = (*self.verts.last().unwrap() - *self.verts.first().unwrap())
                    / X::from_subset(&(points.len() as f64));

                (
                    points.into_boxed_slice(),
                    (0..self.verts.len()).map(|_| weight).collect(),
                )
            }
            Quadrature::GaussLobatto(n) => {
                let mut points: Vec<X> = vec![];
                let mut weights: Vec<X> = vec![];

                for window in self.verts.windows(2) {
                    let (glx, mut glw) =
                        gauss_lobatto::scaled_points_and_weights(window[0], window[1], *n);
                    points.pop();
                    glw[0] += weights.pop().unwrap_or_else(|| X::zero());

                    points.extend(glx);
                    weights.extend(glw);
                }

                (points.into_boxed_slice(), weights.into_boxed_slice())
            }
        };

        unsafe {
            QuadratureGrid::from_raw_parts(quad, self.verts.into_boxed_slice(), points, weights)
        }
    }
}

impl<T> Grid<T>
where
    T: ComplexField + PartialOrd,
{
    pub fn add_vert(&mut self, point: T) -> &mut Self {
        match self
            .verts
            .binary_search_by(|a| a.partial_cmp(&point).unwrap())
        {
            Ok(_) => {}
            Err(idx) => {
                self.verts.insert(idx, point);
            }
        }

        self
    }
}

#[derive(Clone)]
pub struct QuadratureGrid<X, W> {
    quadrature: Quadrature,
    original_points: Box<[X]>,
    points: Box<[X]>,
    weights: Box<[W]>,
}

impl<X, W> QuadratureGrid<X, W> {
    /// # Safety
    /// Panics if points.len() neq weights.len()
    /// Panics if empty
    /// Does not check the validity of original points and quadrature
    pub unsafe fn from_raw_parts(
        quadrature: Quadrature,
        original_points: Box<[X]>,
        points: Box<[X]>,
        weights: Box<[W]>,
    ) -> Self {
        assert_eq!(points.len(), weights.len());
        assert!(points.len() > 1);

        Self {
            quadrature,
            original_points,
            points,
            weights,
        }
    }

    pub fn len(&self) -> usize {
        self.points.len()
    }

    pub fn get_quadrature(&self) -> &Quadrature {
        &self.quadrature
    }

    pub fn get_point(&self, idx: usize) -> &X {
        &self.points[idx]
    }

    pub fn get_weight(&self, idx: usize) -> &W {
        &self.weights[idx]
    }

    pub fn iter_points(&self) -> impl DoubleEndedIterator<Item = &X> {
        self.points.iter()
    }

    pub fn iter_weights(&self) -> impl Iterator<Item = &W> {
        self.weights.iter()
    }

    pub fn iter_points_weights(&self) -> impl Iterator<Item = (&X, &W)> {
        self.points.iter().zip(self.weights.iter())
    }

    pub fn represent_fn_dvr<F, Y>(&self, f: F) -> Vec<Y>
    where
        F: Fn(X) -> Y,
        X: Clone,
    {
        self.points.iter().map(|x| f(x.clone())).collect()
    }
}

impl<T> QuadratureGrid<T, T>
where
    T: ComplexField + Clone,
{
    pub fn represent_fn<F: Fn(T) -> T>(&self, f: F) -> Vec<T> {
        self.points
            .iter()
            .zip(self.weights.iter())
            .map(|(x, w)| f(x.clone()) * w.clone().sqrt())
            .collect()
    }

    pub fn integrate_fn<F: Fn(T) -> T>(&self, f: F) -> T {
        let mut acc = T::zero();
        for (x, w) in self.points.iter().zip(self.weights.iter()) {
            acc += f(x.clone()) * w.clone();
        }

        acc
    }
}

impl<T> QuadratureGrid<T, T>
where
    T: ComplexField + Copy,
{
    pub fn stiffness_matrix_whole(&self, prefactor: T) -> nalgebra_sparse::CooMatrix<T> {
        macro_rules! matidx {
            ($width:expr; $i:expr, $j:expr) => {
                $i * $width + $j
            };
        }

        let gl_n = match self.quadrature {
            Quadrature::GaussLobatto(n) => n,
            _ => todo!(),
        };

        let n = self.points.len();
        // let mut result = FullMatrix::<T>::new_zero(n, n);
        let mut result = nalgebra_sparse::CooMatrix::new(n, n);

        let unscaled_submatrix = {
            // Lagpols for given Gauss-Lobatto order (all elements have the same order)
            let dlagpol = crate::generators::lagpols::derivatives(gl_n);

            // Gauss-Lobatto weights (at -1:1) for given order gl_n
            let gauss_lobatto = gauss_lobatto::points_and_weights(gl_n).1;

            let mut data = Vec::with_capacity(gl_n * gl_n);

            for i in 0..gl_n {
                for j in 0..gl_n {
                    let mut val = 0.;

                    (0..gl_n).for_each(|s| {
                        val += dlagpol[matidx!(gl_n; i, s)]
                            * dlagpol[matidx!(gl_n; j, s)]
                            * gauss_lobatto[s]
                    });

                    data.push(T::from_subset(&val).neg());
                }
            }

            nalgebra::DMatrix::from_vec_storage(nalgebra::VecStorage::new(
                nalgebra::Dynamic::new(gl_n),
                nalgebra::Dynamic::new(gl_n),
                data,
            ))
        };

        for (elem_idx, elem) in self.original_points.windows(2).enumerate() {
            let elem_pos = elem_idx * (gl_n - 1);

            let len_factor = T::from_subset(&2.) / (elem[1] - elem[0]);

            // TODO: take scaling out of the weights
            // let block = unscaled_submatrix * prefactor * len_factor
            //     / (self.weights[elem_pos + i] * self.weights[elem_pos + j]).sqrt();

            // result.push_matrix(elem_pos, elem_pos, m);

            for i in 0..gl_n {
                for j in 0..gl_n {
                    let val = unscaled_submatrix[(i, j)] * prefactor * len_factor
                        / (self.weights[elem_pos + i] * self.weights[elem_pos + j]).sqrt();

                    result.push(elem_pos + i, elem_pos + j, val);
                    // if j != i {
                    //     result.push(elem_pos + j, elem_pos + i, val);
                    // }
                }
            }
        }

        result
    }
}

impl<X, W> QuadratureGrid<X, W> {
    pub fn grid_product(self, rhs: QuadratureGrid<X, W>) -> ProductGrid<X, W, Self, 2> {
        ProductGrid::new([self, rhs])
    }
}

impl<X, Y> std::ops::Index<usize> for QuadratureGrid<X, Y> {
    type Output = X;

    fn index(&self, index: usize) -> &Self::Output {
        &self.points[index]
    }
}

#[derive(Clone, Debug)]
pub enum Quadrature {
    Simple,
    GaussLobatto(usize),
}
