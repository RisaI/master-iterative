use num_traits::real::Real;

pub type C32 = Complex<f32>;
pub type C64 = Complex<f64>;

pub use num_complex::Complex;

pub trait Sqrt {
    fn sqrt(self) -> Self;
}

impl<T: Real> Sqrt for T {
    fn sqrt(self) -> Self {
        Real::sqrt(self)
    }
}
