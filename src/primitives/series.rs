pub mod arithmetic {
    pub fn from_start_sum_n(a_0: f64, sum_n: f64, n: usize) -> impl Iterator<Item = f64> {
        let d = {
            let n = n as f64;
            2. * (sum_n / (n + 1.) - a_0) / n.max(1.)
        };

        (0..=n).map(move |i| a_0 + i as f64 * d)
    }

    pub fn from_start_end_n(a_0: f64, a_n: f64, n: usize) -> impl Iterator<Item = f64> {
        let d = (a_n - a_0) / n as f64;

        (0..=n).map(move |i| a_0 + i as f64 * d)
    }
}
