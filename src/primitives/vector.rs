use nalgebra::Scalar;
use num_traits::Zero;
use simba::scalar::ComplexField;
use std::ops::{Add, AddAssign, Index, IndexMut, Sub};

use super::{Bounds, ScalarField};

// use rayon::prelude::*;

pub trait Vector:
    Sized
    + Bounds<1>
    + Index<usize, Output = Self::Element>
    + IndexMut<usize, Output = Self::Element>
    + Add<Output = Self>
    + Sub<Output = Self>
    + AddAssign<Self>
{
    type Element: ScalarField;

    fn zero(rows: usize) -> Self;
    fn rows(&self) -> usize {
        self.bound(0)
    }

    fn scale(&mut self, factor: Self::Element);
    fn add_assign_ref<V: Vector<Element = Self::Element>>(&mut self, rhs: &V);
    fn add_assign_ref_scaled<V: Vector<Element = Self::Element>>(
        &mut self,
        rhs: &V,
        scale: Self::Element,
    );

    fn len_squared(&self) -> Self::Element {
        let mut acc = Self::Element::zero();

        for i in 0..self.rows() {
            let elem = self[i];
            acc += elem * elem;
        }

        acc
    }

    fn dist_squared<V: Vector<Element = Self::Element>>(&self, b: &V) -> Self::Element {
        debug_assert_eq!(self.rows(), b.rows());

        let mut acc = Self::Element::zero();

        for i in 0..self.rows() {
            let elem = self[i] - b[i];

            acc += elem * elem;
        }

        acc
    }

    fn dot_prod<V: Vector<Element = Self::Element>>(&self, b: &V) -> Self::Element {
        debug_assert_eq!(self.rows(), b.rows());

        let mut acc = Self::Element::zero();

        for i in 0..self.rows() {
            acc += self[i] * b[i];
        }

        acc
    }

    fn cplx_dot_prod<V: Vector<Element = Self::Element>>(&self, b: &V) -> Self::Element
    where
        Self::Element: ComplexField,
    {
        debug_assert_eq!(self.rows(), b.rows());

        let mut acc = Self::Element::zero();

        for i in 0..self.rows() {
            acc += (self[i]).conjugate() * b[i];
        }

        acc
    }

    fn cplx_len_squared(&self) -> Self::Element
    where
        Self::Element: ComplexField,
    {
        let mut acc = Self::Element::zero();

        for i in 0..self.rows() {
            acc += Self::Element::from_real(self[i].modulus_squared());
        }

        acc
    }
}

pub type FullVector<T> = nalgebra::DVector<T>;

pub fn shrink_vec<T: Scalar>(vec: FullVector<T>, left: usize, right: usize) -> FullVector<T> {
    assert!(left + right < vec.nrows());

    let len = vec.nrows() - left - right;

    nalgebra::DVector::from_iterator(len, vec.iter().skip(left).take(len).cloned())
}

impl<T> Bounds<1> for FullVector<T> {
    fn bound(&self, pos: usize) -> usize {
        debug_assert!(pos == 0);

        self.data.len()
    }
}

impl<T> Vector for FullVector<T>
where
    T: ScalarField + Copy,
{
    type Element = T;

    fn zero(rows: usize) -> Self {
        (0..rows).map(|_| T::zero()).collect::<Vec<_>>().into()
    }

    fn rows(&self) -> usize {
        self.data.len()
    }

    fn scale(&mut self, factor: Self::Element) {
        for elem in self.iter_mut() {
            *elem *= factor;
        }
    }

    fn add_assign_ref<V: Vector<Element = Self::Element>>(&mut self, rhs: &V) {
        assert_eq!(self.nrows(), rhs.rows());

        for i in 0..self.nrows() {
            self[i] += rhs[i];
        }
    }

    fn add_assign_ref_scaled<V: Vector<Element = Self::Element>>(
        &mut self,
        rhs: &V,
        scale: Self::Element,
    ) {
        assert_eq!(self.nrows(), rhs.rows());

        for i in 0..self.nrows() {
            self[i] += rhs[i] * scale;
        }
    }
}
