#![allow(clippy::uninit_vec)]
use std::{borrow::Borrow, ops::Mul};

use nalgebra::Scalar;
use simba::scalar::ComplexField;

use crate::primitives::Indexed;

use crate::primitives::{
    DiagonalMatrix, FullVector, IntoMatrix, Matrix, ScalarField, TriangularMatrix, Vector,
};

pub fn symmetric_incomplete_cholesky<T, M>(mat: &M) -> LDLPrecondMatrix<M, &M>
where
    T: ComplexField + Scalar + Copy,
    M: Matrix<Element = T>,
{
    assert_eq!(mat.rows(), mat.cols());

    let mut l: TriangularMatrix<T, true> = mat.into_mat();
    let mut d: DiagonalMatrix<T> = mat.into_mat();
    let n = mat.rows();

    for j in 0..n {
        for k in 0..j {
            let ljk = l[(j, k)];
            if ljk.is_zero() {
                continue;
            }

            for i in (j + 1)..n {
                let lik = l[(i, k)];
                if lik.is_zero() {
                    continue;
                }

                l.set([i, j], l[(i, j)] - lik * *d.get([k, k]) * ljk)
            }
        }

        for i in (j + 1)..n {
            let l_ij = l[(i, j)];
            if l_ij.is_zero() {
                continue;
            }

            l.set([i, j], l_ij / *d.get([j, j]));

            let l_ij = l[(i, j)];
            d.set([i, i], *d.get([i, i]) - *d.get([j, j]) * l_ij * l_ij);
        }
    }

    LDLPrecondMatrix::new(mat, d, l)
}

pub struct LDLPrecondMatrix<M: Matrix, B: Borrow<M>> {
    pub a: B,
    pub d: DiagonalMatrix<M::Element>, // |D|^{1/2}
    pub l: TriangularMatrix<M::Element, true>,
}

impl<T, M, B> LDLPrecondMatrix<M, B>
where
    T: ComplexField + Copy + Sync + Send + 'static,
    M: Matrix<Element = T>,
    B: Borrow<M>,
{
    pub fn new(
        a: B,
        mut d: DiagonalMatrix<M::Element>,
        l: TriangularMatrix<M::Element, true>,
    ) -> Self {
        for i in 0..d.rows() {
            d.set([i, i], (*d.get([i, i])).sqrt());
        }

        Self { a, d, l }
    }

    pub fn apply_to_rhs<V: Vector<Element = T>>(&self, vec: &V) -> FullVector<T> {
        let mut result = self.l.back_substitute(vec);

        for i in 0..self.d.rows() {
            result[i] /= *self.d.get([i, i]);
        }

        result
    }
}

impl<'a, 'b, T, M, B, V> Mul<&'b V> for &'a LDLPrecondMatrix<M, B>
where
    T: ComplexField + Copy + 'static,
    M: Matrix<Element = T>,
    B: Borrow<M>,
    V: Vector<Element = T> + 'static,
{
    type Output = FullVector<T>;

    fn mul(self, vec: &'b V) -> Self::Output {
        let mut elems = Vec::<T>::with_capacity(self.d.rows());

        for i in 0..self.d.rows() {
            elems.push(vec[i] / *self.d.get([i, i]));
        }

        let mut elems = elems.into();

        // Invert LT
        {
            let lt = self.l.transpose_ref();
            elems = lt.back_substitute(&elems);
        }

        self.a
            .borrow()
            .mul_vec_to(&elems.clone(), elems.as_mut_slice());

        // Invert L
        elems = self.l.back_substitute(&elems);

        for i in 0..self.d.rows() {
            elems[i] /= *self.d.get([i, i]);
        }

        elems
    }
}

// ANCHOR Sparse
// pub fn sparse_ic0<T>(mat: &SparseMatrix<T>) -> SparseLDLPrecondMatrix<'_, T>
// where
//     T: ComplexField + ScalarField,
// {
//     assert_eq!(mat.rows(), mat.cols());
//     let n = mat.rows();

//     let mut l = mat.0.lower_triangle();
//     let mut d: DiagonalMatrix<T> = mat.into_mat();

//     for j in 0..n {
//         for k in 0..j {
//             let ljk = match l.get_entry(j, k).unwrap() {
//                 SparseEntry::NonZero(v) => *v,
//                 SparseEntry::Zero => continue,
//             };

//             for i in (j + 1)..n {
//                 let lik = match l.get_entry(i, k).unwrap() {
//                     SparseEntry::NonZero(v) => *v,
//                     SparseEntry::Zero => continue,
//                 };

//                 if let SparseEntryMut::NonZero(val) = l.get_entry_mut(i, j).unwrap() {
//                     *val -= lik * ljk * *d.get([k, k]);
//                 }
//             }
//         }

//         for i in (j + 1)..n {
//             let l_ij = match l.get_entry_mut(i, j).unwrap() {
//                 SparseEntryMut::NonZero(v) => v,
//                 SparseEntryMut::Zero => continue,
//             };

//             *l_ij /= *d.get([j, j]);

//             d.set([i, i], *d.get([i, i]) - *d.get([j, j]) * *l_ij * *l_ij);
//         }
//     }

//     SparseLDLPrecondMatrix::new(mat, d, SparseMatrix(l))
// }

// pub struct SparseLDLPrecondMatrix<'a, T> {
//     pub a: &'a SparseMatrix<T>,
//     pub d: DiagonalMatrix<T>, // |D|^{1/2}
//     pub l: SparseMatrix<T>,
// }

// impl<'a, T> SparseLDLPrecondMatrix<'a, T>
// where
//     T: ComplexField + ScalarField,
// {
//     pub fn new(a: &'a SparseMatrix<T>, mut d: DiagonalMatrix<T>, l: SparseMatrix<T>) -> Self {
//         for i in 0..d.rows() {
//             d.set([i, i], (*d.get([i, i])).sqrt());
//         }

//         Self { a, d, l }
//     }

//     pub fn apply_to_rhs<V: Vector<Element = T>>(&self, vec: &V) -> FullVector<T> {
//         let mut result = self.l.back_substitute(vec);

//         for i in 0..self.d.rows() {
//             result[i] /= *self.d.get([i, i]);
//         }

//         result
//     }
// }

// impl<'a, 'b, T, V> Mul<&'b V> for &'a SparseLDLPrecondMatrix<'a, T>
// where
//     T: ComplexField + ScalarField,
//     V: Vector<Element = T> + 'static,
// {
//     type Output = FullVector<T>;

//     fn mul(self, vec: &'b V) -> Self::Output {
//         let mut elems = Vec::<T>::with_capacity(self.d.rows());

//         for i in 0..self.d.rows() {
//             elems.push(vec[i] / *self.d.get([i, i]));
//         }

//         let mut elems = elems.into();

//         // Invert LT
//         {
//             let lt = self.l.transpose_ref();
//             elems = lt.back_substitute(&elems);
//         }

//         self.a
//             .borrow()
//             .mul_vec_to(&elems.clone(), elems.as_mut_slice());

//         // Invert L
//         elems = self.l.back_substitute(&elems);

//         for i in 0..self.d.rows() {
//             elems[i] /= *self.d.get([i, i]);
//         }

//         elems
//     }
// }
