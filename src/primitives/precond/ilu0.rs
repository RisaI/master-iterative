use crate::primitives::{FullVector, Matrix, ScalarField, SparseMatrix, Vector};
use nalgebra_sparse::CsrMatrix;
use std::{cmp::Ordering, iter::Sum, ops::Mul};

/// ## Source
/// Yoinked from  [ilu++](https://github.com/c-f-h/ilupp/blob/master/src/ilupp/ILU0.hpp)
pub fn ilu0<T: ScalarField>(source: &SparseMatrix<T>) -> ILU0Preconditioner<T> {
    let mut lu_data = Vec::with_capacity(source.0.nnz());

    // Calculate ILU
    compute_ilu0(source, &mut lu_data);

    let (row_offsets, col_indices, _) = source.0.csr_data();
    let lu_data = CsrMatrix::try_from_csr_data(
        source.rows(),
        source.cols(),
        row_offsets.to_vec(),
        col_indices.to_vec(),
        lu_data,
    )
    .unwrap();

    let mut l = lu_data.lower_triangle();
    let u = lu_data.upper_triangle();

    // Set the diagonal of L to one
    l.triplet_iter_mut().for_each(|(i, j, v)| {
        if i == j {
            *v = T::one();
        }
    });

    ILU0Preconditioner {
        l: SparseMatrix(l),
        u: SparseMatrix(u),
    }
}

fn compute_ilu0<T: ScalarField>(source: &SparseMatrix<T>, output: &mut Vec<T>) -> (usize, usize) {
    let n = source.cols();

    let mut diag = Vec::with_capacity(n);
    let (mut nnz_l, mut nnz_u) = (0, 0);

    for i in 0..n {
        let row = source.0.row(i);
        let mut diag_idx = 0;

        for (&j, v) in row.col_indices().iter().zip(row.values().iter()) {
            if j <= i {
                // Count non-zero lower triangular elems
                nnz_l += 1;
            }
            if j >= i {
                // Count non-zero upper triangular elems
                nnz_u += 1;
            }
            if j == i {
                // Remember index of the diagonal element
                diag_idx = output.len();
            }

            // Fill in the origin values
            output.push(*v);
        }

        let (row_idcs, cols, _) = source.0.csr_data();

        // Go through non-zero elements in i-th row
        for kk in row_idcs[i]..row_idcs[i + 1] {
            let k = cols[kk];

            if k >= i {
                // Take into account only previous rows
                continue;
            }

            let l_ik = output[kk] / diag[k];
            // U[i, k+1:] -= L_ik * U[k, k+1:]
            let (mut cur_i, mut cur_k) = (row_idcs[i], row_idcs[k]);

            while cur_i < row_idcs[i + 1] && cur_k < row_idcs[k + 1] {
                // This if statement is keeping column at row i and k in sync (sparsity pattern will be different)
                match cols[cur_i].cmp(&cols[cur_k]) {
                    Ordering::Equal => {
                        if cols[cur_i] > k {
                            // col is >= k+1
                            let k_val = output[cur_k];
                            output[cur_i] -= l_ik * k_val;
                        }

                        // Advance both
                        cur_i += 1;
                        cur_k += 1;
                    }
                    Ordering::Less => {
                        // col_i is behind
                        cur_i += 1;
                    }
                    Ordering::Greater => {
                        // col_k is behind
                        cur_k += 1;
                    }
                }
            }

            output[kk] = l_ik;
        }

        diag.push(output[diag_idx]);
    }

    (nnz_l, nnz_u)
}

pub struct ILU0Preconditioner<T> {
    pub l: SparseMatrix<T>,
    pub u: SparseMatrix<T>,
}

impl<'a, 'b, T, V> Mul<&'a V> for &'b ILU0Preconditioner<T>
where
    T: ScalarField + Sum,
    V: Vector<Element = T>,
{
    type Output = FullVector<T>;

    fn mul(self, rhs: &'a V) -> Self::Output {
        // (LU)^-1 = U^-1 L^-1
        let mut x = FullVector::<T>::zero(rhs.rows());

        for i in 0..x.nrows() {
            let l_row = self.l.0.row(i);
            let l_ii = self.l[[i, i]];

            x[i] = (rhs[i]
                - l_row
                    .col_indices()
                    .iter()
                    .zip(l_row.values())
                    .take_while(|(j, _)| **j < i)
                    .map(|(j, l_ij)| *l_ij * x[*j])
                    .sum::<T>())
                / l_ii;
        }

        for i in (0..x.nrows()).rev() {
            let u_row = self.u.0.row(i);
            let u_ii = self.u[[i, i]];

            x[i] = (x[i]
                - u_row
                    .col_indices()
                    .iter()
                    .zip(u_row.values())
                    .filter(|(j, _)| **j > i)
                    .map(|(j, u_ij)| *u_ij * x[*j])
                    .sum::<T>())
                / u_ii;
        }

        x
    }
}
