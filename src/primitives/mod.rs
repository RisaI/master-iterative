mod complex;
pub mod grid;
mod indexed;
mod matrix;
pub mod physics;
pub mod precond;
pub mod series;
mod twoform;
mod vector;

pub use complex::*;
pub use indexed::*;
pub use matrix::*;
pub use twoform::*;
pub use vector::*;

pub trait ScalarField = nalgebra::Scalar + nalgebra::Field + Copy;

pub mod prelude {
    pub use super::{FullMatrix, FullVector, Indexed, Matrix, SparseMatrix, Vector, C32, C64};
}
