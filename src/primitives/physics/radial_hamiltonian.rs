use simba::scalar::ComplexField;

use crate::primitives::{grid::QuadratureGrid, DiagonalMatrix};

pub struct RadialHamiltionian<T, V: Fn(T) -> T> {
    pub potential: V,
    pub mass: f64,
    pub angular_momentum: usize,
    pub energy_offset: T,
}

impl<T: ComplexField + Copy, V: Fn(T) -> T> RadialHamiltionian<T, V> {
    pub fn represent_on_grid(&self, grid: &QuadratureGrid<T, T>) -> nalgebra_sparse::CooMatrix<T> {
        let mut result = grid.stiffness_matrix_whole(T::from_subset(&(-1. / (2. * self.mass))));

        // diagonal
        for i in 0..result.nrows() {
            let x = grid[i];
            let e = self.energy_offset;

            let mut pot = (self.potential)(x);

            if self.angular_momentum > 0 {
                let l = self.angular_momentum as f64;
                let m = self.mass;

                pot += T::from_subset(&(l * (l + 1.))) / (T::from_subset(&(2. * m)) * x * x);
            }

            result.push(i, i, pot - e);
        }

        result
    }

    pub fn represent_potential_dvr(&self, grid: &QuadratureGrid<T, T>) -> DiagonalMatrix<T> {
        grid.represent_fn_dvr(&self.potential).into()
    }
}
