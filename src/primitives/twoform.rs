use super::{Matrix, ScalarField, Vector};

pub struct TwoForm<'a, M: Matrix>(&'a M);

impl<'a, T: ScalarField, M: Matrix<Element = T>> TwoForm<'a, M> {
    pub fn wrap(mat: &'a M) -> Self {
        Self(mat)
    }

    pub fn product<V, W>(&self, lhs: &V, rhs: &W) -> T
    where
        V: Vector<Element = T>,
        W: Vector<Element = T>,
    {
        assert_eq!(lhs.rows(), rhs.rows());
        assert_eq!(rhs.rows(), self.0.cols());

        let mut acc = T::zero();

        for i in 0..self.0.cols() {
            let mut elem = T::zero();

            for j in 0..self.0.cols() {
                elem += *self.0.get([i, j]) * rhs[j];
            }

            acc += elem * lhs[i];
        }

        acc
    }

    pub fn mul_norm_squared<V>(&self, rhs: &V) -> T
    where
        V: Vector<Element = T>,
    {
        assert_eq!(rhs.rows(), self.0.cols());

        let mut acc = V::Element::zero();

        for i in 0..self.0.cols() {
            let mut elem = T::zero();

            for j in 0..self.0.cols() {
                elem += *self.0.get([i, j]) * rhs[j];
            }

            acc = elem * elem;
        }

        acc
    }
}
