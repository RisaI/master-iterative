use std::ops::{Index, IndexMut};

pub trait Indexed<const N: usize>: Bounds<N> + Index<[usize; N], Output = Self::Item> {
    type Item;

    fn get(&self, index: [usize; N]) -> &Self::Item {
        &self[index]
    }

    fn set(&mut self, index: [usize; N], item: Self::Item)
    where
        Self: IndexMut<[usize; N]>,
    {
        self[index] = item;
    }
}

pub trait Bounds<const N: usize> {
    fn bound(&self, pos: usize) -> usize;
}
