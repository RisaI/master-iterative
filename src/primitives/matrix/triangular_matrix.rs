#![allow(clippy::uninit_vec)]
use std::ops::{Index, IndexMut};

use crate::primitives::{Bounds, FullVector, Indexed, ScalarField, Vector};

use super::{FromMatrix, FullMatrix, IntoTranspose, Invert, Matrix, OptMatMul};

#[derive(Clone)]
#[repr(C)]
pub struct TriangularMatrix<T, const LOWER: bool> {
    data: Box<[T]>,
    dim: usize,
    zero: T,
}

impl<T, const LOWER: bool> TriangularMatrix<T, LOWER>
where
    T: ScalarField,
{
    pub fn back_substitute<V: Vector<Element = T>>(&self, v: &V) -> FullVector<T> {
        assert_eq!(self.dim, v.rows());

        let mut result = Vec::with_capacity(self.dim);

        unsafe {
            result.set_len(self.dim);
        }

        if LOWER {
            for i in 0..self.dim {
                let d = self[(i, i)];

                let mut val = v[i] / d;

                for (j, &r_j) in result.iter().enumerate().take(i) {
                    val -= r_j * self[(i, j)] / d;
                }

                result[i] = val;
            }
        } else {
            for i in (0..self.dim).rev() {
                let d = self[(i, i)];

                let mut val = v[i] / d;

                for (j, &r_j) in result.iter().enumerate().take(self.dim).skip(i + 1) {
                    val -= r_j * self[(i, j)] / d;
                }

                result[i] = val;
            }
        }

        result.into()
    }
}

impl<T, const LOWER: bool> Bounds<2> for TriangularMatrix<T, LOWER> {
    fn bound(&self, _: usize) -> usize {
        self.dim
    }
}

impl<T, const LOWER: bool> std::ops::Index<(usize, usize)> for TriangularMatrix<T, LOWER> {
    type Output = T;

    fn index(&self, index: (usize, usize)) -> &Self::Output {
        &self[[index.0, index.1]]
    }
}

impl<T, const LOWER: bool> Indexed<2> for TriangularMatrix<T, LOWER> {
    type Item = T;
}

impl<T, const LOWER: bool> Index<[usize; 2]> for TriangularMatrix<T, LOWER> {
    type Output = T;

    fn index(&self, index: [usize; 2]) -> &Self::Output {
        if LOWER {
            if index[1] > index[0] {
                &self.zero
            } else {
                &self.data[(index[1] + ((index[0] + 1) * index[0]) / 2)]
            }
        } else if index[0] > index[1] {
            &self.zero
        } else {
            &self.data[(index[0] + ((index[1] + 1) * index[1]) / 2)]
        }
    }
}

impl<T, const LOWER: bool> IndexMut<[usize; 2]> for TriangularMatrix<T, LOWER> {
    fn index_mut(&mut self, index: [usize; 2]) -> &mut Self::Output {
        if LOWER {
            if index[1] <= index[0] {
                &mut self.data[(index[1] + ((index[0] + 1) * index[0]) / 2)]
            } else {
                panic!()
            }
        } else if index[0] <= index[1] {
            &mut self.data[(index[0] + ((index[1] + 1) * index[1]) / 2)]
        } else {
            panic!()
        }
    }
}

impl<T: ScalarField, const LOWER: bool> Matrix for TriangularMatrix<T, LOWER> {
    type Element = T;

    fn cols(&self) -> usize {
        self.dim
    }

    fn rows(&self) -> usize {
        self.dim
    }

    fn mul_vec<V: crate::primitives::Vector<Element = Self::Element>>(&self, vec: &V) -> V {
        debug_assert_eq!(self.cols(), vec.rows());

        let mut result = V::zero(self.rows());

        if LOWER {
            for i in 0..self.dim {
                let mut sum = Self::Element::zero();

                for j in 0..=i {
                    sum += vec[j] * *self.get([i, j]);
                }

                result[i] = sum;
            }
        } else {
            for i in 0..self.dim {
                let mut sum = Self::Element::zero();

                for j in i..self.dim {
                    sum += vec[j] * *self.get([i, j]);
                }

                result[i] = sum;
            }
        }

        result
    }

    fn mul_vec_to<V: crate::primitives::Vector<Element = Self::Element>>(
        &self,
        vec: &V,
        output: &mut [Self::Element],
    ) {
        debug_assert_eq!(self.cols(), vec.rows());

        if LOWER {
            for (i, out) in output.iter_mut().enumerate() {
                let mut sum = Self::Element::zero();

                for j in 0..=i {
                    sum += vec[j] * *self.get([i, j]);
                }

                *out = sum;
            }
        } else {
            for (i, out) in output.iter_mut().enumerate() {
                let mut sum = Self::Element::zero();

                for j in i..self.dim {
                    sum += vec[j] * *self.get([i, j]);
                }

                *out = sum;
            }
        }
    }
}

impl<T: ScalarField, M: Matrix<Element = T>, const LOWER: bool> FromMatrix<M>
    for TriangularMatrix<T, LOWER>
{
    fn from_mat(m: &M) -> Self {
        assert!(m.rows() == m.cols());

        Self {
            dim: m.rows(),
            zero: T::zero(),
            data: if LOWER {
                (0..m.rows())
                    .flat_map(|i| (0..=i).map(move |j| *m.get([i, j])))
                    .collect()
            } else {
                (0..m.cols())
                    .flat_map(|j| (0..=j).map(move |i| *m.get([i, j])))
                    .collect()
            },
        }
    }
}

impl<T: ScalarField> IntoTranspose for TriangularMatrix<T, true> {
    type Result = TriangularMatrix<T, false>;

    fn into_transpose(self) -> Self::Result {
        Self::Result {
            data: self.data,
            dim: self.dim,
            zero: self.zero,
        }
    }
}

impl<T: ScalarField> IntoTranspose for TriangularMatrix<T, false> {
    type Result = TriangularMatrix<T, true>;

    fn into_transpose(self) -> Self::Result {
        Self::Result {
            data: self.data,
            dim: self.dim,
            zero: self.zero,
        }
    }
}

impl<T: ScalarField> TriangularMatrix<T, true> {
    pub fn transpose_ref(&self) -> &TriangularMatrix<T, false> {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: ScalarField> TriangularMatrix<T, false> {
    pub fn transpose_ref(&self) -> &TriangularMatrix<T, false> {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: ScalarField> Invert for TriangularMatrix<T, false> {
    type Output = TriangularMatrix<T, true>;

    fn invert(&self) -> <Self as Invert>::Output {
        let mut output = TriangularMatrix {
            dim: self.dim,
            zero: self.zero,
            data: self.data.clone(),
        };
        let n = self.dim;

        for k in 0..n {
            output.set([k, k], T::one() / *self.get([k, k]));

            for i in (k + 1)..n {
                let lii = *self.get([i, i]);
                output.set(
                    [i, k],
                    -{
                        let mut sum = T::zero();

                        for j in k..i {
                            sum += *self.get([i, j]) * *output.get([j, k]);
                        }

                        sum
                    } / lii,
                );
            }
        }

        output
    }
}

impl<T: ScalarField> Invert for TriangularMatrix<T, true> {
    type Output = TriangularMatrix<T, false>;

    // https://www.researchgate.net/figure/Matrix-inversion-of-upper-triangular-matrices_fig1_220094411
    fn invert(&self) -> <Self as Invert>::Output {
        let mut output = TriangularMatrix {
            dim: self.dim,
            zero: self.zero,
            data: self.data.clone(),
        };
        let n = self.dim;

        for j in 0..n {
            for i in 1..j {
                for k in 1..j {
                    output.set(
                        [i, j],
                        *output.get([i, j]) + *output.get([i, k]) * *self.get([k, j]),
                    );
                }
            }

            for k in 1..j {
                output.set([k, j], -*output.get([k, j]) / *self.get([j, j]));
            }

            output.set([j, j], T::one() / *self.get([j, j]));
        }

        output
    }
}

impl<T: ScalarField, const LOWER: bool> OptMatMul<FullMatrix<T>> for TriangularMatrix<T, LOWER> {
    type Output = FullMatrix<T>;

    fn opt_mul<B: std::borrow::Borrow<FullMatrix<T>>>(
        &self,
        rhs: B,
    ) -> <Self as OptMatMul<FullMatrix<T>>>::Output {
        let b = rhs.borrow();
        let mut output = b.clone();

        for i in 0..self.rows() {
            for j in 0..b.cols() {
                let mut acc = T::zero();

                for k in 0..self.cols() {
                    acc += *self.get([i, k]) * *b.get([k, j]);
                }

                output.set([i, j], acc);
            }
        }

        output
    }
}

// 1. for k = 1 to n
// 2.   X[k,k] = l/L[k,k]
// 3.   for i = k+1 to n
// 4.     X[i,k] = -L[i, k:i-1]*X[k:i-1,k]/L[i,i]
// 5.   end for i
// 6. end for k
