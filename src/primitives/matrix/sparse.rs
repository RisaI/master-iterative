use std::{
    fmt::{Debug, Display},
    ops::{Index, IndexMut, Mul},
};

use nalgebra_sparse::CsrMatrix;

use crate::primitives::{Bounds, FullVector, Indexed, ScalarField};

use super::Matrix;

#[derive(Clone)]
pub struct SparseMatrix<T>(pub CsrMatrix<T>);

impl<T> SparseMatrix<T>
where
    T: ScalarField,
{
    pub fn shrink(&self, left: usize, right: usize, top: usize, bot: usize) -> Self {
        let nrows = self.0.nrows();
        let ncols = self.0.ncols();

        let mut result = nalgebra_sparse::CooMatrix::new(nrows - left - right, ncols - top - bot);

        self.0.triplet_iter().for_each(|(i, j, v)| {
            if i >= top && j >= left && i < nrows - bot && j < ncols - right {
                result.push(i - top, j - left, *v);
            }
        });

        Self((&result).into())
    }

    pub fn into_full(self) -> super::FullMatrix<T>
    where
        T: Copy + Send + Sync,
    {
        let mut result = super::FullMatrix::new_zero(self.0.nrows(), self.0.ncols());

        self.0.triplet_iter().for_each(|(i, j, v)| {
            result.set([i, j], *v);
        });

        result
    }
}

impl<T, M> From<M> for SparseMatrix<T>
where
    M: Into<CsrMatrix<T>>,
{
    fn from(mat: M) -> Self {
        Self(mat.into())
    }
}

impl<T: ScalarField> Matrix for SparseMatrix<T> {
    type Element = T;

    fn cols(&self) -> usize {
        self.bound(1)
    }

    fn rows(&self) -> usize {
        self.bound(0)
    }
}

impl<T> Bounds<2> for SparseMatrix<T> {
    fn bound(&self, pos: usize) -> usize {
        match pos {
            0 => self.0.nrows(),
            _ => self.0.ncols(),
        }
    }
}

impl<T> Indexed<2> for SparseMatrix<T> {
    type Item = T;
}

impl<T> Index<[usize; 2]> for SparseMatrix<T> {
    type Output = T;

    fn index(&self, [i, j]: [usize; 2]) -> &Self::Output {
        match self.0.get_entry(i, j).unwrap() {
            nalgebra_sparse::SparseEntry::NonZero(val) => val,
            _ => todo!(),
        }
    }
}

impl<T> IndexMut<[usize; 2]> for SparseMatrix<T> {
    fn index_mut(&mut self, [i, j]: [usize; 2]) -> &mut Self::Output {
        match self.0.get_entry_mut(i, j).unwrap() {
            nalgebra_sparse::SparseEntryMut::NonZero(val) => val,
            _ => panic!(),
        }
    }
}

impl<T: Debug> Display for SparseMatrix<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl<'a, 'b, T> Mul<&'a FullVector<T>> for &'b SparseMatrix<T>
where
    T: ScalarField + Copy,
{
    type Output = FullVector<T>;

    fn mul(self, rhs: &'a FullVector<T>) -> Self::Output {
        &self.0 * rhs
    }
}
