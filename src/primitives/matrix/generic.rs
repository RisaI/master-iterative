#![allow(clippy::wrong_self_convention)]
use std::{
    borrow::{Borrow, BorrowMut},
    fmt::Display,
    marker::PhantomData,
    ops::{Index, IndexMut},
};

use num_traits::Zero;

use crate::primitives::Bounds;

use super::super::{Indexed, ScalarField, Vector};

pub trait Matrix: Indexed<2, Item = Self::Element> + Sized {
    type Element: ScalarField + Copy;

    fn cols(&self) -> usize;
    fn rows(&self) -> usize;

    fn mul_vec<V>(&self, vec: &V) -> V
    where
        V: Vector<Element = Self::Element>,
    {
        debug_assert_eq!(self.cols(), vec.rows());

        let mut result = V::zero(self.rows());

        for i in 0..self.rows() {
            let mut sum = Self::Element::zero();

            for j in 0..self.cols() {
                sum += vec[j] * *self.get([i, j]);
            }

            result[i] = sum;
        }

        result
    }

    fn mul_vec_to<V: Vector<Element = Self::Element>>(
        &self,
        vec: &V,
        output: &mut [Self::Element],
    ) {
        debug_assert_eq!(self.cols(), vec.rows());

        for (i, out) in output.iter_mut().enumerate().take(self.rows()) {
            let mut sum = Self::Element::zero();

            for j in 0..self.cols() {
                sum += vec[j] * *self.get([i, j]);
            }

            *out = sum;
        }
    }

    fn naive_mul_mat<M: Matrix<Element = Self::Element>, B: Borrow<M>>(
        &self,
        rhs: B,
    ) -> super::FullMatrix<Self::Element> {
        let b = rhs.borrow();
        let mut output = super::FullMatrix::new_zero(self.rows(), b.cols());

        for i in 0..self.rows() {
            for j in 0..b.cols() {
                let mut acc = Self::Element::zero();

                for k in 0..self.cols() {
                    acc += *self.get([i, k]) * *b.get([k, j]);
                }

                output.set([i, j], acc);
            }
        }

        output
    }

    fn map_diagonal<F: Fn(Self::Element, usize) -> Self::Element>(&mut self, f: F)
    where
        Self: IndexMut<[usize; 2], Output = Self::Element>,
    {
        let range = 0..(self.rows().min(self.cols()));

        for i in range {
            self.set([i, i], f(*self.get([i, i]), i))
        }
    }
}

pub trait FromMatrix<M>
where
    M: Matrix,
{
    fn from_mat(m: &M) -> Self;
}

pub trait IntoMatrix<M>: Matrix
where
    M: Matrix,
{
    fn into_mat(&self) -> M;
}

impl<M, N> IntoMatrix<M> for N
where
    M: Matrix + FromMatrix<N>,
    N: Matrix,
{
    fn into_mat(&self) -> M {
        M::from_mat(self)
    }
}

pub trait IntoTranspose: Matrix {
    type Result: Matrix;

    fn into_transpose(self) -> Self::Result;
}

pub struct TransposedMatrix<B: Borrow<M>, M: Matrix> {
    mat: B,
    phantom: PhantomData<M>,
}

impl<B: Borrow<M>, M: Matrix> TransposedMatrix<B, M> {
    pub fn wrap(mat: B) -> Self {
        Self {
            mat,
            phantom: PhantomData::default(),
        }
    }
}

impl<B: Borrow<M>, M: Matrix> Bounds<2> for TransposedMatrix<B, M> {
    fn bound(&self, pos: usize) -> usize {
        self.mat.borrow().bound(1 - pos)
    }
}

impl<B: Borrow<M>, M: Matrix> Indexed<2> for TransposedMatrix<B, M> {
    type Item = M::Element;
}

impl<B: Borrow<M>, M: Matrix> Index<[usize; 2]> for TransposedMatrix<B, M> {
    type Output = M::Element;

    fn index(&self, index: [usize; 2]) -> &Self::Output {
        &self.mat.borrow()[index]
    }
}

impl<B: BorrowMut<M>, M: Matrix + IndexMut<[usize; 2], Output = M::Element>> IndexMut<[usize; 2]>
    for TransposedMatrix<B, M>
{
    fn index_mut(&mut self, index: [usize; 2]) -> &mut Self::Output {
        &mut self.mat.borrow_mut()[index]
    }
}

impl<B: Borrow<M>, M: Matrix> Matrix for TransposedMatrix<B, M> {
    type Element = M::Element;

    fn cols(&self) -> usize {
        self.mat.borrow().cols()
    }

    fn rows(&self) -> usize {
        self.mat.borrow().rows()
    }
}

// impl<'a, M: Matrix + OptMatMul<N>, N: Matrix<Element = M::Element>> OptMatMul<N> for TransposedMatrix<'a, M> {
//     type Output = M::Output;

//     fn opt_mul<B>(&self, rhs: B) -> Self::Output where B: Borrow<N> {
//         self.mat.opt_mul(rhs)
//     }
// }

pub trait Invert: Matrix {
    type Output: Matrix;

    fn invert(&self) -> <Self as Invert>::Output;
}

pub trait OptMatMul<Rhs: Matrix<Element = Self::Element>>: Matrix {
    type Output: Matrix<Element = Self::Element>;

    fn opt_mul<B: Borrow<Rhs>>(&self, rhs: B) -> <Self as OptMatMul<Rhs>>::Output;
}

pub trait OptMatMulAssign<Rhs: Matrix>: Matrix<Element = Rhs::Element> {
    fn opt_mul_assign<B: BorrowMut<Rhs>>(&self, rhs: B) -> B;
}

pub fn print_matrix<T: ScalarField + Display, M: Matrix<Element = T>>(mat: &M) {
    let show_cols = mat.cols().min(20);

    println!("[");
    for row in 0..mat.rows() {
        print!("  ");
        for col in 0..show_cols {
            print!("{:.3}, ", mat.get([row, col]));
        }

        if show_cols < mat.cols() {
            println!(" ... ,");
        } else {
            println!();
        }
    }
    println!("]");
}
