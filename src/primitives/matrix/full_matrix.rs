use nalgebra::ComplexField;
use nalgebra_sparse::{CooMatrix, CsrMatrix};
use num_traits::Zero;

use crate::primitives::{Bounds, Vector};

use super::super::{Indexed, ScalarField};
use super::{Matrix, SparseMatrix};
use std::fmt::Display;
use std::ops::{DivAssign, Index, IndexMut, Mul, MulAssign};

#[derive(Clone)]
pub struct FullMatrix<T> {
    cols: usize,
    data: Box<[T]>,
}

impl<T: ScalarField + Display> Display for FullMatrix<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("[\n"))?;

        for i in 0..self.rows() {
            for j in 0..self.cols() {
                f.write_fmt(format_args!("{:.3} ", self.get([i, j])))?;
            }
            f.write_fmt(format_args!("\n"))?;
        }

        f.write_fmt(format_args!("]"))?;

        Ok(())
    }
}

impl<T> Bounds<2> for FullMatrix<T> {
    fn bound(&self, pos: usize) -> usize {
        debug_assert!(pos < 2);

        match pos {
            0 => self.data.len() / self.cols,
            _ => self.cols,
        }
    }
}

impl<T: ScalarField> DivAssign<T> for FullMatrix<T> {
    fn div_assign(&mut self, rhs: T) {
        self.data.iter_mut().for_each(|value| *value /= rhs);
    }
}

impl<T> Indexed<2> for FullMatrix<T> {
    type Item = T;
}

impl<T> Index<[usize; 2]> for FullMatrix<T> {
    type Output = T;

    fn index(&self, [col, row]: [usize; 2]) -> &Self::Output {
        &self.data[col + self.cols * row]
    }
}

impl<T> IndexMut<[usize; 2]> for FullMatrix<T> {
    fn index_mut(&mut self, [col, row]: [usize; 2]) -> &mut Self::Output {
        &mut self.data[col + self.cols * row]
    }
}

impl<T: ScalarField> Matrix for FullMatrix<T> {
    type Element = T;

    fn cols(&self) -> usize {
        self.cols
    }

    fn rows(&self) -> usize {
        self.data.len() / self.cols
    }
}

impl<T: ScalarField> FullMatrix<T> {
    pub fn new(cols: usize, data: Box<[T]>) -> Self {
        debug_assert_eq!(data.len() % cols, 0);

        Self { cols, data }
    }

    pub fn new_zero(rows: usize, cols: usize) -> Self
    where
        T: Zero,
    {
        Self {
            cols,
            data: (0..(rows * cols)).map(|_| T::zero()).collect(),
        }
    }

    pub fn from_iter(cols: usize, iter: impl Iterator<Item = T>) -> Self {
        Self {
            cols,
            data: iter.collect(),
        }
    }

    pub fn scale(mut self, val: T) -> Self
    where
        T: MulAssign<T>,
    {
        for a in self.data.iter_mut() {
            *a *= val;
        }

        self
    }

    pub fn shrink(&self, left: usize, right: usize, top: usize, bot: usize) -> Self
    where
        T: ScalarField,
    {
        let mut result =
            Vec::with_capacity((self.rows() - top - bot) * (self.cols() - left - right));

        for i in top..(self.rows() - bot) {
            for j in left..(self.cols() - right) {
                result.push(*self.get([i, j]));
            }
        }

        Self {
            cols: self.cols() - left - right,
            data: result.into_boxed_slice(),
        }
    }

    pub fn into_sparse(self) -> SparseMatrix<T>
    where
        T: ScalarField + std::fmt::Debug,
    {
        let mut result = CooMatrix::new(self.rows(), self.cols());

        for i in 0..self.rows() {
            for j in 0..self.cols() {
                let elem = *self.get([i, j]);

                if !elem.is_zero() {
                    result.push(i, j, elem);
                }
            }
        }

        CsrMatrix::from(&result).into()
    }

    pub fn invert(self) -> Self
    where
        T: ComplexField,
    {
        use nalgebra::{DMatrix, Dynamic, VecStorage};
        let mut mat = DMatrix::from_data(VecStorage::new(
            Dynamic::new(self.rows()),
            Dynamic::new(self.cols()),
            self.data.to_vec(),
        ));

        if !mat.try_inverse_mut() {
            panic!("Failed to invert the matrix");
        }

        mat.into()
    }

    pub fn find_eigenvalues(self) -> crate::primitives::FullVector<T>
    where
        T: ComplexField,
    {
        use nalgebra::{DMatrix, Dynamic, VecStorage};
        let mat = DMatrix::from_data(VecStorage::new(
            Dynamic::new(self.rows()),
            Dynamic::new(self.cols()),
            self.data.to_vec(),
        ));

        let vec: Vec<T> = mat.eigenvalues().unwrap().data.into();

        vec.into()
    }
}

impl<'a, 'b, V: Vector<Element = T>, T: ScalarField> Mul<&'a V> for &'b FullMatrix<T> {
    type Output = V;

    fn mul(self, rhs: &'a V) -> Self::Output {
        Matrix::mul_vec(self, rhs)
    }
}

impl<T: ComplexField, M: Into<nalgebra::DMatrix<T>>> From<M> for FullMatrix<T> {
    fn from(mat: M) -> Self {
        let mat: nalgebra::DMatrix<T> = mat.into();
        let cols = mat.ncols();

        Self {
            cols,
            data: Into::<Vec<T>>::into(mat.data).into_boxed_slice(),
        }
    }
}
