use std::{
    borrow::BorrowMut,
    ops::{Index, IndexMut},
};

use crate::primitives::{Bounds, Indexed, ScalarField, Sqrt, Vector};

use super::{FromMatrix, Invert, Matrix, OptMatMulAssign};

#[derive(Clone)]
pub struct DiagonalMatrix<T> {
    data: Box<[T]>,
    zero: T,
}

impl<T: ScalarField + Sqrt> DiagonalMatrix<T> {
    pub fn sqrt<B: BorrowMut<Self>>(mut this: B) -> B {
        let s = this.borrow_mut();
        for val in s.data.iter_mut() {
            *val = val.sqrt();
        }

        this
    }
}
impl<T> Bounds<2> for DiagonalMatrix<T> {
    fn bound(&self, _: usize) -> usize {
        self.data.len()
    }
}

impl<T> Indexed<2> for DiagonalMatrix<T> {
    type Item = T;
}

impl<T> Index<[usize; 2]> for DiagonalMatrix<T> {
    type Output = T;

    fn index(&self, index: [usize; 2]) -> &Self::Output {
        if index[0] == index[1] {
            &self.data[index[0]]
        } else {
            &self.zero
        }
    }
}

impl<T> IndexMut<[usize; 2]> for DiagonalMatrix<T> {
    fn index_mut(&mut self, [i, j]: [usize; 2]) -> &mut Self::Output {
        if i == j {
            &mut self.data[i]
        } else {
            panic!()
        }
    }
}

impl<T: ScalarField> Matrix for DiagonalMatrix<T> {
    type Element = T;

    fn cols(&self) -> usize {
        self.data.len()
    }

    fn rows(&self) -> usize {
        self.data.len()
    }

    fn mul_vec<V: Vector<Element = Self::Element>>(&self, vec: &V) -> V {
        debug_assert_eq!(self.cols(), vec.rows());

        let mut result = V::zero(self.rows());

        for i in 0..self.rows() {
            result[i] *= *self.get([i, i]);
        }

        result
    }

    fn mul_vec_to<V: Vector<Element = Self::Element>>(
        &self,
        vec: &V,
        output: &mut [Self::Element],
    ) {
        debug_assert_eq!(self.cols(), vec.rows());

        for (i, out) in output.iter_mut().enumerate().take(self.rows()) {
            *out = vec[i] * *self.get([i, i]);
        }
    }
}

impl<T: ScalarField, M: Matrix<Element = T>> FromMatrix<M> for DiagonalMatrix<T> {
    fn from_mat(matrix: &M) -> Self {
        assert!(matrix.rows() == matrix.cols());

        Self {
            data: (0..matrix.rows()).map(|i| *matrix.get([i, i])).collect(),
            zero: T::zero(),
        }
    }
}

impl<T: ScalarField, V: IntoIterator<Item = T>> From<V> for DiagonalMatrix<T> {
    fn from(vec: V) -> Self {
        Self {
            zero: T::zero(),
            data: vec.into_iter().collect(),
        }
    }
}

impl<T: ScalarField> Invert for DiagonalMatrix<T> {
    type Output = DiagonalMatrix<T>;

    fn invert(&self) -> <Self as Invert>::Output {
        let mut output = self.clone();

        for val in output.data.iter_mut() {
            *val = T::one() / *val;
        }

        output
    }
}

impl<T: ScalarField, M: Matrix<Element = T> + IndexMut<[usize; 2], Output = T>> OptMatMulAssign<M>
    for DiagonalMatrix<T>
{
    fn opt_mul_assign<B: std::borrow::BorrowMut<M>>(&self, mut rhs: B) -> B {
        let b = rhs.borrow_mut();

        assert_eq!(self.cols(), b.rows());
        assert_eq!(b.rows(), b.cols());

        for i in 0..self.data.len() {
            for j in 0..self.data.len() {
                b.set([i, j], *b.get([i, j]) * *self.get([i, i]));
            }
        }

        rhs
    }
}
