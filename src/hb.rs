use std::{
    error::Error,
    io::{BufRead, BufReader, Read},
};

// Harwell-Boeing matrix format

#[derive(Debug)]
pub struct HBReader<R: Read> {
    header: HBHeader,
    reader: BufReader<R>,
}

impl<R: Read> HBReader<R> {
    pub fn new(reader: R) -> Result<Self, Box<dyn std::error::Error>> {
        let mut buf_read = BufReader::new(reader);

        let header = {
            let mut buffer = String::new();

            buf_read.read_line(&mut buffer)?;
            buffer.truncate(buffer.len() - 1);

            let title = buffer[0..72].trim().into();
            let key = buffer[72..80].trim().into();

            buffer.clear();
            buf_read.read_line(&mut buffer)?;
            buffer.truncate(buffer.len() - 1);
            let counts = HBCounts::parse(&buffer[..])?;

            buffer.clear();
            buf_read.read_line(&mut buffer)?;
            buffer.truncate(buffer.len() - 1);
            let mat_type = HBMatrixType::parse(&buffer[..])?;

            // Pointer format skip
            buf_read.read_line(&mut buffer)?;

            // RHS skip
            if counts.rhs > 0 {
                buffer.clear();
                buf_read.read_line(&mut buffer)?;
            }

            HBHeader {
                title,
                key,

                counts,
                mat_type,
            }
        };

        Ok(Self {
            header,
            reader: buf_read,
        })
    }

    pub fn read(mut self) -> Result<(HBHeader, HBCSCMatrix), Box<dyn Error>> {
        let mut buffer = String::new();

        for _ in 0..self.header.counts.pointers {
            self.reader.read_line(&mut buffer)?;
            buffer.truncate(buffer.len() - 1);
        }

        let pointers: Vec<usize> = buffer
            .split(' ')
            .filter_map(|s| {
                if s.is_empty() {
                    None
                } else {
                    Some(s.parse::<usize>().map(|i| i - 1).unwrap())
                }
            })
            .collect();

        buffer.clear();
        for _ in 0..self.header.counts.indices {
            self.reader.read_line(&mut buffer)?;
            buffer.truncate(buffer.len() - 1);
        }

        let row_idx: Vec<usize> = buffer
            .split(' ')
            .filter_map(|s| {
                if s.is_empty() {
                    None
                } else {
                    Some(s.parse::<usize>().map(|i| i - 1).unwrap())
                }
            })
            .collect();

        buffer.clear();
        for _ in 0..self.header.counts.values {
            self.reader.read_line(&mut buffer)?;
            buffer.truncate(buffer.len() - 1);
        }

        let values: Vec<f64> = buffer
            .split(' ')
            .filter_map(|s| {
                if s.is_empty() {
                    None
                } else {
                    Some(s.parse::<f64>().unwrap())
                }
            })
            .collect();

        let symmetry = self.header.mat_type.symmetry.clone();
        let numeric = self.header.mat_type.numeric.clone();

        Ok((
            self.header,
            HBCSCMatrix {
                complex: matches!(numeric, HBNumericType::Complex),
                indices: row_idx,
                pointers,
                values,
                symmetry,
            },
        ))
    }
}

#[derive(Debug)]
pub struct HBHeader {
    pub title: String,
    pub key: String,

    pub counts: HBCounts,
    pub mat_type: HBMatrixType,
}

#[derive(Debug)]
pub struct HBCounts {
    pub total: usize,
    pub pointers: usize,
    pub indices: usize,
    pub values: usize,
    pub rhs: usize,
}

impl HBCounts {
    pub fn parse(line: &str) -> Result<Self, Box<dyn Error>> {
        Ok(Self {
            total: line[0..14].trim().parse()?,
            pointers: line[14..28].trim().parse()?,
            indices: line[28..42].trim().parse()?,
            values: line[42..56].trim().parse()?,
            rhs: if line.len() > 56 {
                line[56..70].trim().parse()?
            } else {
                0
            },
        })
    }
}

#[derive(Debug)]
pub struct HBMatrixType {
    pub numeric: HBNumericType,
    pub symmetry: HBSymmetryType,
    pub is_assembled: bool,

    pub rows: usize,
    pub cols: usize,
    pub nonzeros: usize,
    pub elem_matrices: usize,
}

impl HBMatrixType {
    pub fn parse(line: &str) -> Result<Self, Box<dyn Error>> {
        let mut tp = line[0..3].chars();

        Ok(Self {
            numeric: HBNumericType::from_letter(tp.next().unwrap()).unwrap(),
            symmetry: HBSymmetryType::from_letter(tp.next().unwrap()).unwrap(),
            is_assembled: match tp.next().unwrap().to_ascii_uppercase() {
                'A' => true,
                'E' => false,
                _ => panic!("Unknown assembly type"),
            },

            rows: line[14..28].trim().parse()?,
            cols: line[28..42].trim().parse()?,
            nonzeros: line[42..56].trim().parse()?,
            elem_matrices: line[56..70].trim().parse()?,
        })
    }
}

#[derive(Debug, Clone)]
pub enum HBNumericType {
    Real,
    Complex,
    Pattern,
}

impl HBNumericType {
    pub fn from_letter(letter: char) -> Option<Self> {
        match letter.to_ascii_uppercase() {
            'R' => Some(Self::Real),
            'C' => Some(Self::Complex),
            'P' => Some(Self::Pattern),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum HBSymmetryType {
    Symmetric,
    Unsymmetric,
    Hermitian,
    SkewSymmetric,
    Rectangular,
}

impl HBSymmetryType {
    pub fn from_letter(letter: char) -> Option<Self> {
        match letter.to_ascii_uppercase() {
            'S' => Some(Self::Symmetric),
            'U' => Some(Self::Unsymmetric),
            'H' => Some(Self::Hermitian),
            'Z' => Some(Self::SkewSymmetric),
            'R' => Some(Self::Rectangular),
            _ => None,
        }
    }
}

pub struct HBCSCMatrix {
    pub pointers: Vec<usize>,
    pub indices: Vec<usize>,
    pub values: Vec<f64>,
    pub complex: bool,
    pub symmetry: HBSymmetryType,
}

impl HBCSCMatrix {
    pub fn iter(&self) -> impl Iterator<Item = (usize, usize, SparseMatElem)> + '_ {
        self.pointers
            .windows(2)
            .enumerate()
            .flat_map(move |(col_idx, range)| {
                (range[0]..range[1]).map(move |p| {
                    (
                        self.indices[p],
                        col_idx,
                        if self.complex {
                            SparseMatElem::Complex(self.values[2 * p], self.values[2 * p + 1])
                        } else {
                            SparseMatElem::Real(self.values[p])
                        },
                    )
                })
            })
    }
}

#[derive(Debug)]
pub enum SparseMatElem {
    Real(f64),
    Complex(f64, f64),
}
