use std::{
    collections::{HashMap, HashSet},
    error::Error,
    f64::consts::FRAC_PI_4,
    sync::{Arc, Mutex},
};

use iterate::{
    models::{self, Model2D},
    primitives::{grid::Quadrature, FullVector, Vector, C64},
    problems::{model_1d::calculation::IterativeError, model_2d::*},
};
use itertools::iproduct;
use num_traits::Zero;
use rayon::prelude::*;
use rusqlite::{params, Connection};

const ME: GridOrdering = GridOrdering::MoleculeElectron;
const EM: GridOrdering = GridOrdering::ElectronMolecule;

const TARGET_PREC: f64 = 1e-8;
const GL_ORDERS: &[(usize, usize, usize, usize)] = &[
    (5, 5, 513, 150_000),
    (10, 8, 257, 90_000),
    (15, 11, 129, 50_000),
    (17, 15, 65, 25_000),
];
const MODELS: &[(&str, &[&str], &[GridOrdering], &[f64])] = &[
    (
        "n2",
        &["ilu0", "partial-invert", "direct"],
        &[EM, ME],
        &[0.03, 0.092, 0.18],
    ),
    ("f2", &["ilu0", "direct"], &[EM], &[0.0001, 0.50005, 1.0]),
    ("no", &["ilu0", "direct"], &[EM, ME], &[0.0275, 0.032, 0.06]),
    (
        "o2",
        &["ilu0", "direct"],
        &[EM, ME],
        &[0.01, 0.00497336979166666, 0.010635, 0.05],
    ),
];
const TRANSITIONS: &[usize] = &[0, 1, 2, 8];

type Result<T> = std::result::Result<T, Box<dyn Error>>;

fn equidist(from: f64, to: f64, points: usize) -> impl Iterator<Item = f64> + Clone {
    assert!(points > 1);

    let d = (to - from) / (points - 1) as f64;

    (0..points).map(move |i| from + d * i as f64)
}

fn solve(
    opts: &ModelParams,
    on_iter: Option<impl FnMut(f64)>,
) -> std::result::Result<ModelRunResult, IterativeError> {
    solve_with_guess(opts, on_iter, None)
}

fn solve_with_guess(
    opts: &ModelParams,
    mut on_iter: Option<impl FnMut(f64)>,
    guess: Option<FullVector<C64>>,
) -> std::result::Result<ModelRunResult, IterativeError> {
    run_model_2d(
        opts,
        |v| {
            if let Some(m) = on_iter.as_mut() {
                m(v)
            }
        },
        guess,
    )
}

fn get_preconditioner(method: &str) -> Preconditioner {
    match method {
        "direct" => Preconditioner::Direct,
        "iterative" => Preconditioner::None,
        "ilu0" => Preconditioner::ILU0,
        "partial-invert" => Preconditioner::PartialInvert,
        "partial-solve" => Preconditioner::PartialSolve,
        _ => panic!("unknown method"),
    }
}

fn get_model_by_str(model: &str) -> &'static Model2D {
    match model {
        "n2" => &models::MODEL_2D_N2,
        "f2" => &models::MODEL_2D_F2,
        "o2" => &models::MODEL_2D_O2,
        "no" => &models::MODEL_2D_NO,
        _ => panic!("unknown model"),
    }
}

fn get_range(model: &str) -> (f64, f64) {
    match model {
        "n2" => (0.02, 0.2),
        "f2" => (0.0001, 0.1),
        "no" => (0.01, 0.08),
        "o2" => (0.0025, 0.06),
        _ => todo!(),
    }
}

fn main() -> Result<()> {
    // let matches = App::new("DD Orchestration").get_matches();

    let conn = Connection::open("results_2d.db")?;

    println!("Running orchestrated work for 2D model");

    let conn = Arc::new(Mutex::new(conn));

    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS iters_per_e_2d (
            id INTEGER PRIMARY KEY AUTOINCREMENT,

            model VARCHAR(32) NOT NULL,
            method VARCHAR(32) NOT NULL,
            ordering VARCHAR(8) NOT NULL,
            gl VARCHAR(8) NOT NULL,
            vin INTEGER NOT NULL,
            vout INTEGER,

            e REAL NOT NULL,
            iters INTEGER NOT NULL,
            runtime REAL NOT NULL,
            error REAL NOT NULL,
            sigmave REAL,
            sigmada REAL,

            UNIQUE(model, method, ordering, gl, vin, vout, e)
        )",
        params![],
    )?;

    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS ilu0_divergence (
            id INTEGER PRIMARY KEY AUTOINCREMENT,

            ordering VARCHAR(8) NOT NULL,
            e REAL NOT NULL,

            diverged INTEGER NOT NULL,
            iters INTEGER NOT NULL,
            error REAL NOT NULL,
            sigmave REAL,

            UNIQUE(ordering, e)
        )",
        params![],
    )?;

    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS ilu0_divergence_conv (
            id INTEGER PRIMARY KEY AUTOINCREMENT,

            ordering VARCHAR(8) NOT NULL,
            e REAL NOT NULL,

            n INTEGER NOT NULL,
            xi REAL NOT NULL,

            UNIQUE(ordering, e, n)
        )",
        params![],
    )?;

    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS kickstart_2d (
            id INTEGER PRIMARY KEY AUTOINCREMENT,

            model VARCHAR(32) NOT NULL,
            method VARCHAR(32) NOT NULL,
            ordering VARCHAR(8) NOT NULL,
            gl VARCHAR(8) NOT NULL,

            e REAL NOT NULL,
            iters INTEGER NOT NULL,
            sigmave REAL,

            UNIQUE(model, method, ordering, gl, e)
        )",
        params![],
    )?;

    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS conv_per_n_2d (
            id INTEGER PRIMARY KEY AUTOINCREMENT,

            model VARCHAR(32) NOT NULL,
            method VARCHAR(32) NOT NULL,
            ordering VARCHAR(8) NOT NULL,
            gl VARCHAR(8) NOT NULL,
            e REAL NOT NULL,

            n INTEGER NOT NULL,
            xi REAL NOT NULL,

            UNIQUE(model, method, ordering, gl, e, n)
        )",
        params![],
    )?;

    // ANCHOR Number of iterations for required precision and GL order per energy N(E, GL)

    println!("Ultralocal method");

    let used_states = TRANSITIONS
        .iter()
        .copied()
        .collect::<HashSet<_>>()
        .into_iter()
        .collect::<Vec<_>>();

    for (gl, (model, methods, orderings, energies)) in iproduct!(GL_ORDERS, MODELS) {
        println!("running {model} {}x{}", gl.0, gl.1);

        let range = get_range(model);
        let model = get_model_by_str(model);

        // Setup grid for coordinate R
        let (states_m, grid_m) = {
            let grid_m = (model.build_grid_m)();
            let grid_sub_m = grid_m.subgrid(|p| p.im.is_zero());
            let grid_m = grid_m.into_quadrature_grid(Quadrature::GaussLobatto(gl.0));

            // Find EV and ES for molecular states
            let system = Eigensystem::get_molecule_eigensystem(
                model,
                &grid_sub_m.into_quadrature_grid(Quadrature::GaussLobatto(gl.0)),
            );

            (
                used_states
                    .iter()
                    .map(|&i| (i, system.get_extended_triplet(i, grid_m.len())))
                    .collect::<HashMap<_, _>>(),
                Arc::new(grid_m),
            )
        };

        // Setup grid for coordinate r
        let (grid_e, e_state_b) = {
            let grid_e = (model.build_grid_e)();
            let grid_sub_e = grid_e
                .subgrid(|p| p.im.is_zero())
                .into_quadrature_grid(Quadrature::GaussLobatto(gl.1));
            let grid_e = grid_e.into_quadrature_grid(Quadrature::GaussLobatto(gl.1));

            // Find EV and ES for electronic state
            let (ev_e_b, es_e_b) = {
                let (ev, v) = ModelParams::get_bound_state(model, &grid_sub_e);

                let mut next = FullVector::<C64>::zero(grid_e.len());

                next.as_mut_slice()[1..=v.len()].copy_from_slice(v.as_slice());

                (ev, next)
            };

            (Arc::new(grid_e), Arc::new((ev_e_b, es_e_b)))
        };

        for (&method, &ordering) in iproduct!(methods.iter(), orderings.iter()) {
            const BANNED: &[&str] = &["partial-invert", "ilu0"];
            if gl.0 > 10 && ordering == ME && BANNED.contains(&method) {
                continue;
            }

            let opts = ModelParams {
                target_prec: TARGET_PREC,
                max_iters: Some(gl.3),
                model,
                quad_order: (gl.0, gl.1),
                ordering,
                preconditioner: get_preconditioner(method),
                t_e: 0.,

                grid_e: grid_e.clone(),
                grid_m: grid_m.clone(),
                e_state_b: e_state_b.clone(),
                m_state_i: states_m.get(&0).unwrap().clone(),
            };

            if gl.0 == 10 && gl.1 == 8 && method == "ilu0" && model.name == "O_2" {
                println!(" - ILU0 ({}) divergence", ordering.to_string());

                let (from, to, pts) = {
                    let mid_point = 0.00497303645833333;
                    let size = 5e-6;

                    (mid_point - size, mid_point + size, 61)
                };

                // get existing points
                let existing = {
                    let mut existing = vec![];
                    let lock = conn.lock().unwrap();
                    let mut stmt = lock.prepare_cached(
                        "SELECT DISTINCT e FROM ilu0_divergence WHERE ordering = ?",
                    )?;
                    let mut rows = stmt.query_map(params![ordering.to_string()], |row| {
                        Ok(row.get::<_, f64>(0)?)
                    })?;

                    while let Some(Ok(e)) = rows.next() {
                        existing.push(e);
                    }

                    existing
                };

                let d = (to - from) / (pts - 1) as f64;

                let result = equidist(from, to, pts)
                    .par_bridge()
                    .filter_map(|e| {
                        if existing.iter().any(|prev| (e - prev).abs() < d / 3.) {
                            return None;
                        }

                        let opts = opts.clone_with_energy(e);
                        let result = match solve(&opts, Option::<fn(_)>::None) {
                            Ok(r) => r,
                            Err(IterativeError::Diverged(s)) => {
                                println!("    - diverged at E = {e:.4}");

                                return Some((e, 1e3, s, None));
                            }
                            Err(err) => {
                                eprintln!("    - failed for E = {e:.6}, error: {err}");
                                return None;
                            }
                        };

                        println!("    - converged at E = {e:.6}");
                        Some((
                            e,
                            result.error,
                            result.iters,
                            Some(result.sigma_ve(&opts, states_m.get(&0).unwrap().clone())),
                        ))
                    })
                    .collect::<Vec<_>>();

                {
                    let mut lock = conn.lock().unwrap();
                    let tx = lock.transaction().unwrap();

                    for (e, err, iters, sigmave) in result.into_iter() {
                        tx.execute(
                            "INSERT OR REPLACE INTO ilu0_divergence (ordering, e, diverged, iters, error, sigmave) VALUES (?1, ?2, ?3, ?4, ?5, ?6)",
                            params![ordering.to_string(), e,  if sigmave.is_some() { 0 } else { 1 }, iters, err, sigmave],
                        )
                        .ok();
                    }

                    tx.commit().ok();
                }

                let mut result = vec![];
                let e = 0.00497336979166666;

                match solve(&opts.clone_with_energy(e), Some(|xi| result.push(xi))) {
                    Err(IterativeError::Diverged(s)) => {
                        println!(
                            "     - conv graph diverged successfully at E = {e:.8} in {s} steps"
                        );
                    }
                    Err(err) => {
                        eprintln!("    - failed for E = {e:.8}, error: {err}");
                        continue;
                    }
                    _ => { /* noop */ }
                }

                {
                    let mut lock = conn.lock().unwrap();
                    let tx = lock.transaction().unwrap();

                    for (n, xi) in result.into_iter().enumerate() {
                        tx.execute(
                            "INSERT OR REPLACE INTO ilu0_divergence_conv (ordering, e, n, xi) VALUES (?1, ?2, ?3, ?4)",
                            params![ordering.to_string(), e, n, xi],
                        )
                        .ok();
                    }

                    tx.commit().ok();
                }

                println!(" - ILU0 ({}) kickstart", ordering.to_string());

                // get existing points
                let existing = {
                    let mut existing = vec![];
                    let lock = conn.lock().unwrap();
                    let mut stmt = lock.prepare_cached(
                        "SELECT DISTINCT e FROM kickstart_2d WHERE model = 'O_2' AND method = 'ilu0' AND ordering = ?1 AND gl = '10x8'",
                    )?;
                    let mut rows = stmt.query_map(params![ordering.to_string()], |row| {
                        Ok(row.get::<_, f64>(0)?)
                    })?;

                    while let Some(Ok(e)) = rows.next() {
                        existing.push(e);
                    }

                    existing
                };

                let pts = gl.2;
                let d = (range.1 - range.0) / (pts - 1) as f64;
                let points = equidist(range.0, range.1, pts).collect::<Vec<_>>();

                points.chunks(pts / 8).par_bridge()
                    .for_each(|window| {
                        println!("{}", window.len());
                        let mut prev = None;

                        for &e in window {
                            if existing.iter().any(|&c| (c - e).abs() < d / 3.) {
                                continue;
                            }

                            let opts = opts.clone_with_energy(e);
                            let mut p = None;
                            std::mem::swap(&mut prev, &mut p);
                            let result = match solve_with_guess(&opts, Option::<fn(_)>::None, p) {
                                Ok(r) => r,
                                Err(IterativeError::Diverged(s)) => {
                                    println!("    - diverged at E = {e:.5}");
                                    conn.lock().unwrap().execute(
                                        "INSERT OR REPLACE INTO kickstart_2d (model, method, gl, ordering, e, iters, sigmave) VALUES ('O_2', 'ilu0', '10x8', ?1, ?2, ?3, ?4)",
                                        params![ordering.to_string(), e, s, None::<f64>],
                                    ).map_err(|e| {
                                        eprintln!("{e}");
                                        e
                                    }).ok();
                                    continue;
                                }
                                Err(err) => {
                                    eprintln!("    - failed for E = {e:.4}, error: {err}");
                                    continue;
                                },
                            };

                            println!("    - converged at E = {e:.5}");
                            conn.lock().unwrap().execute(
                                "INSERT OR REPLACE INTO kickstart_2d (model, method, gl, ordering, e, iters, sigmave) VALUES ('O_2', 'ilu0', '10x8', ?1, ?2, ?3, ?4)",
                                params![ordering.to_string(), e, result.iters, result.sigma_ve(&opts, states_m.get(&0).unwrap().clone())],
                            ).map_err(|e| {
                                eprintln!("{e}");
                                e
                            }).ok();

                            prev = Some(result.psi_sc);
                        }
                    });
            }

            println!(" - {method} ({})", ordering.to_string());

            let existing = {
                let lock = conn.lock().unwrap();
                let mut stmt = lock.prepare("SELECT DISTINCT e FROM iters_per_e_2d WHERE model = ?1 AND method = ?2 AND gl = ?3 AND ordering = ?4 AND vin = 0 AND vout = 0").unwrap();

                let mut iter = stmt
                    .query(params![
                        model.name,
                        method,
                        format!("{}x{}", gl.0, gl.1),
                        ordering.to_string(),
                    ])
                    .unwrap();

                let mut result = vec![];

                while let Some(row) = iter.next().unwrap() {
                    result.push(row.get::<_, f64>(0).unwrap());
                }

                result
            };

            let pts = match model.name {
                "F_2" => 32,
                _ => gl.2,
            };
            let d = (range.1 - range.0) / (pts - 1) as f64;
            equidist(range.0, range.1, pts).par_bridge().for_each(|e| {
                if existing.iter().any(|&c| (c - e).abs() < d / 3.) {
                    return;
                }

                let opts = opts.clone_with_energy(e);
                let result = match solve(&opts, Option::<fn(_)>::None) {
                    Ok(r) => r,
                    Err(IterativeError::Diverged(s)) => {
                        println!("    - diverged at E = {e:.4}");
                        conn.lock().unwrap().execute(
                            "INSERT OR REPLACE INTO iters_per_e_2d (model, method, gl, ordering, e, iters, runtime, error, vin, vout) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, 0, 0)",
                            params![model.name, method, format!("{}x{}",gl.0, gl.1), ordering.to_string(), e, s, 0, 1e4],
                        ).map_err(|e| {
                            eprintln!("{e}");
                            e
                        }).ok();
                        return;
                    }
                    Err(err) => {
                        eprintln!("    - failed for E = {e:.4}, error: {err}");
                        return;
                    },
                };

                let sigma_da = result.sigma_da(&opts);
                for transition in TRANSITIONS {
                    let sigma_ve = result.sigma_ve(&opts, states_m.get(transition).unwrap().clone());

                    conn.lock().unwrap().execute(
                        "INSERT OR REPLACE INTO iters_per_e_2d (model, method, gl, ordering, vin, vout, e, iters, runtime, error, sigmave, sigmada) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12)",
                        params![model.name, method, format!("{}x{}",gl.0, gl.1), ordering.to_string(), 0, transition, e, result.iters, result.runtime as f64 / 1e3, result.error, sigma_ve, sigma_da],
                    ).map_err(|e| {
                        eprintln!("{e}");
                        e
                    }).ok();
                }
            });
        }

        if false {
            println!(" - convergence graphs");

            iproduct!(methods.iter(), orderings.iter(), energies.iter()).par_bridge().for_each(|(&method, &ordering, &e)| {
                if model.name == "O_2" || method == "direct" { return; }

                let count: usize = conn.lock().unwrap().query_row(
                    "SELECT COUNT() as c FROM conv_per_n_2d WHERE model = ?1 AND method = ?2 AND ordering = ?3 AND gl = ?4 AND e = ?5 AND xi < 1e-7;",
                    params![model.name, method, ordering.to_string(), format!("{}x{}", gl.0, gl.1),e],
                    |r| r.get(0)
                ).unwrap();

                if count > 0 {
                    return;
                }

                let opts = ModelParams {
                    target_prec: TARGET_PREC * 10.,
                    max_iters: Some(gl.3 + gl.3 / 3),
                    model,
                    quad_order: (gl.0, gl.1),
                    ordering,
                    preconditioner: get_preconditioner(method),
                    t_e: e,

                    grid_e: grid_e.clone(),
                    grid_m: grid_m.clone(),
                    e_state_b: e_state_b.clone(),
                    m_state_i: states_m.get(&0).unwrap().clone(),
                };

                println!("   - {method} ({}) or E = {e:.4}", ordering.to_string());

                let mut convergence = vec![];
                let _result = solve(
                    &opts,
                    Some(|xi| {
                        convergence.push(xi);
                    }),
                );

                match conn.lock()
                    .map(|mut conn| {
                        let tx = conn.transaction().unwrap();

                        for (n, err) in convergence.iter().enumerate() {
                            tx.execute(
                                "INSERT OR REPLACE INTO conv_per_n_2d (model, method, ordering, gl, e, n, xi) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",
                                params![model.name, method, ordering.to_string(), format!("{}x{}", gl.0, gl.1), e, n + 1, err],
                            )
                            .ok();
                        }

                        tx.commit().ok();
                }) {
                    Ok(_) => {
                        println!("   - DONE: {method} ({}) or E = {e:.4}", ordering.to_string());
                    },
                    Err(e) => println!("Failed to commit the convergence graph: {}", e),
                }
            });
        }
    }

    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS acc_per_elems_2d (
            id INTEGER PRIMARY KEY AUTOINCREMENT,

            model VARCHAR(32) NOT NULL,
            method VARCHAR(32) NOT NULL,
            ordering VARCHAR(8) NOT NULL,
            gl VARCHAR(8) NOT NULL,
            e REAL NOT NULL,

            elems INTEGER NOT NULL,
            error REAL NOT NULL,

            UNIQUE(model, method, ordering, gl, e, elems)
        )",
        params![],
    )?;

    let ref_val = conn
        .lock()
        .unwrap()
        .query_row(
            "SELECT sigmave FROM iters_per_e_2d
                WHERE model = 'N_2' AND method = 'ilu0' AND ordering = 'em' AND gl = '17x15'
                AND vin = 0 AND vout = 0 AND ABS(e - 0.0790625) < 1e-8",
            params![],
            |r| r.get::<_, f64>(0),
        )
        .unwrap();

    for gl in [5, 10, 15] {
        let model = get_model_by_str("n2");

        // Setup grid for coordinate r
        let (grid_e, e_state_b) = {
            let grid_e = (model.build_grid_e)();
            let grid_sub_e = grid_e
                .subgrid(|p| p.im.is_zero())
                .into_quadrature_grid(Quadrature::GaussLobatto(15));
            let grid_e = grid_e.into_quadrature_grid(Quadrature::GaussLobatto(15));

            // Find EV and ES for electronic state
            let (ev_e_b, es_e_b) = {
                let (ev, v) = ModelParams::get_bound_state(model, &grid_sub_e);

                let mut next = FullVector::<C64>::zero(grid_e.len());

                next.as_mut_slice()[1..=v.len()].copy_from_slice(v.as_slice());

                (ev, next)
            };

            (Arc::new(grid_e), Arc::new((ev_e_b, es_e_b)))
        };

        (1..=20).into_par_iter().for_each(|i| {
            let elems = i * 2;
            println!(" - {elems} elements");
            // Setup grid for coordinate R
            let (state_m, grid_m) = {
                let mut grid_m = iterate::primitives::grid::Grid::<C64>::empty();

                let exp = C64::new(0., FRAC_PI_4).exp();
                grid_m
                    .append_equidist(6., elems)
                    .append_many([5. * exp, 15. * exp])
                    .append_equidist(80. * exp, 4);

                let grid_sub_m = grid_m.subgrid(|p| p.im.is_zero());
                let grid_m = grid_m.into_quadrature_grid(Quadrature::GaussLobatto(gl));

                // Find EV and ES for molecular states
                let system = Eigensystem::get_molecule_eigensystem(
                    model,
                    &grid_sub_m.into_quadrature_grid(Quadrature::GaussLobatto(gl)),
                );

                (
                    system.get_extended_triplet(0, grid_m.len()),
                    Arc::new(grid_m),
                )
            };

            let opts = ModelParams {
                target_prec: TARGET_PREC * 10.,
                max_iters: Some(100_000),
                model,
                quad_order: (gl, 15),
                ordering: EM,
                preconditioner: get_preconditioner("ilu0"),
                t_e: 0.0790625,

                grid_e: grid_e.clone(),
                grid_m,
                e_state_b: e_state_b.clone(),
                m_state_i: state_m.clone(),
            };

            let result = solve(&opts, Option::<fn(_)>::None).unwrap();
            let sigmave = result.sigma_ve(&opts, state_m);

            conn.lock().unwrap().execute(
                "INSERT OR REPLACE INTO acc_per_elems_2d (model, method, ordering, gl, e, elems, error) VALUES ('N_2', 'ilu0', 'em', ?1, ?2, ?3, ?4)",
                params![format!("{}x{}", gl, 15), 0.0790625, elems, ref_val - sigmave]).unwrap();
        });
    }

    Ok(())
}
