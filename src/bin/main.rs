use std::collections::HashMap;

fn main() {
    let (args, funcs) = {
        let mut args = vec![];
        let mut funcs = HashMap::new();

        for problem in iterate::problems::get_problems() {
            args.push(problem.arg);

            funcs.insert(problem.id, problem.func);
        }

        (args, funcs)
    };

    let app = clap::App::new("Iterative solver")
        .author("Ivánek R. <richard.ivanek@gmail.com>")
        .setting(clap::AppSettings::SubcommandRequiredElseHelp)
        .subcommands(args);

    let matches = app.get_matches();

    if let (problem, Some(matches)) = matches.subcommand() {
        if let Some(f) = funcs.get(problem) {
            f(matches);
        } else {
            panic!("Invalid subcommand");
        }
    } else {
        eprintln!("No problem provided");
    }
}
