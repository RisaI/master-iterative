use std::{
    error::Error,
    sync::{Arc, Mutex},
};

use clap::App;
use iterate::{
    models::TOY_MODEL,
    primitives::{
        grid::{Grid, Quadrature},
        FullVector, C64,
    },
    problems::model_1d::{
        build_grid,
        calculation::{CalculationOptions, CalculationResults},
    },
};
use itertools::iproduct;
use rayon::prelude::*;
use rusqlite::{params, Connection};

const TARGET_PREC: f64 = 1e-8;
const GL_ORDERS: [usize; 3] = [5, 10, 15];
const CHANNELS: [usize; 2] = [0, 1];
const PRESELECT_E: [f64; 4] = [0.1, 1.9, 2.1, 3.];
const METHODS: &[&str; 3] = &["iterative", "ilu0", "direct"];

fn equidist(from: f64, to: f64, points: usize) -> impl Iterator<Item = f64> + Clone {
    assert!(points > 1);

    let d = (to - from) / (points - 1) as f64;

    (0..points).map(move |i| from + d * i as f64)
}

fn solve(
    method: &str,
    opts: &CalculationOptions,
    on_iter: Option<impl FnMut(usize, f64)>,
    kickstart: Option<FullVector<C64>>,
) -> CalculationResults<2> {
    match method {
        "iterative" => iterate::problems::model_1d::calculation::run_iterative(
            &iterate::models::TOY_MODEL,
            opts,
            on_iter,
            kickstart,
        )
        .unwrap(),
        "ilu0" => iterate::problems::model_1d::calculation::run_iterative_preconditioned(
            &iterate::models::TOY_MODEL,
            opts,
            on_iter,
            kickstart,
        )
        .unwrap(),
        "direct" => {
            iterate::problems::model_1d::calculation::run_direct(&iterate::models::TOY_MODEL, opts)
        }
        m => panic!("unknown method {m}"),
    }
}

fn main() -> Result<()> {
    let matches = App::new("1D Orchestration")
        .subcommands([App::new("plot")])
        .get_matches();

    let conn = rusqlite::Connection::open("results.db")?;

    if let (cmd, Some(_)) = matches.subcommand() {
        if cmd == "plot" {
            return plot(conn);
        }

        return Ok(());
    }

    println!("Running orchestrated work for two-channel 1D model");

    let conn = Arc::new(Mutex::new(conn));

    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS iters_per_e (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            method VARCHAR(32) NOT NULL,
            gl INTEGER NOT NULL,
            e REAL NOT NULL,
            channel INTEGER NOT NULL,
            iters INTEGER NOT NULL,
            runtime REAL NOT NULL,
            error REAL NOT NULL,
            UNIQUE(method, gl, e, channel)
        )",
        params![],
    )?;

    // ANCHOR Number of iterations for required precision and GL order per energy N(E, GL)

    let grid_map: Arc<dashmap::DashMap<_, _>> = Arc::new(
        GL_ORDERS
            .iter()
            .map(|&gl| (gl, Arc::new(build_grid(gl, 45.))))
            .collect(),
    );

    const RESOLUTION: usize = 1024;

    // ANCHOR Convergence graph for selected energies Prec(N, GL)
    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS result_1d(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                method VARCHAR(32) NOT NULL,
                gl INTEGER NOT NULL,
                chin INTEGER NOT NULL,
                chout INTEGER NOT NULL,

                e REAL NOT NULL,
                sigma REAL NOT NULL,
                UNIQUE(method, gl, chin, chout, e)
    )",
        params![],
    )?;

    println!("Longer ECS");
    for i in 1..=3 {
        let mut grid = Grid::empty();
        let exp = C64::new(0., std::f64::consts::PI / 4.).exp();

        grid.append_equidist(2., 2); // 0-1-2
        grid.append_many([exp, 3. * exp, 8. * exp, 16. * exp]);
        grid.append_equidist(i as f64 * 20. * exp, 6);

        let grid = Arc::new(grid.into_quadrature_grid(Quadrature::GaussLobatto(15)));

        let results: Vec<_> = equidist(1e-2, 5., RESOLUTION + 1)
            .take(RESOLUTION)
            .par_bridge()
            .filter_map(|e| {
                if (e - 2.0).abs() < 1e-4 {
                    return None;
                }
                let opts = CalculationOptions {
                    energy: e,
                    init_channel: 0,
                    max_iters: None,
                    precision: TARGET_PREC,
                    grid: grid.clone(),
                };

                let result = solve("ilu0", &opts, Option::<fn(_, _)>::None, None)
                    .get_cross_section(&TOY_MODEL, &opts.grid, 0);

                Some((e, result))
            })
            .collect();

        let mut conn = conn.lock().unwrap();
        let tx = conn.transaction().unwrap();

        for (e, result) in results {
            tx.execute("INSERT OR REPLACE INTO result_1d (method, gl, chin, chout, e, sigma) VALUES (?1, 15, 0, 0, ?2, ?3)",  params![format!("ilu0-long-{i}"), e, result],).unwrap();
        }

        tx.commit().unwrap();
    }

    println!("Sigma");
    iproduct!(GL_ORDERS, CHANNELS, CHANNELS, ["analytic","ilu0", "direct"])
        .for_each({
            let conn = conn.clone();
            let grid_map = grid_map.clone();

            move |(gl, chin, chout, method)| {
                let results: Vec<_> = equidist(1e-2, 5., RESOLUTION  + 1).take(RESOLUTION)
                    .par_bridge()
                    .filter_map(|e|{
                        if method == "analytic" {
                            return Some((e, TOY_MODEL.analytic_scatter_solution(chin, chout, e)));
                        }
                        else if (e - 2.0).abs() < 1e-4 {
                            return None;
                        }

                        let opts = CalculationOptions {
                            energy: e,
                            init_channel: chin,
                            max_iters: None,
                            precision: TARGET_PREC,
                            grid: grid_map.get(&gl).unwrap().clone(),
                        };

                        let result = solve(method, &opts, Option::<fn(_, _)>::None, None).get_cross_section(&TOY_MODEL, &opts.grid, chout);

                        Some((e, result))
                    })
                    .collect();

                let mut conn = conn.lock().unwrap();
                let tx = conn.transaction().unwrap();

                for (e, result) in results {
                    tx.execute("INSERT OR REPLACE INTO result_1d (method, gl, chin, chout, e, sigma) VALUES (?1, ?2, ?3, ?4, ?5, ?6)",  params![method, gl, chin, chout, e, result],).unwrap();
                }

                tx.commit().unwrap();
            }
        });

    println!("Ultralocal method");
    iproduct!(GL_ORDERS, CHANNELS, METHODS)
        .for_each({
            let conn = conn.clone();
            let grid_map = grid_map.clone();

            move |(gl,channel, method)| {
                let results: Vec<_> = equidist(1e-2, 5., RESOLUTION  + 1).take(RESOLUTION)
                    .par_bridge()
                    .filter_map(|e|{
                        if (e - 2.0).abs() < 1e-4 {
                            return None;
                        }

                        let opts = CalculationOptions {
                            energy: e,
                            init_channel: channel,
                            max_iters: None,
                            precision: TARGET_PREC,
                            grid: grid_map.get(&gl).unwrap().clone(),
                        };

                        let result = solve(method, &opts, Option::<fn(_, _)>::None, None);

                        Some((e, result.iters, result.runtime as f64 / 1e3, result.precision))
                    })
                    .collect();

                let mut conn = conn.lock().unwrap();
                let tx = conn.transaction().unwrap();

                for (e, iters, runtime, precision) in results {
                    tx.execute("INSERT OR REPLACE INTO iters_per_e (method, gl, e, channel, iters, runtime, error) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",  params![method, gl, e, channel, iters, runtime, precision],).unwrap();
                }

                tx.commit().unwrap();
            }
        });

    let pts = equidist(1e-2, 5., 1024).collect::<Vec<_>>();

    println!("Kickstarted method");
    iproduct!(GL_ORDERS, pts.chunks_exact(1024 / 32), CHANNELS, ["iterative", "ilu0"])
        .par_bridge()
        .for_each({
            let conn = conn.clone();
            let grid_map = grid_map.clone();

            move |(gl, window, channel, method)| {
                let mut kickstart = None;
                let mut results = Vec::with_capacity(window.len());

                for &e in window {
                    if (e - 2.0).abs() < 1e-4 {
                        continue;
                    }

                    let opts = CalculationOptions {
                        energy: e,
                        init_channel: channel,
                        max_iters: None,
                        precision: TARGET_PREC,
                        grid: grid_map.get(&gl).unwrap().clone(),
                    };

                    let result = solve(method, &opts, Option::<fn(_, _)>::None, kickstart);

                    results.push((e, result.iters, result.runtime as f64 / 1e3, result.precision));

                    kickstart = Some(result.psi_sc);
                }

                let mut conn = conn.lock().unwrap();
                let tx = conn.transaction().unwrap();

                for (e, iters, runtime, precision) in results {
                    tx.execute(
                        "INSERT OR REPLACE INTO iters_per_e (method, gl, e, channel, iters, runtime, error) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",
                        params![format!("{method}-kickstart"), gl, e, channel, iters, runtime, precision]).unwrap();
                }

                tx.commit().unwrap();
            }
        });

    std::mem::drop(pts);

    println!("Kickstart bench");

    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS kickstart_per_e (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            method VARCHAR(32) NOT NULL,
            gl INTEGER NOT NULL,
            e REAL NOT NULL,
            density INTEGER NOT NULL,
            channel INTEGER NOT NULL,
            iters INTEGER NOT NULL,
            runtime REAL NOT NULL,
            error REAL NOT NULL,
            UNIQUE(method, gl, e, channel, density)
        )",
        params![],
    )?;

    let dist = (5. - 1e-2) / 15.;
    iproduct!([15], equidist(1e-2, 5., 17).take(16), CHANNELS, ["iterative", "ilu0"], [32, 64, 96, 128])
        .par_bridge()
        .for_each({
            let conn = conn.clone();
            let grid_map = grid_map.clone();

            move |(gl, from_e, channel, method, density)| {
                let mut kickstart = None;
                let mut results = Vec::with_capacity(density);

                for e in equidist(from_e, from_e + dist, density + 1).take(density) {
                    if (e - 2.0).abs() < 1e-4 {
                        continue;
                    }

                    let opts = CalculationOptions {
                        energy: e,
                        init_channel: channel,
                        max_iters: None,
                        precision: TARGET_PREC,
                        grid: grid_map.get(&gl).unwrap().clone(),
                    };

                    let result = solve(method, &opts, Option::<fn(_, _)>::None, kickstart);

                    results.push((e, result.iters, result.runtime as f64 / 1e3, result.precision));

                    kickstart = Some(result.psi_sc);
                }

                let mut conn = conn.lock().unwrap();
                let tx = conn.transaction().unwrap();

                for (e, iters, runtime, precision) in results {
                    tx.execute(
                        "INSERT OR REPLACE INTO kickstart_per_e (method, gl, e, channel, density, iters, runtime, error) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)",
                        params![format!("{method}-kickstart"), gl, e, channel, density, iters, runtime as f64 / 1e3, precision]
                    ).unwrap();
                }

                tx.commit().unwrap();
            }
        });

    // ANCHOR Convergence graph for selected energies Prec(N, GL)
    println!("Convergence graphs");
    conn.lock().unwrap().execute(
        "CREATE TABLE IF NOT EXISTS conv_per_n(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                method VARCHAR(32) NOT NULL,
                gl INTEGER NOT NULL,
                e REAL NOT NULL,
                channel INTEGER NOT NULL,
                n INTEGER NOT NULL,
                error REAL NOT NULL,
                UNIQUE(method, gl, e, channel, n)
    )",
        params![],
    )?;

    iproduct!(GL_ORDERS, PRESELECT_E, CHANNELS, ["iterative", "ilu0"])
        .par_bridge()
        .for_each({
            // let conn = conn.clone();

            move |(gl, e, channel, method)| {
                let opts = CalculationOptions {
                    energy: e,
                    init_channel: channel,
                    max_iters: None,
                    precision: TARGET_PREC,
                    grid: grid_map.get(&gl).unwrap().clone(),
                };

                let mut convergence = vec![];
                let result = solve(method, &opts, Some(|_, prec| {
                    convergence.push(prec);
                }), None);

                println!(
                    "Convergence graph for {gl}:{e}:{channel} done in {}ms",
                    result.runtime as f64 / 1000.
                );

                conn.lock()
                    .map(|mut conn| {
                        let tx = conn.transaction().unwrap();

                        for (n, err) in convergence.iter().enumerate() {
                            tx.execute(
                                "INSERT OR REPLACE INTO conv_per_n (method, gl, e, channel, n, error) VALUES (
                            ?1, ?2, ?3, ?4, ?5, ?6
                        )",
                                params![method, gl, e, channel, n, err],
                            )
                            .ok();
                        }

                        tx.commit().ok();
                    })
                    .ok();
            }
        });

    Ok(())
}

type Result<T> = std::result::Result<T, Box<dyn Error>>;

fn show_plot(
    title: impl AsRef<str>,
    mut stuff: impl FnMut(&mut plotly::Plot) -> Result<()>,
) -> Result<()> {
    use plotly::{common::Title, Layout, Plot};

    let mut plot = Plot::new();
    plot.set_layout(Layout::new().title(Title::new(title.as_ref())));

    stuff(&mut plot)?;

    plot.show();

    Ok(())
}

fn plot(conn: Connection) -> Result<()> {
    use plotly::Scatter;

    for gl in GL_ORDERS {
        show_plot(format!("N(E) for GL {gl}"), |plot| {
            let mut stmt = conn.prepare("SELECT e, iters FROM iters_per_e WHERE method = ?3 AND gl = ?1 AND channel = ?2 ORDER BY e")?;

            for (channel, method) in iproduct!(CHANNELS, METHODS) {
                let mut trace = (vec![], vec![]);

                let mut rows = stmt.query(params![gl, channel, method])?;
                while let Some(row) = rows.next()? {
                    trace.0.push(row.get::<_, f64>(0)?);
                    trace.1.push(row.get::<_, usize>(1)?);
                }

                plot.add_trace(
                    Scatter::new(trace.0, trace.1).text(&format!("Channel {channel}, {method}")),
                );
            }

            Ok(())
        })?;

        for channel in CHANNELS {
            use plotly::{common::Title, layout::Axis, Layout};
            show_plot("", |plot| {
                plot.set_layout(
                    Layout::new()
                        .title(Title::new(&format!(
                            "Prec(N) for GL {gl} in channel {channel}"
                        )))
                        .y_axis(Axis::new().type_(plotly::layout::AxisType::Log)),
                );

                let mut stmt = conn.prepare("SELECT n, error FROM conv_per_n WHERE gl = ?1 AND channel = ?2 AND e = ?3 ORDER BY n")?;

                for e in PRESELECT_E {
                    let mut trace = (vec![], vec![]);

                    let mut rows = stmt.query(params![gl, channel, e])?;
                    while let Some(row) = rows.next()? {
                        trace.0.push(row.get::<_, usize>(0)?);
                        trace.1.push(row.get::<_, f64>(1)?);
                    }

                    plot.add_trace(
                        Scatter::new(trace.0, trace.1).text(&format!("Channel {channel}, E = {e}")),
                    );
                }

                Ok(())
            })?;
        }
    }

    Ok(())
}
