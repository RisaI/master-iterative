use iterate::primitives::C64;

#[test]
fn complex_mul() {
    let len = C64::new(0.0, 4.0).conj().norm();
    assert!((1.0 - len) < 1e-8);
}

#[test]
fn len_vs_conjugacy() {
    const RANGE: i32 = 100;
    for i in -RANGE..RANGE {
        for j in -RANGE..RANGE {
            let a = C64::new(i as f64, j as f64);

            assert!(((a * a.conj()).re - a.norm_sqr()) < 1e-8);
        }
    }
}
