use iterate::primitives::grid::MultiIndexIterator;

#[test]
pub fn multiindex_size() {
    let mul = MultiIndexIterator::new([0..4, 0..3, 0..2, 0..1]);

    let points: Vec<[usize; 4]> = mul.collect();

    println!("{:?}", points);

    assert_eq!(points.len(), 24);
}
