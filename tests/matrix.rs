use iterate::primitives::{FullMatrix, FullVector, Matrix};

#[test]
fn mat_vec_mul() {
    let m = FullMatrix::<f64>::new(3, Box::new([1.0, 0.2, 2.8, 0.0, 0.1, 1.2, 3.2, 2.4, 1.0]));

    let x = FullVector::<f64>::from(vec![1.5, 0.7, 1.0]);

    let y = m.mul_vec(&x);

    assert!((y[0] - (1.5 + 0.14 + 2.8)) < 1e-8);
    assert!((y[1] - (0.07 + 1.2)) < 1e-8);
    assert!((y[2] - (4.8 + 1.68 + 1.0)) < 1e-8);
}
